Carlos Coloma Portfolio: 

- BlackJack Online Game (2019): Individual Project. Online Game. 
Technologies: VS2017 ASP .NET, MVC, C#, JavaScript, JQuery, Ajax, CSS, HTML, Bootstrap.

- FacePalm  Social Network (2019): Team Project. Social Network. 
Technologies: XAMPP, PHP, Slim framework, Twig, HTML, JavaScript, JQuery, Ajax, CSS, Bootstrap, MySQL.

- Math Quiz Mobile App (2018): Individual Project. Mobile application. 
Technologies: Android, Java.

- Hotel Management Desktop App (2018): Team Project. Desktop Application. 
Technologies: Database Azure, C#, packages: NuGet Unit Test, Entity Framework, MahApps. 

- Library Database (2018): Team Project. Database. 
Technologies: Microsoft SQL Server. 

- Rock Band Webpage (2018): Team Project. Webpage. 
Technologies:  HTML, JavaScript, CSS, Bootstrap.

- Memory Online Game (2018): Team Project. Online Game. 
Technologies: HTML, CSS.

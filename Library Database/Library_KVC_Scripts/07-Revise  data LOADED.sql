/* Revise Data in Tables
	Script Date: September 15, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the Library_KVC database
use Library_KVC
;
go


select *
from Member.Member 
;
go

select *
from Member.Adult 
;
go

select *
from Member.Juvenile 
;
go

select *
from Book.Title 
;
go

select *
from Book.Item 
;
go

select *
from Book.Copy 
;
go

select *
from Business.Reservation
;
go

select *
from Business.Loan
;
go

select *
from Business.Loan
;
go

select *
from Business.Message
;
go
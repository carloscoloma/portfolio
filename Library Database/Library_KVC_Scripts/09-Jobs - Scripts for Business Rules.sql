/* Jobs Scripts in Library_KVC
	Script Date: September 19, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the Library_KVC database
use Library_KVC
;
go

/*****************Scripts to run dayly at 01:00 am *****************************/
/* Script 1. Notify the following Juvenile Members for 18 birthday date approaching in 1 month */

select MJ.MemberID, MM.LastName, MM.FirstName, MJ.BirthDate, 'Notification for 18 birthday date approaching' as 'Message'
from member.Juvenile as MJ
	inner join Member.Member as MM
		on MJ.MemberID = MM.MemberID
where (MJ.BirthDate < DATEADD(month, -1, DATEADD(year, -18, getdate())))
;
go

/* Script 2. Move Juvenile Members to Adult Table when they turn 18 */

Insert INTO Member.Adult 
	select MJ.MemberID, MA.Street, MA.City, MA.State, MA.Zip, MA.PhoneNo, DATEADD(year, 1, getdate())
	from member.Juvenile as MJ
		inner join Member.Adult as MA
			on MJ.AdultMemberID = MA.MemberID
	where (MJ.BirthDate < DATEADD(year, -18, getdate()))
;
go

delete from member.Juvenile
where (BirthDate < DATEADD(year, -18, getdate()))
;
go

/* Script 3. Notify the following Adult Members of expiration date*/

select MA.MemberID, MM.LastName, MM.FirstName, MA.ExprDate, 'Notification for membership expiration date approaching' as 'Message'
from Member.Adult as MA
	inner join Member.Member as MM
		on MA.MemberID = MM.MemberID
where (MA.ExprDate < DATEADD(month, -1, getdate()))
order by MA.ExprDate asc
;
go

/*****************Script to run at every loan *****************************/
declare @LoanForMemberID as int = 663  -- Enter in this variable the current memberID
select IIF(count(BL.LoanID)>4 , 'Sorry, you cannot loan more books','You can loan this book')
from Business.Loan as BL
where BL.InDate is Null and BL.MemberID = @LoanForMemberID
;
go

/*****************Script to run at every reservation *****************************/
declare @ReservationForMemberID as int = 663  -- Enter in this variable the current memberID
select IIF(count(BR.ResevationID)>4 , 'Sorry, you cannot reserve more books','You can reserve this book')
from Business.Reservation as BR
where BR.MemberID = @ReservationForMemberID
;
go
/* Insert Data into Tables in Library_KVC
	Script Date: September 15, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the Library_KVC database
use Library_KVC
;
go

/***** 01. Load data to table Member.Member *****/

-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	MemberID int not null,
	LastName nvarchar(30) not null,
	FirstName nvarchar(30) not null,
	MiddelInitial nvarchar(2) null,
	Photograph nvarchar(max) null,
)
;
go

-- Load data to #TemporaryTable
BULK insert #TemporaryTable 
from 'C:\KVC_Databases\Library_KVC_Data\member_data.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator =  '0x0D0A'	
	)
;
go

-- Copy data from #TemporaryTable to Member.Member table
SET IDENTITY_INSERT Member.Member ON
Insert INTO Member.Member ( MemberID, LastName, FirstName, MiddelInitial, Photograph)
SELECT T.MemberID, T.LastName, T.FirstName, T.MiddelInitial, 
		cast ( nullif(T.Photograph,'NULL') as varbinary(max) )
FROM #TemporaryTable as T
SET IDENTITY_INSERT Member.Member OFF
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

/***** 02. Load data to table Member.Adult *****/

-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	MemberID int not null, 
	Street nvarchar(60) not null,
	City nvarchar(15) not null,
	State nvarchar(2) not null,
	Zip nvarchar(15) not null,
	PhoneNo nvarchar(20) null,
	ExprDate datetime not null,
	ExprTime datetime not null
)
;
go

-- Load data to #TemporaryTable
BULK insert #TemporaryTable 
from 'C:\KVC_Databases\Library_KVC_Data\adult_data.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go

-- Copy data from #TemporaryTable to Member.Adult table
Insert INTO Member.Adult 
SELECT T.MemberID, T.Street, T.City, T.State, T.Zip, 
		nullif(T.PhoneNo,'NULL'), DATEADD(year, 11, T.ExprDate + T.ExprTime)
FROM #TemporaryTable as T
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

/***** 03. Load data to table Member.Juvenile *****/

-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	MemberID int not null, 
	AdultMemberID int not null, 
	BirthDate datetime not null,
	BirthTime datetime not null
)
;
go

-- Load data to #TemporaryTable
BULK insert #TemporaryTable  
from 'C:\KVC_Databases\Library_KVC_Data\juvenile_data.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go

-- Copy data from #TemporaryTable to Member.Juvenile table
Insert INTO Member.Juvenile 
SELECT T.MemberID, T.AdultMemberID, DATEADD(year, 11, T.BirthDate + T.BirthTime)
FROM #TemporaryTable as T
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

/***** 04. Load data to table Book.Title *****/

-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	TitleID int not null, 
	Title nvarchar(255) not null,
	Author nvarchar(50) not null,
	Synopsis nvarchar(max) not null,
)
;
go

-- Load data to #TemporaryTable
BULK insert #TemporaryTable  
from 'C:\KVC_Databases\Library_KVC_Data\title_data.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go

-- Copy data from #TemporaryTable to Book.Title table
SET IDENTITY_INSERT Book.Title ON
Insert INTO Book.Title (TitleID, Title, Author, Synopsis)
SELECT T.TitleID, T.Title, T.Author,  
		NULLIF( T.Synopsis, 'NULL')
FROM #TemporaryTable as T
SET IDENTITY_INSERT Book.Title OFF
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

/***** 05. Load data to table Book.Item *****/

-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	ISBN int not null,
	TitleID int not null, 
	Language nvarchar(20) not null,
	Cover nvarchar(20) not null, 
	Loanable nvarchar(20) not null, 
)
;
go

-- Load data to #TemporaryTable
BULK insert #TemporaryTable  
from 'C:\KVC_Databases\Library_KVC_Data\item_data.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go

-- Copy data from #TemporaryTable to Book.Item table
Insert INTO Book.Item 
SELECT T.ISBN, T.TitleID, T.Language, 
		IIF( T.Cover = 'NULL', NULL, IIF( T.Cover = 'hardback', 1, 0)), 
		IIF( T.Loanable = 'NULL', NULL, IIF( T.Loanable = 'Y', 1, 0))
FROM #TemporaryTable as T
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

/***** 06. Load data to table Book.Copy *****/

-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	ISBN int not null, 
	CopyNo int not null,
	TitleID int not null,
	OnLoan nvarchar(10) not null, 
)
;
go

-- Load data to #TemporaryTableCopy
BULK insert #TemporaryTable  
from 'C:\KVC_Databases\Library_KVC_Data\copy_data.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go

-- Create #TemporaryTableCopy
IF OBJECT_ID('tempdb..#TemporaryTableCopy', 'U') is not null
DROP TABLE #TemporaryTableCopy
;
go

CREATE TABLE #TemporaryTableCopy
(
	CopyID int identity(1,1) not null,
	ISBN int not null, 
	CopyNo int not null,
	TitleID int not null,
	OnLoan nvarchar(10) not null, 
)
;
go

-- Copy data from #TemporaryTable to #TemporaryTableCopy table
Insert INTO #TemporaryTableCopy ( ISBN, CopyNo, TitleID, OnLoan )
SELECT T.ISBN, T.CopyNo, T.TitleID, T.OnLoan
FROM #TemporaryTable as T
order by T.ISBN, T.CopyNo
;
go

-- Copy data from ##TemporaryTableCopy to Book.Copy table
SET IDENTITY_INSERT Book.Copy ON
Insert INTO Book.Copy ( CopyID, ISBN, TitleID, OnLoan, OnHold, ReservationID, StatusGood)
SELECT TC.CopyID, TC.ISBN, TC.TitleID,  
		IIF( TC.OnLoan = 'Y', 1, 0),
		0, NULL, 1
FROM #TemporaryTableCopy as TC
SET IDENTITY_INSERT Book.Copy OFF
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

/***** 07. Load data to table Business.Reservation *****/

-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	-- ReservationID int not null,
	ISBN int not null, 
	MemberID int not null, 
	LogDate datetime not null,
	LogTime datetime not null,
	Remarks nvarchar(max) not null,
)
;
go

-- Load data to #TemporaryTable
BULK insert #TemporaryTable  
from 'C:\KVC_Databases\Library_KVC_Data\reservation_data.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go

-- Copy data from #TemporaryTable to Business.Reservation table
Insert INTO Business.Reservation (ISBN, MemberID, LogDate, Remarks)
SELECT T.ISBN, T.MemberID, dateadd(year, 10,T.LogDate + T.LogTime),  
		NULLIF( T.Remarks, 'NULL')
FROM #TemporaryTable as T
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

/***** 08. Load data to table Business.Loan *****/

-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	-- LoanID int identity(1,1) not null,
	ISBN int not null, 
	CopyNo int not null, 
	TitleID int not null, 
	MemberID int not null, 
	OutDate datetime not null,
	OutTime datetime not null,
	DueDate datetime not null,
	DueTime datetime not null,
)
;
go

-- Load data to #TemporaryTable
BULK insert #TemporaryTable  
from 'C:\KVC_Databases\Library_KVC_Data\loan_dataV3.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go


-- Create #TemporaryTable2
IF OBJECT_ID('tempdb..#TemporaryTable2', 'U') is not null
DROP TABLE #TemporaryTable2
;
go

CREATE TABLE #TemporaryTable2
(
	-- LoanhistID int identity(1,1) not null,
	ISBN int not null,
	CopyNo int not null, 
	OutDate datetime not null,
	OutTime datetime not null,
	TitleID int not null, 
	MemberID int not null, 
	DueDate datetime not null,
	DueTime datetime not null,
	InDate datetime not null,
	InTime datetime not null,
	FineAssessed varchar(30) not null,
	FinePaid varchar(30) not null,
	FineWaived varchar(30) not null,
	Remarks nvarchar(max) not null,
)
;
go

-- Load data to #TemporaryTable2
BULK insert #TemporaryTable2  
from 'C:\KVC_Databases\Library_KVC_Data\loanhist_dataV3.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go

-- Copy data from #TemporaryTable2 to Business.Loan table
Insert INTO Business.Loan (CopyID, MemberID, OutDate, DueDate, InDate)
SELECT TC.CopyID, T2.MemberID, 
		dateadd(year, 10, T2.OutDate + T2.OutTime), 
		dateadd(year, 10, T2.DueDate + T2.DueTime), 
		dateadd(year, 10, T2.InDate + T2.InTime)
FROM #TemporaryTable2 as T2
	inner join #TemporaryTableCopy as TC
		on T2.ISBN = TC.ISBN and T2.CopyNo = TC.CopyNo
order by T2.ISBN, T2.CopyNo
;
go

-- Copy data from #TemporaryTable to Business.Loan table
Insert INTO Business.Loan (CopyID, MemberID, OutDate, DueDate)
SELECT TC.CopyID, T.MemberID, 
		dateadd(year, 10, T.OutDate + T.OutTime), 
		dateadd(year, 10,T.DueDate + T.DueTime)
FROM #TemporaryTable as T
	inner join #TemporaryTableCopy as TC
		on T.ISBN = TC.ISBN and T.CopyNo = TC.CopyNo
order by T.ISBN, T.CopyNo
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

-- Drop #TemporaryTable2
IF OBJECT_ID('tempdb..#TemporaryTable2', 'U') is not null
DROP TABLE #TemporaryTable2
;
go

/***** 09. Load data to table Business.Loanhist *****/
/*
-- Create #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go

CREATE TABLE #TemporaryTable
(
	-- LoanhistID int identity(1,1) not null,
	ISBN int not null,
	CopyID int not null, 
	OutDate datetime not null,
	OutTime datetime not null,
	TitleID int not null, 
	MemberID int not null, 
	DueDate datetime not null,
	DueTime datetime not null,
	InDate datetime not null,
	InTime datetime not null,
	FineAssessed varchar(30) not null,
	FinePaid varchar(30) not null,
	FineWaived varchar(30) not null,
	Remarks nvarchar(max) not null,
)
;
go

-- Load data to #TemporaryTable
BULK insert #TemporaryTable  
from 'C:\KVC_Databases\Library_KVC_Data\loanhist_data.txt' 
with 
	(
		datafiletype = 'char',
		fieldterminator = '\t',
		rowterminator = '0x0D0A'
	)
;
go


-- Copy data from #TemporaryTable to Business.Loanhist table
Insert INTO Business.Loanhist 
	(ISBN, CopyID, OutDate, TitleID, MemberID, DueDate, InDate, FineAssessed, FinePaid, FineWaived, Remarks)
SELECT T.ISBN, T.CopyID, T.OutDate + T.OutTime, T.TitleID, T.MemberID, T.DueDate + T.DueTime,
	T.InDate + T.InTime, 
	cast ( nullif(T.FineAssessed,'NULL') as money ),
	cast ( nullif(T.FinePaid,'NULL') as money ),
	cast ( nullif(T.FineWaived,'NULL') as money ),
	nullif(T.Remarks,'NULL')
FROM #TemporaryTable as T
;
go

-- Drop #TemporaryTable
IF OBJECT_ID('tempdb..#TemporaryTable', 'U') is not null
DROP TABLE #TemporaryTable
;
go
*/

-- Drop #TemporaryTableCopy
IF OBJECT_ID('tempdb..#TemporaryTableCopy', 'U') is not null
DROP TABLE #TemporaryTableCopy
;
go

/***** 10. Load data to table Business.Message *****/

-- Delete table content if you must
delete from Business.Message
;
go

-- inserting data into Message table
insert into  Business.Message(MessageID,Content)
values 
('0001','Book overdue 1st reminder. 25 cents a day charge applies! Please prelong your overdue items online or return them to the libriary.'),
('0002','Book overdue 2rd reminder. 25 cents a day charge applies! Your account will be suspended in 21 days! Please return your items to the libriary.'),
('0003','Book overdue 3rd reminder. Please note your account was suspended. Return your items to the libriary ASAP'),
('0004','Your Library membership will expire in 1 week. Please come to the libriary to renew your membership')
;
go


/* Apply Data Integrity to Tables in Library_KVC
	Script Date: September 12, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the Library_KVC database
use Library_KVC
;
go

/***** Create FK's in tables *****/

-- 01. create FK in table Member.Member

-- 02. create FK in table Member.Adult
alter table Member.Adult
	add constraint fk_Adult_Member foreign key (MemberID)
	references Member.Member (MemberID)
;
go

-- 03. create FK in table Member.Juvenile
alter table Member.Juvenile
	add constraint fk_Juvenile_Member foreign key (MemberID)
	references Member.Member (MemberID)
;
go

alter table Member.Juvenile 
	add constraint fk_Juvenile_Adult foreign key (AdultMemberID)
	references Member.Adult (MemberID)
;
go

-- 04. create FK in table Book.Item
alter table Book.Item
	add constraint fk_Item_Title foreign key (TitleID)
	references Book.Title (TitleID)
;
go

-- 05. create FK in table Book.Copy
alter table Book.Copy
	add constraint fk_Copy_Item foreign key (ISBN)
	references Book.Item (ISBN)
;
go

alter table Book.Copy
	add constraint fk_Copy_Title foreign key (TitleID)
	references Book.Title (TitleID)
;
go

-- 06. create FK in table Book.Title

-- 07. create FK in table Business.Reservation
alter table Business.Reservation
	add constraint fk_Reservation_Item foreign key (ISBN)
	references Book.Item (ISBN)
;
go

alter table Business.Reservation
	add constraint fk_Reservation_Member foreign key (MemberID)
	references Member.Member (MemberID)
;
go

-- 08. create FK in table Business.Loan
alter table Business.Loan
	add constraint fk_Loan_Member foreign key (MemberID)
	references Member.Member (MemberID)
;
go

alter table Business.Loan
	add constraint fk_Loan_Copy foreign key (CopyID)
	references Book.Copy (CopyID)
;
go

/*
alter table Business.Loan
	add constraint fk_Loan_Title foreign key (TitleID)
	references Book.Title (TitleID)
;
go
*/

-- 09. create FK in table Business.Fine
alter table Business.Fine
	add constraint fk_Fine_Loan foreign key (LoanID)
	references Business.Loan (LoanID)
;
go

-- 10. create FK in table Business.MessageLog
alter table Business.MessageLog
	add constraint fk_MessageLog_Loan foreign key (LoanID)
	references Business.Loan (LoanID)
;
go

-- 11. create FK in table Business.MessageLog
alter table Business.MessageLog
	add constraint fk_MessageLog_Message foreign key (MessageID)
	references Business.Message (MessageID)
;
go

/***** Create Constraints in tables *****/

-- 01. make WA the default for state column in adult table
alter table Member.Adult
	add constraint df_State
		default 'WA' for State
;
go

-- 02. create phone number constraint for the adult table
alter table Member.Adult
	add constraint df_PhoneNo
		default Null for PhoneNo
;
go

-- 03. add a default constraint to the DateSent column in the MessageLog table: 
-- DateSent = getdate()
alter table Business.MessageLog
	add constraint df_DateSent
		default getdate() for DateSent
;
go

-- 04. add a constraint to the DueDate column in the loan table: 
-- DueDate >= OutDate
alter table Business.Loan
	add constraint ck_DueDate_Loan
		check (DueDate >= OutDate)
;
go

-- 05. add a constraint to the InDate column in the loan table: 
-- InDate >= OutDate
alter table Business.Loan
	add constraint ck_InDate_Loan
		check (InDate >= OutDate)
;
go

-- 06. add a constraint to the BirthDate column in the Juvenile table: 
-- (BirthDate > DATEADD(year, -18, getdate()))
alter table Member.Juvenile
	add constraint ck_CheckAge_Juvenile
		check (BirthDate > DATEADD(year, -18, getdate()))
;
go

-- 07. make current date the default for LogDate column in Reservation table
alter table Business.Reservation
	add constraint df_LogDate
		default getdate() for LogDate
;
go

/* Create Indexes in Library_KVC
	Script Date: September 17, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the Library_KVC database
use Library_KVC
;
go

-- create indexes
create nonclustered index ncl_Title on Book.Title (Title)
;
go

create nonclustered index ncl_ISBN on Book.Copy (ISBN)
;
go

create nonclustered index ncl_LastName on Member.Member (LastName)
;
go

create nonclustered index ncl_Author on Book.Title (Author)
;
go



/*	create Library_KVC database
	Script Date: September 12, 2018
	Developed by: 
	Kelly Greyling
	Vahe Hakobyan
	Carlos Coloma
*/

-- switch to the master database
use master
;
go 



/*syntax: create object_type object name */
create database Library_KVC
on primary 
(
	-- rows data file name
	name = 'Library_KVC',
	-- rows data path and file name
	filename = 'C:\KVC_Databases\Library_KVC.mdf',
	-- rows data file size
	size = 12MB,
	-- rows data file growth
	filegrowth = 2MB,
	-- maximum size of rows data file
	maxsize = 100MB
)
-- log file
log on
(
	-- log filename
	name = 'Library_KVC_log',
	-- log path and filename
	filename = 'C:\KVC_Databases\Library_KVC_log.ldf',
	-- log file size
	size = 3MB,
	-- log file growth
	filegrowth = 10%,
	-- maximum size of log file
	maxsize = 25MB
)
;
go
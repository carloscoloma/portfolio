package com.coloma.carlos.projectmathquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.coloma.carlos.projectmathquiz.Model.QuestionAndAnswer;

import java.io.Serializable;
import java.util.ArrayList;

public class ShowResult extends AppCompatActivity implements View.OnClickListener {

    TextView textViewResults, textViewScoreValue;
    ArrayList<QuestionAndAnswer> listOfQuestionAndAnswers;
    EditText editTextRegisterName;
    RadioButton radioButtonAll, radioButtonRight, radioButtonWrong, radioButtonSortA,radioButtonSortD;
    Button buttonShow, buttonBack;
    int rightAnswers = 0;
    String resultsStr, nameSentBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(MainPage.themeColor[MainPage.themeSelected]);
        setContentView(R.layout.activity_show_result);
        initialize();

    }

    private void initialize() {
        editTextRegisterName = findViewById(R.id.editTextRegisterName);

        textViewResults = findViewById(R.id.textViewResults);
        textViewScoreValue = findViewById(R.id.textViewScoreValue);

        radioButtonAll = findViewById(R.id.radioButtonAll);
        radioButtonAll.setOnClickListener(this);

        radioButtonRight = findViewById(R.id.radioButtonRight);
        radioButtonRight.setOnClickListener(this);

        radioButtonWrong = findViewById(R.id.radioButtonWrong);
        radioButtonWrong.setOnClickListener(this);

        radioButtonSortA = findViewById(R.id.radioButtonSortA);
        radioButtonSortA.setOnClickListener(this);

        radioButtonSortD = findViewById(R.id.radioButtonSortD);
        radioButtonSortD.setOnClickListener(this);

        buttonShow = findViewById(R.id.buttonShow);
        buttonShow.setOnClickListener(this);

        buttonBack = findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(this);


        Bundle bundle = getIntent().getBundleExtra("intentExtra");
        Serializable bundledListOfQuestionAndAnswers = bundle.getSerializable("listOfQuestionAndAnswers");
//        nameSentBack = bundle.getString("nameSentBack");

        listOfQuestionAndAnswers = (ArrayList<QuestionAndAnswer>) bundledListOfQuestionAndAnswers;

//        showListOfQuestionAndAnswers();
        showScore();
;
    }

    private void showListOfQuestionAndAnswers() {
        for (QuestionAndAnswer oneQA : listOfQuestionAndAnswers) {
            resultsStr = resultsStr + oneQA;
            resultsStr += "\n";
        }
    }

    private void showListOfRightQuestionAndAnswers() {
        for (QuestionAndAnswer oneQA : listOfQuestionAndAnswers) {
            if (oneQA.isRight()){
                resultsStr = resultsStr + oneQA;
                resultsStr += "\n";
            }
        }
    }

    private void showListOfWrongQuestionAndAnswers() {
        for (QuestionAndAnswer oneQA : listOfQuestionAndAnswers) {
            if (!oneQA.isRight()){
                resultsStr = resultsStr + oneQA;
                resultsStr += "\n";
            }
        }
    }

    private void showScore() {
        rightAnswers = 0;
            for (QuestionAndAnswer oneQA : listOfQuestionAndAnswers) {
                if (oneQA.isRight()){
                    rightAnswers ++;
                }
            }
        textViewScoreValue.setText(String.valueOf((100*rightAnswers)/QuestionAndAnswer.instanceCounter) + "%");
    }

    private void showListSelected() {
        textViewResults.setText(resultsStr);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.radioButtonAll:
                resultsStr = "";
                showListOfQuestionAndAnswers();
                break;
            case R.id.radioButtonRight:
                resultsStr = "";
                showListOfRightQuestionAndAnswers();
                break;
            case R.id.radioButtonWrong:
                resultsStr = "";
                showListOfWrongQuestionAndAnswers();
                break;
            case R.id.radioButtonSortA:
                resultsStr = "";
                showListOfRightQuestionAndAnswers();
                showListOfWrongQuestionAndAnswers();
                break;
            case R.id.radioButtonSortD:
                resultsStr = "";
                showListOfWrongQuestionAndAnswers();
                showListOfRightQuestionAndAnswers();
                break;
            case R.id.buttonShow:
                showListSelected();
                break;
            case R.id.buttonBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra("newTitle" , editTextRegisterName.getText().toString() + " your score: " + textViewScoreValue.getText().toString() );
        setResult(RESULT_OK, intent);
        finish();
    }
}

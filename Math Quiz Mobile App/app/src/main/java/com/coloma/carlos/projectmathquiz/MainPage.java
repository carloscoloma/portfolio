package com.coloma.carlos.projectmathquiz;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.coloma.carlos.projectmathquiz.Model.QuestionAndAnswer;

import java.io.Serializable;
import java.util.ArrayList;

public class MainPage extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    boolean nightMode = false;
    MenuItem menuLightDark;
    Menu top_menu;
    static int themeSelected = 0;
    Button button0, button1, button2, button3, button4,
            button5, button6, button7, button8, button9,
            buttonDot, buttonSign,
            buttonGenerate, buttonValidate, buttonClear,
            buttonScore, buttonFinish;

    TextView textViewAnswer, textViewQuestion, textViewName;

    static int[] themeColor = new int[]{R.style.AppTheme_GREEN,
            R.style.AppTheme_RED,
            R.style.AppTheme_PINK,
            R.style.AppTheme_PURPLE,
            R.style.AppTheme_BLUE,
            R.style.AppTheme_BLUEGREY};
    boolean inMenuThemeColor = false;

    String answerInputStr = "";
    boolean answerDot = false, answerNegative = false;
    QuestionAndAnswer currentQuestionAndAnswer;
    boolean questionGenerated = false, answerValidated = false;;

    ArrayList<QuestionAndAnswer> listOfQuestionAndAnswers;

    String nameSentBack ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("*****************************************************onCreate+++++++++++");

        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            System.out.println("*****************************************************before setTheme+++++++++++: " + themeSelected);
            themeSelected = savedInstanceState.getInt("themeSelected");
            nightMode = savedInstanceState.getBoolean("nightMode");
            inMenuThemeColor = savedInstanceState.getBoolean("inMenuThemeColor");

            answerDot = savedInstanceState.getBoolean("answerDot");
            answerNegative = savedInstanceState.getBoolean("answerNegative");
            questionGenerated = savedInstanceState.getBoolean("questionGenerated");
            answerValidated = savedInstanceState.getBoolean("answerValidated");
            Serializable bundledCurrentQuestionAndAnswer = savedInstanceState.getSerializable("bundledCurrentQuestionAndAnswer");
            currentQuestionAndAnswer = (QuestionAndAnswer) bundledCurrentQuestionAndAnswer;
            answerInputStr = savedInstanceState.getString("answerInputStr");
            Serializable bundledListOfQuestionAndAnswers = savedInstanceState.getSerializable("listOfQuestionAndAnswers");
            listOfQuestionAndAnswers = (ArrayList<QuestionAndAnswer>) bundledListOfQuestionAndAnswers;

            nameSentBack = savedInstanceState.getString("nameSentBack");

            this.setTheme(themeColor[themeSelected]);
            System.out.println("*****************************************************after setTheme+++++++++++: " + themeSelected);
        }

//
        setContentView(R.layout.activity_main_page);

//        if (savedInstanceState != null) {
//            nightMode = savedInstanceState.getBoolean("nightMode");
//        }
        initialize();

    }


    private void initialize() {
        System.out.println("*****************************************************initialize+++++++++++");

        listOfQuestionAndAnswers = new ArrayList<>();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        themeColor = new int[]{R.style.AppTheme_GREEN,
//                R.style.AppTheme_RED,
//                R.style.AppTheme_PINK,
//                R.style.AppTheme_PURPLE,
//                R.style.AppTheme_BLUE,
//                R.style.AppTheme_BLUEGREY};

//        setNightMode();

        if (inMenuThemeColor) {
            changeColorTheme();
        }

        textViewAnswer = findViewById(R.id.textViewAnswer);
        textViewQuestion = findViewById(R.id.textViewQuestion);
        textViewName = findViewById(R.id.textViewName);

        button0 = findViewById(R.id.button0);
        button0.setOnClickListener(this);

        button1 = findViewById(R.id.button1);
        button1.setOnClickListener(this);

        button2 = findViewById(R.id.button2);
        button2.setOnClickListener(this);

        button3 = findViewById(R.id.button3);
        button3.setOnClickListener(this);

        button4 = findViewById(R.id.button4);
        button4.setOnClickListener(this);

        button5 = findViewById(R.id.button5);
        button5.setOnClickListener(this);

        button6 = findViewById(R.id.button6);
        button6.setOnClickListener(this);

        button7 = findViewById(R.id.button7);
        button7.setOnClickListener(this);

        button8 = findViewById(R.id.button8);
        button8.setOnClickListener(this);

        button9 = findViewById(R.id.button9);
        button9.setOnClickListener(this);

        buttonDot = findViewById(R.id.buttonDot);
        buttonDot.setOnClickListener(this);

        buttonSign = findViewById(R.id.buttonSign);
        buttonSign.setOnClickListener(this);

        buttonGenerate = findViewById(R.id.buttonGenerate);
        buttonGenerate.setOnClickListener(this);

        buttonValidate = findViewById(R.id.buttonValidate);
        buttonValidate.setOnClickListener(this);

        buttonClear = findViewById(R.id.buttonClear);
        buttonClear.setOnClickListener(this);

        buttonScore = findViewById(R.id.buttonScore);
        buttonScore.setOnClickListener(this);

        buttonFinish = findViewById(R.id.buttonFinish);
        buttonFinish.setOnClickListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        System.out.println("*****************************************************onCreateOptionsMenu+++++++++++");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_menu, menu);
        top_menu = menu;
//        setNightMode();
        prepareNightMode();

//        if (inMenuThemeColor) {
//            changeColorTheme();
//        }
//        inMenuThemeColor = false;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        System.out.println("*****************************************************onOptionsItemSelected+++++++++++");
        switch (item.getItemId()){
            case R.id.menuLightDark:

//                prepareNightMode();
                setNightMode();
                nightMode = !nightMode;

//                toolbar.setBackgroundColor();
                return true;
            case R.id.menuColorTheme:
//                Toast.makeText(this, "menu1", Toast.LENGTH_SHORT).show();
                System.out.println("*****************************************************selected menuColorTheme+++++++++++");
                changeColorTheme();
                // To change theme just put your theme id.
//                int theme = getThemeFromPreferences(); // R.style.AppTheme_RED
//store your theme id in preference
//                saveThemeInPreferences(R.style.AppTheme_RED);
//recreate this activity to see changes
//                setTheme(R.style.AppTheme_RED);
//                this.recreate();
//                setNightMode();
                return true;
//            case R.id.menu2:
//                Toast.makeText(this, "menu2", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.menu2_1:
//                Toast.makeText(this, "menu2_1", Toast.LENGTH_SHORT).show();
//                return true;
//            case R.id.menu2_2:
//                Toast.makeText(this, "menu2_2", Toast.LENGTH_SHORT).show();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void prepareNightMode() {
        System.out.println("*****************************************************prepareNightMode+++++++++++");
        String textColor;
        if (!nightMode){
            top_menu.getItem(0).setIcon(R.drawable.ic_lightbulb);
//            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//                    (R.color.colorPrimary);
        }
        else{
            top_menu.getItem(0).setIcon(R.drawable.ic_lightbulbdark);
//            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }


    }

    private void setNightMode() {
        System.out.println("*****************************************************setNightMode+++++++++++");
        if (!nightMode){
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else{
            getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    public void changeColorTheme() {
//        Toast.makeText(this, "Button 3", Toast.LENGTH_SHORT).show();
        System.out.println("*****************************************************changeColorTheme+++++++++++");
        TextView textViewMenu = new TextView(MainPage.this);
        textViewMenu.setText("Select the new color");
        textViewMenu.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewMenu.setPadding(20, 30, 20, 30);
        textViewMenu.setTextSize(20F);
        textViewMenu.setBackgroundColor(((ColorDrawable) toolbar.getBackground()).getColor());
        textViewMenu.setTextColor(Color.WHITE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCustomTitle(textViewMenu)
        //type 3
//        builder.setTitle()
                .setCancelable(true) // dialog will not be closed if clicked outside
                .setSingleChoiceItems(new String[] {"Green", "Red", "Pink", "Purple", "Blue", "Blue Grey"},
                        themeSelected, // default selected if -1 = none
                        new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                themeSelected = which;
//                                switch (which){
//                                    case 0:
//                                        themeSelected = R.style.AppTheme;
//                                        break;
//                                    case 1:
//                                        themeSelected = R.style.AppTheme_RED;
//                                        break;
//                                    case 2:
//                                        themeSelected = R.style.AppTheme_PINK;
//                                        break;
//                                    case 3:
//                                        themeSelected = R.style.AppTheme_BLUE;
//                                        break;
////                                        themeUtils.changeToTheme(this, themeUtils.BLACK);
////                                        break;
////                                    case R.id.bluebutton:
////                                        themeUtils.changeToTheme(this, themeUtils.BLUE);
////                                        break;
//                                }
//                                Toast.makeText(MainPage.this, "selected = " + themeSelected,
//                                        Toast.LENGTH_SHORT).show();
                                System.out.println("*****************************************************applyStyle+++++++++++");
//                                setNightMode();
//                                getTheme().applyStyle(themeSelected, true);

                                MainPage.this.recreate();

//                                MainPage.this.setTheme(themeSelected);
//                                MainPage.this.finish();
//                                MainPage.this.startActivity(new Intent(MainPage.this, MainPage.this.getClass()));
                            }
                        })
//        ;
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                         inMenuThemeColor = false;

                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                inMenuThemeColor = false;
            }
        });
        builder.show();
        inMenuThemeColor = true;
//        inMenuThemeColor = false;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        System.out.println("*****************************************************onSaveInstanceState+++++++++++");
        savedInstanceState.putBoolean("nightMode", nightMode);
        savedInstanceState.putBoolean("inMenuThemeColor", inMenuThemeColor);
//        savedInstanceState.putDouble("myDouble", 1.9);
        savedInstanceState.putInt("themeSelected", themeSelected);
//        savedInstanceState.putString("MyString", "Welcome back to Android");

        savedInstanceState.putBoolean("answerDot", answerDot);
        savedInstanceState.putBoolean("answerNegative", answerNegative);
        savedInstanceState.putBoolean("questionGenerated", questionGenerated);
        savedInstanceState.putBoolean("answerValidated", answerValidated);
        savedInstanceState.putString("answerInputStr", answerInputStr);
        savedInstanceState.putSerializable("bundledCurrentQuestionAndAnswer", currentQuestionAndAnswer);

        savedInstanceState.putSerializable("listOfQuestionAndAnswers", listOfQuestionAndAnswers);

        savedInstanceState.putString("nameSentBack", nameSentBack);
        super.onSaveInstanceState(savedInstanceState);
    }

//onRestoreInstanceState

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        System.out.println("*****************************************************onRestoreInstanceState+++++++++++");
        super.onRestoreInstanceState(savedInstanceState);
        themeSelected = savedInstanceState.getInt("themeSelected");
        nightMode = savedInstanceState.getBoolean("nightMode");
        inMenuThemeColor = savedInstanceState.getBoolean("inMenuThemeColor");

        answerDot = savedInstanceState.getBoolean("answerDot");
        answerNegative = savedInstanceState.getBoolean("answerNegative");
        questionGenerated = savedInstanceState.getBoolean("questionGenerated");
        answerValidated = savedInstanceState.getBoolean("answerValidated");
        Serializable bundledCurrentQuestionAndAnswer = savedInstanceState.getSerializable("bundledCurrentQuestionAndAnswer");
        currentQuestionAndAnswer = (QuestionAndAnswer) bundledCurrentQuestionAndAnswer;
        answerInputStr = savedInstanceState.getString("answerInputStr");

        Serializable bundledListOfQuestionAndAnswers = savedInstanceState.getSerializable("listOfQuestionAndAnswers");
        listOfQuestionAndAnswers = (ArrayList<QuestionAndAnswer>) bundledListOfQuestionAndAnswers;

        nameSentBack = savedInstanceState.getString("nameSentBack");
        if (!nameSentBack.equals("")){
            textViewName.setText(nameSentBack);
        }

        if (questionGenerated){
            textViewQuestion.setText(currentQuestionAndAnswer.questionToString());
        }
        if (answerValidated){
            textViewQuestion.setText(currentQuestionAndAnswer.toString());
        }
        if (answerNegative){
            textViewAnswer.setText("-" + answerInputStr);
        }else{
            textViewAnswer.setText(answerInputStr);
        }

//        nightMode = savedInstanceState.getBoolean("nightMode");
//        themeSelected = savedInstanceState.getInt("themeSelected");

//        setNightMode();
//        double myDouble = savedInstanceState.getDouble("myDouble");

//        String myString = savedInstanceState.getString("MyString");
    }

    @Override
    public void onClick(View v) {
//        answerInputStr;
//        answerInput;
        switch (v.getId()){
            case R.id.buttonGenerate:
                generateNewQuestion();
                return;
            case R.id.buttonValidate:
                if (!questionGenerated) return;
                if (answerInputStr.length() == 0) return;
                validateAnswer();
                return;
            case R.id.buttonScore:
                processShowAll();
                return;
            case R.id.buttonFinish:
                finish();
                return;
        }
        if (!questionGenerated) {
            return;
        }

            switch (v.getId()){
                case R.id.button0:
                    if (!answerInputStr.equals("0")) {
                        answerInputStr += button0.getText().toString();
                    }
                    break;
                case R.id.button1:
                    answerInputStr += button1.getText().toString();
                    break;
                case R.id.button2:
                    answerInputStr += button2.getText().toString();
                    break;
                case R.id.button3:
                    answerInputStr += button3.getText().toString();
                    break;
                case R.id.button4:
                    answerInputStr += button4.getText().toString();
                    break;
                case R.id.button5:
                    answerInputStr += button5.getText().toString();
                    break;
                case R.id.button6:
                    answerInputStr += button6.getText().toString();
                    break;
                case R.id.button7:
                    answerInputStr += button7.getText().toString();
                    break;
                case R.id.button8:
                    answerInputStr += button8.getText().toString();
                    break;
                case R.id.button9:
                    answerInputStr += button9.getText().toString();
                    break;
                case R.id.buttonDot:
                    if (!answerDot) {
                        if (answerInputStr.equals("")) {
                            answerInputStr += "0";
                        }
                        answerInputStr += buttonDot.getText().toString();
                        answerDot = true;
                    }
                    break;
                case R.id.buttonSign:
                    answerNegative = !answerNegative;
                    break;
                case R.id.buttonClear:
                    clearAnswer();
                    return;
            }

        if ((answerInputStr.length() == 2)&&
                ((answerInputStr.charAt(0)+"").equals("0"))&&
                (!(answerInputStr.charAt(1)+"").equals("."))) {
            answerInputStr = answerInputStr.charAt(1)+"";
        }

        if ((answerInputStr.length()>12)){
            answerInputStr = answerInputStr.substring(0,12);
        }
        if (answerNegative){
            textViewAnswer.setText("-" + answerInputStr);
        }else{
            textViewAnswer.setText(answerInputStr);
        }

    }

    public void generateNewQuestion(){
        if (questionGenerated) return;
        clearAnswer();
        currentQuestionAndAnswer = new QuestionAndAnswer();
        textViewQuestion.setText(currentQuestionAndAnswer.questionToString());
        questionGenerated = true;
        answerValidated = false;
    }

    public void validateAnswer(){
        answerValidated = true;
        currentQuestionAndAnswer.setAnswer(Float.valueOf(textViewAnswer.getText().toString()));
        textViewQuestion.setText(currentQuestionAndAnswer.toString());
        questionGenerated = false;
        listOfQuestionAndAnswers.add(currentQuestionAndAnswer);
    }

    public void clearAnswer(){
        answerInputStr = "";
        answerDot = false;
        answerNegative = false;
        textViewAnswer.setText(answerInputStr);
    }

    private void processShowAll() {
        if (!answerValidated) return;
        Bundle bundle = new Bundle();
        bundle.putSerializable("listOfQuestionAndAnswers", listOfQuestionAndAnswers);

//        bundle.putString("nameSentBack", nameSentBack);

        Intent intent = new Intent(this, ShowResult.class);
        intent.putExtra("intentExtra", bundle);
//        startActivity(intent);

        startActivityForResult(intent, 123);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == 123) && (resultCode == RESULT_OK)){
            nameSentBack = data.getStringExtra("newTitle");
            textViewName.setText(nameSentBack);
        }
    }
}

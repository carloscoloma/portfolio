package com.coloma.carlos.projectmathquiz.Model;

import java.io.Serializable;
import java.util.Random;

public class QuestionAndAnswer implements Serializable {

    private int firstNumber;
    private int secondNumber;
    private int operation;
    private float answer;
    private boolean isRight;
    public static int instanceCounter = 0;
    private static int score = 0;
    private int id = 0;
    private float correctAnswer;


    public QuestionAndAnswer() {
        generateQuestion();
    }

    private void generateQuestion() {
        int max = 10;
        int min = -10;
        operation = generateNumber( 0, 3);
        firstNumber = generateNumber( min, max);
        secondNumber = generateNumber( min, max);
        while ((operation == 3) && (secondNumber == 0)){
            secondNumber = generateNumber( min, max);
        }
        instanceCounter++;
        id = instanceCounter;
    }

    private int generateNumber(int miniInt, int maxInt){
        Random rnd = new Random();
        int number = rnd.nextInt(maxInt + 1 -miniInt) + miniInt;
        return number;
    }

    public void setAnswer(float answer) {
        this.answer = (float) (Math.round( (answer) * 10) / 10.0);
        checkAnswer();
    }

    private void checkAnswer(){
        switch (operation){
            case 0:
                correctAnswer = firstNumber + secondNumber;
                break;
            case 1:
                correctAnswer = firstNumber - secondNumber;
                break;
            case 2:
                correctAnswer = firstNumber * secondNumber;
                break;
            case 3:
                correctAnswer = (float) (Math.round(((float) firstNumber / secondNumber) * 10) / 10.0);
                break;
        }
        this.isRight = (this.answer == correctAnswer);
        if (this.isRight){
            this.score++;
        }
    }

    public int getFirstNumber() {
        return firstNumber;
    }

    public int getSecondNumber() {
        return secondNumber;
    }

    public int getOperation() {
        return operation;
    }

    public float getAnswer() {
        return answer;
    }

    public boolean isRight() {
        return isRight;
    }

    public String questionToString() {
        String operationStr = "";
        switch (operation) {
            case 0:
                operationStr = " + ";
                break;
            case 1:
                operationStr = " - ";
                break;
            case 2:
                operationStr = " * ";
                break;
            case 3:
                operationStr = " / ";
                break;
        }
        String firstNumberStr = (firstNumber < 0) ?
                "(" + String.valueOf(firstNumber) + ")" : String.valueOf(firstNumber);
        String secondNumberStr = (secondNumber < 0) ?
                "(" + String.valueOf(secondNumber) + ")" : String.valueOf(secondNumber);
        return
//                "Question #" + this.id +
                firstNumberStr +
                operationStr +
                secondNumberStr;
    }

    public String questionAndCorrectAnswerToString() {
        String correctAnswerStr =
//                (correctAnswer < 0) ?
//                "(" + String.valueOf(correctAnswer) + ")" :
                        String.valueOf(correctAnswer);
        return questionToString() +
                " = " +
                correctAnswerStr;
    }

    @Override
    public String toString() {
        String answerStr =
//                (answer < 0) ?
//                "(" + String.valueOf(answer) + ")" :
                String.valueOf(answer);
        String rightStr = (isRight) ?
                "right" : "wrong";
        return questionAndCorrectAnswerToString() +
                "\nYour answer = " + answerStr +
                " is " + rightStr ;
    }
}

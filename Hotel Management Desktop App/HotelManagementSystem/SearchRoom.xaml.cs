﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro;
using MahApps.Metro.Controls;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for SearchRoom.xaml
    /// </summary>
    public partial class SearchRoom : MetroWindow
    {
        //DisplayRoomsFoundDatabase dbRoomsFound1;
        int Adults = 1;
        int Children = 0;
        int Rooms = 1;
        int flyOutFlipViewRoomGroupSelectedIndex = 0;
        int flyOutFlipViewRoomGroupMaxIndex = 0;
        Dictionary<RoomGroup, int> DictionaryRoomGroupsAvailableAndQuantity = new Dictionary<RoomGroup, int>();
        List<Picture> currentPicturesInRoomGroupSelected = new List<Picture>();
        bool selectionUp = true;
        int selectionCase = 0;
        RoomGroup roomGroupSelected;

        public SearchRoom()
        {
            InitializeComponent();
            Globals.ctx = new HotelContext();
            //dbRoomsFound1 = new DisplayRoomsFoundDatabase();
            dtpkCheckIn.SelectedDate = DateTime.Now;
            dtpkCheckIn.DisplayDateStart = DateTime.Now;
            dtpkCheckIn.DisplayDateEnd = DateTime.Now.AddYears(1);
            dtpkCheckIn.SelectedDateFormat = DatePickerFormat.Long;
            dtpkCheckOut.SelectedDate = dtpkCheckIn.DisplayDate.AddDays(1);
            dtpkCheckOut.DisplayDateStart = dtpkCheckIn.DisplayDate.AddDays(1);
            dtpkCheckOut.DisplayDateEnd = DateTime.Now.AddYears(1).AddDays(1);
            dtpkCheckOut.SelectedDateFormat = DatePickerFormat.Long;
            btAdultsDown.IsEnabled = false;
            btAdultsDown.Foreground = Brushes.LightGray;
            btChildrenDown.IsEnabled = false;
            btChildrenDown.Foreground = Brushes.LightGray;
            //btRoomsDown.IsEnabled = false;
        }

        private void btAdultsUp_Click(object sender, RoutedEventArgs e)
        {
            Adults++;
            tbAdults.Text = Adults + "";
            btAdultsDown.IsEnabled = true;
            btAdultsDown.SetResourceReference(Control.ForegroundProperty, "AccentColorBrush");
            //btAdultsDown.Foreground = Brushes.White;
        }

        private void btAdultsDown_Click(object sender, RoutedEventArgs e)
        {
            if (Adults > 1)
            {
                Adults--;
            }
            if (Adults <= 1)
            {
                Adults = 1;
                btAdultsDown.IsEnabled = false;
                btAdultsDown.Foreground = Brushes.LightGray;
            }
            tbAdults.Text = Adults + "";
        }

        private void btChildrenUp_Click(object sender, RoutedEventArgs e)
        {
            Children++;
            tbChildren.Text = Children + "";
            btChildrenDown.IsEnabled = true;
            btChildrenDown.SetResourceReference(Control.ForegroundProperty, "AccentColorBrush");
        }

        private void btChildrenDown_Click(object sender, RoutedEventArgs e)
        {
            if (Children > 0)
            {
                Children--;
            }
            if (Children <= 0)
            {
                Children = 0;
                btChildrenDown.IsEnabled = false;
                btChildrenDown.Foreground = Brushes.LightGray;
            }
            tbChildren.Text = Children + "";
        }

        //private void btRoomsUp_Click(object sender, RoutedEventArgs e)
        //{
        //    Rooms++;
        //    tbRooms.Text = Rooms + "";
        //    btRoomsDown.IsEnabled = true;
        //}

        //private void btRoomsDown_Click(object sender, RoutedEventArgs e)
        //{
        //    if (Rooms > 1)
        //    {
        //        Rooms--;
        //    }
        //    if (Rooms <= 1)
        //    {
        //        Rooms = 1;
        //        btRoomsDown.IsEnabled = false;
        //    }
        //    tbRooms.Text = Rooms + "";
        //}

        private void dtpkCheckIn_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(dtpkCheckIn.SelectedDate < dtpkCheckOut.SelectedDate))
            {
                dtpkCheckOut.SelectedDate = ((DateTime)dtpkCheckIn.SelectedDate).AddDays(1);
            }
            dtpkCheckOut.DisplayDateStart = ((DateTime)dtpkCheckIn.SelectedDate).AddDays(1);
        }

        private void dtpkCheckIn_CalendarClosed(object sender, RoutedEventArgs e)
        {

        }

        private void btSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvRoomsBrowser.SelectedIndex = -1;
                int adultsRequested, childrenRequested;

                if (!int.TryParse(tbAdults.Text, out adultsRequested) || (adultsRequested < 1))
                {
                    MessageBox.Show(this, "Please enter a valid value for the quantity of Adults.", "Incorrect value for Adults", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    lvRoomsBrowser.ItemsSource = null;
                    return;
                }
                if (!int.TryParse(tbChildren.Text, out childrenRequested) || (childrenRequested < 0))
                {
                    MessageBox.Show(this, "Please enter a valid value for the quantity of Children.", "Incorrect value for Children", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    lvRoomsBrowser.ItemsSource = null;
                    return;
                }
                SortedDictionary<int, Room> sortedDictionaryRoomsAvailable = new SortedDictionary<int, Room>();

                var roomsTotal = (from r in Globals.ctx.Rooms select r).ToList();
                foreach (Room r in roomsTotal)
                {
                    sortedDictionaryRoomsAvailable.Add(r.Id, r);
                }

                var reservationList = (from r in Globals.ctx.Reservations where ((r.CheckIn <= dtpkCheckIn.SelectedDate) && (r.CheckOut >= dtpkCheckOut.SelectedDate) && (r.Cancelled == false)) select r).ToList();
                foreach (Reservation reservation in reservationList)
                {
                    if (sortedDictionaryRoomsAvailable.ContainsKey(reservation.RoomId))
                    {
                        sortedDictionaryRoomsAvailable.Remove(reservation.RoomId);
                        //var roomReserved = (from r in Globals.ctx.Rooms where (r.Id == reservation.RoomId) select r).FirstOrDefault();
                        //if (!(roomReserved is null))
                        //{

                        //}
                    }
                }
                if ((sortedDictionaryRoomsAvailable is null) || (sortedDictionaryRoomsAvailable.Count < 1))
                {
                    MessageBox.Show(this, "Sorry, there is no room availability for the dates requested", "No rooms available", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    lvRoomsBrowser.ItemsSource = null;
                    return;
                }

                //List<Room> roomsAvailableList = new List<Room>();
                //foreach (KeyValuePair<int, Room> roomIdAndRoom in sortedDictionaryRoomsAvailable)
                //{
                //    roomsAvailableList.Add(roomIdAndRoom.Value);
                //}
                //lvRoomsBrowser.ItemsSource = roomsAvailableList;

                DictionaryRoomGroupsAvailableAndQuantity = new Dictionary<RoomGroup, int>();
                //SortedDictionary<int, int> sortedDictionaryRoomGroupsAvailableAndQuantity = new SortedDictionary<int, int>();

                var roomGroupsTotal = (from rg in Globals.ctx.RoomGroups select rg).ToList();
                var roomGroupsWithCapacityRequested = (from rg in roomGroupsTotal where ((rg.AdultCapacity >= adultsRequested) && (rg.ChildCapacity >= childrenRequested)) select rg).ToList();

                if ((roomGroupsWithCapacityRequested is null) || (roomGroupsWithCapacityRequested.Count < 1))
                {
                    MessageBox.Show(this, "Sorry, there is no room available for the quantity of adults and children requested", "No rooms available", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    lvRoomsBrowser.ItemsSource = null;
                    return;
                }

                //foreach (RoomGroup rg in roomGroupsWithCapacityRequested)
                //{
                //    Boolean rgHasRoomAvailable = false;
                //    int quantityOfRoomsAvailableInrg = 0;
                //    var roomsInRoomGroupWithCapacityRequested = Globals.ctx.RoomGroups.FirstOrDefault(y => y.Id == rg.Id).Rooms.ToList();
                //    foreach (Room r in roomsInRoomGroupWithCapacityRequested)
                //    {
                //        if (sortedDictionaryRoomsAvailable.ContainsKey(r.Id))
                //        {
                //            rgHasRoomAvailable = true;
                //            quantityOfRoomsAvailableInrg++;
                //        }
                //    }
                //    if (rgHasRoomAvailable)
                //    {
                //        sortedDictionaryRoomGroupsAvailableAndQuantity.Add(rg.Id, quantityOfRoomsAvailableInrg);
                //    }
                //}

                //if ((sortedDictionaryRoomGroupsAvailableAndQuantity is null) || (sortedDictionaryRoomGroupsAvailableAndQuantity.Count < 1))
                //{
                //    MessageBox.Show(this, "Sorry, there is no package availability for the dates requested", "No rooms available", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                //    lvRoomsBrowser.ItemsSource = null;
                //    return;
                //}

                //lvRoomsBrowser.ItemsSource = sortedDictionaryRoomGroupsAvailableAndQuantity;

                foreach (RoomGroup rg in roomGroupsWithCapacityRequested)
                {
                    Boolean rgHasRoomAvailable = false;
                    int quantityOfRoomsAvailableInrg = 0;
                    var roomsInRoomGroupWithCapacityRequested = Globals.ctx.RoomGroups.FirstOrDefault(y => y.Id == rg.Id).Rooms.ToList();
                    foreach (Room r in roomsInRoomGroupWithCapacityRequested)
                    {
                        if (sortedDictionaryRoomsAvailable.ContainsKey(r.Id))
                        {
                            rgHasRoomAvailable = true;
                            quantityOfRoomsAvailableInrg++;
                        }
                    }
                    if (rgHasRoomAvailable)
                    {

                        DictionaryRoomGroupsAvailableAndQuantity.Add(Globals.ctx.RoomGroups.FirstOrDefault(x => x.Id == rg.Id), quantityOfRoomsAvailableInrg);
                        //DictionaryRoomGroupsAvailableAndQuantity.Keys.First().Picturecollection.First();
                    }
                }

                if ((DictionaryRoomGroupsAvailableAndQuantity is null) || (DictionaryRoomGroupsAvailableAndQuantity.Count < 1))
                {
                    MessageBox.Show(this, "Sorry, there is no package availability for the dates requested", "No rooms available", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    lvRoomsBrowser.ItemsSource = null;
                    return;
                }

                List<ListViewForRoomGroup> listViewForRoomGroup = new List<ListViewForRoomGroup>();
                foreach (KeyValuePair<RoomGroup, int> roomGroupAndQty in DictionaryRoomGroupsAvailableAndQuantity)
                {
                    listViewForRoomGroup.Add(new ListViewForRoomGroup(roomGroupAndQty.Key, roomGroupAndQty.Value));
                }

                lvRoomsBrowser.ItemsSource = listViewForRoomGroup;
                //lvRoomsBrowser.ItemsSource = DictionaryRoomGroupsAvailableAndQuantity;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        //private void lvRoomsBrowser_Selected(object sender, RoutedEventArgs e)
        //{
        //    MessageBox.Show(this, "Room Selected", "Selected", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        //    //lvRoomsBrowser.ItemsSource = null;
        //    return;
        //}

        private void lvRoomsBrowser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lbQtyRoomsLeft.Content = null;
            imRoomSelected.Source = null;
            flyOutFlipViewRoomGroupSelectedIndex = 0;
            flyOutFlipViewRoomGroupMaxIndex = 0;
            currentPicturesInRoomGroupSelected = null;
            selectionUp = true;
            selectionCase = 0;
            if (lvRoomsBrowser.SelectedIndex < 0)
            {
                flyOutRoomGroupSelected.IsOpen = false;
                return;
            }
            roomGroupSelected = DictionaryRoomGroupsAvailableAndQuantity.Keys.ToList()[lvRoomsBrowser.SelectedIndex];
            flyOutRoomGroupSelected.IsOpen = true;
            //lvRoomsBrowser.
            //RoomGroup roomGroupSelected = (lvRoomsBrowser.SelectedItems).
            flyOutRoomGroupSelected.Header = roomGroupSelected.Name;
            lbPrice.Content = string.Format("$ {0:0}", roomGroupSelected.Price);
            tbDescription.Text = roomGroupSelected.Description;
            int roomsAvailable = DictionaryRoomGroupsAvailableAndQuantity.Values.ToList()[lvRoomsBrowser.SelectedIndex];
            if (roomsAvailable <6)
            {
                lbQtyRoomsLeft.Content = "Only " + roomsAvailable  + " Rooms left!";
            }
            
            if (roomGroupSelected.Picturecollection.Count > 0)
            {
                currentPicturesInRoomGroupSelected = (from p in roomGroupSelected.Picturecollection select p).ToList();
                flyOutFlipViewRoomGroupMaxIndex = currentPicturesInRoomGroupSelected.Count;
                MemoryStream memoryStream = new MemoryStream(currentPicturesInRoomGroupSelected[flyOutFlipViewRoomGroupSelectedIndex].Photograph);
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.EndInit();
                imRoomSelected.Source = bitmapImage;
            }
        }

        private void btReserve_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddNewCustomerDialog dlg = new AddNewCustomerDialog(this);
                if (dlg.ShowDialog() == true) //do not use show, or there will be 2 windows  in the program
                {
                    Customer customer = new Customer(dlg.AnswerName, "", dlg.AnswerEmail, dlg.AnswerTelephone, dlg.AnswerAddress);
                    var sameCustomerInDB = (from c in Globals.ctx.Customers
                                            where (c.Name.ToLower() == customer.Name.ToLower()) && (c.Email == customer.Email)
                                            select c).ToList().FirstOrDefault();
                    if (sameCustomerInDB is null)
                    {
                        Globals.ctx.Customers.Add(customer);
                        Globals.ctx.SaveChanges();
                    }
                    else
                    {
                        Globals.ctx.Customers.FirstOrDefault(c => (c.Name.ToLower() == customer.Name.ToLower()) && (c.Email == customer.Email)).Address = dlg.AnswerAddress;
                        Globals.ctx.Customers.FirstOrDefault(c => (c.Name.ToLower() == customer.Name.ToLower()) && (c.Email == customer.Email)).Telephone = dlg.AnswerTelephone;
                        Globals.ctx.SaveChanges();
                    }

                    var reservationRoomIdList = (from r in Globals.ctx.Reservations where ((r.CheckIn <= dtpkCheckIn.SelectedDate) && (r.CheckOut >= dtpkCheckOut.SelectedDate)) select r.RoomId).ToList();
                    int customerId = Globals.ctx.Customers.FirstOrDefault(c => (c.Name.ToLower() == customer.Name.ToLower()) && (c.Email == customer.Email)).Id;
                    var roomSelected = (from r in (Globals.ctx.RoomGroups.FirstOrDefault(rg => rg.Id == roomGroupSelected.Id).Rooms) select r.Id).Except(reservationRoomIdList).ToList();
                    if (roomSelected.Count == 0)
                    {
                        MessageBox.Show(this, "Sorry, there is no room availability for the dates requested", "No rooms available", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        lvRoomsBrowser.ItemsSource = null;
                        return;
                    }
                    int roomIdSelected = roomSelected[0];
                    Reservation reservation = new Reservation(customerId, 1, (DateTime)dtpkCheckIn.SelectedDate, (DateTime)dtpkCheckOut.SelectedDate, false, false, roomIdSelected, roomGroupSelected.Id, false, roomGroupSelected.Price, false);
                    Globals.ctx.Reservations.Add(reservation); // persist - schedule Person to be inserted into database table
                    Globals.ctx.SaveChanges();
                    MessageBox.Show(this, "Thank you for your Reservation! Your Reservation ID: " + reservation.Id, "Reservation Completed", MessageBoxButton.OK, MessageBoxImage.Information);


            //Export to reservation confirmation pdf file
                    string PdfFileName = string.Format("Reservation{0}.pdf", reservation.Id);

                    Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
                    PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(PdfFileName, FileMode.Create));
                    doc.Open();

                /*  
                   iTextSharp.text.Image picture = iTextSharp.text.Image.GetInstance("room.jpg");
                    picture.ScalePercent(25f);
                    picture.SetAbsolutePosition(doc.PageSize.Width - 36f - 72f, doc.PageSize.Height - 36f - 216f);
                    picture.ScaleToFit(50f, 100f);

                    picture.Border = iTextSharp.text.Rectangle.BOX;

                    picture.BorderColor = iTextSharp.text.BaseColor.RED;
                    picture.BorderWidth = 5f;
                    doc.Add(picture);*/



                    iTextSharp.text.Paragraph paragraph1 = new iTextSharp.text.Paragraph(new Phrase( "Welcome to Hotel",new iTextSharp.text.Font(iTextSharp.text.Font.NORMAL, 22, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLUE)));
                    doc.Add(paragraph1);
                    iTextSharp.text.Paragraph paragraph2 = new iTextSharp.text.Paragraph("                         ");
                    doc.Add(paragraph2);

                    PdfPTable table = new PdfPTable(7);

                    PdfPCell cell = new PdfPCell(new Phrase(" Confirmation of Reservation", new iTextSharp.text.Font(iTextSharp.text.Font.NORMAL, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.RED)));
                    cell.BackgroundColor = new iTextSharp.text.BaseColor(200, 200, 200);
                    cell.Colspan = 7;
                    cell.HorizontalAlignment = 1;
                    table.AddCell(cell);

                    table.AddCell("Reservation");
                    table.AddCell("Customer");
                    table.AddCell("Check In");
                    table.AddCell("Check out");
                    table.AddCell("Package");
                    table.AddCell("Bed");
                    table.AddCell("Price");
                    //table.AddCell("Paid");



                    table.AddCell(reservation.Id.ToString());
                    table.AddCell(reservation.CustomerGuest.Name.ToString());
                    table.AddCell(reservation.CheckIn.ToShortDateString());
                    table.AddCell(reservation.CheckOut.ToShortDateString());
                    table.AddCell(reservation.RoomGroupSelected.Name.ToString());
                    table.AddCell(reservation.RoomGroupSelected.BedDescription.ToString());
                    table.AddCell(reservation.Price.ToString());
                    //table.AddCell(reservation.Paid.ToString());


                    doc.Add(table);

                    doc.Close();

                    System.Diagnostics.Process.Start(PdfFileName);

                    lvRoomsBrowser.ItemsSource = null;
                    //RefreshList();
                    //MessageBox.Show("Success!!");
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }

        }


        private void btPhotoNext_Click(object sender, RoutedEventArgs e)
        {
            if (roomGroupSelected.Picturecollection.Count <= 0)
            {
                return;
            }
                if (flyOutFlipViewRoomGroupSelectedIndex < flyOutFlipViewRoomGroupMaxIndex)
            {
                flyOutFlipViewRoomGroupSelectedIndex++;
            }
            if (flyOutFlipViewRoomGroupSelectedIndex >= flyOutFlipViewRoomGroupMaxIndex)
            {
                flyOutFlipViewRoomGroupSelectedIndex = 0;
            }
            MemoryStream memoryStream = new MemoryStream(currentPicturesInRoomGroupSelected[flyOutFlipViewRoomGroupSelectedIndex].Photograph);
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = memoryStream;
            bitmapImage.EndInit();
            imRoomSelected.Source = bitmapImage;
        }

        private void btPhotoPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (roomGroupSelected.Picturecollection.Count <= 0)
            {
                return;
            }
            if (flyOutFlipViewRoomGroupSelectedIndex > 0)
            {
                flyOutFlipViewRoomGroupSelectedIndex--;
            }
            if (flyOutFlipViewRoomGroupSelectedIndex <= 0)
            {
                flyOutFlipViewRoomGroupSelectedIndex = flyOutFlipViewRoomGroupMaxIndex - 1;
            }
            MemoryStream memoryStream = new MemoryStream(currentPicturesInRoomGroupSelected[flyOutFlipViewRoomGroupSelectedIndex].Photograph);
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = memoryStream;
            bitmapImage.EndInit();
            imRoomSelected.Source = bitmapImage;
        }
    }

    public class ListViewForRoomGroup
    {
        public ListViewForRoomGroup(RoomGroup roomGroup, int quantityOfRooms)
        {
            Id = roomGroup.Id;
            Name = roomGroup.Name;
            Description = roomGroup.Description;
            Price = roomGroup.Price;
            Breakfast = roomGroup.Breakfast;
            AdultCapacity = roomGroup.AdultCapacity;
            ChildCapacity = roomGroup.ChildCapacity;
            BedDescription = roomGroup.BedDescription;
            Crib = roomGroup.Crib;
            QuantityOfRooms = quantityOfRooms;
            if (roomGroup.Picturecollection.ToList().Count > 0)
            {
                MainPicture = roomGroup.Picturecollection.ToList()[0];
            }
            
        }
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public bool Breakfast { get; set; }

        public int AdultCapacity { get; set; }

        public int ChildCapacity { get; set; }

        public string BedDescription { get; set; }

        public bool Crib { get; set; }

        public Picture MainPicture { get; set; }

        public int QuantityOfRooms { get; set; }

    }
}

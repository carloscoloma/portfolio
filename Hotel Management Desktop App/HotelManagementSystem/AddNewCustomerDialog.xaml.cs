﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for AddNewCustomerDialog.xaml
    /// </summary>
    public partial class AddNewCustomerDialog : MetroWindow
    {
        public AddNewCustomerDialog(Window parent)
        {
            InitializeComponent();
        }

        private void Button_CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false; //DialogResult is a property inherited from Window
        }

        private void Button_SaveClick(object sender, RoutedEventArgs e)
        {
            if ((tbName.Text.Trim(' ').Length < 1) || (tbName.Text.Trim(' ').Length > 100))
            {
                MessageBox.Show("Name must be between 1 and 100 characteres long");
                return;
            }
            if ((tbAddress.Text.Trim(' ').Length < 1) || (tbAddress.Text.Trim(' ').Length > 100))
            {
                MessageBox.Show("Address must be between 1 and 100 characteres long");
                return;
            }

            this.DialogResult = true;
        }

        public string AnswerName
        {
            get { return tbName.Text.Trim(' '); }
        }

        public string AnswerEmail
        {
            get { return tbEmail.Text.Trim(' '); }
        }

        public string AnswerTelephone
        {
            get { return tbTelephone.Text.Trim(' '); }
        }

        public string AnswerAddress
        {
            get { return tbAddress.Text.Trim(' '); }
        }

        private void tbEmail_LostFocus(object sender, RoutedEventArgs e)
        {
            if ((tbName.Text.Trim(' ').Length < 1) || (tbName.Text.Trim(' ').Length > 100))
            {
                return;
            }
            autocompleteInfo();
        }

        private void tbName_LostFocus(object sender, RoutedEventArgs e)
        {
            if ((tbEmail.Text.Trim(' ').Length < 1) || (tbEmail.Text.Trim(' ').Length > 100))
            {
                return;
            }
            autocompleteInfo();
        }

        private void autocompleteInfo()
        {
            tbName.Text = tbName.Text.Trim(' ');
            tbEmail.Text = tbEmail.Text.Trim(' ');
            var sameCustomerInDB = (from c in Globals.ctx.Customers where (c.Name.ToLower() == tbName.Text.ToLower()) && (c.Email == tbEmail.Text)
                                    select c).ToList().FirstOrDefault();
            if (sameCustomerInDB is null)
            {
                return;
            }
            tbName.Text = sameCustomerInDB.Name;
            tbEmail.Text = sameCustomerInDB.Email;
            tbTelephone.Text = sameCustomerInDB.Telephone;
            tbAddress.Text = sameCustomerInDB.Address;
        }
    }


}

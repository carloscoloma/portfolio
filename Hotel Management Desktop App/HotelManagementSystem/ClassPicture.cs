﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Picture
    {
        [Key]
        public int Id { get; set; }

        [MaxLength, Required]
        public byte[] Photograph { get; set; }

        //public ICollection<PictureInGroup> PictureInGroupCollection { get; set; } //

        public Picture(string imagePath)
        {
            byte[] photograph = System.IO.File.ReadAllBytes(imagePath);
            Photograph = photograph;
        }

        [Obsolete("Only needed for serialization and materialization", true)]
        public Picture() { }

        public Picture(Picture picture)
        {
        }

        public virtual ICollection<RoomGroup> RoomGroups { get; set; }
    }
}

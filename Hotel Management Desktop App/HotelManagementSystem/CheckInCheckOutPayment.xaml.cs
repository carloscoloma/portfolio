﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for CheckInCheckOutPayment.xaml
    /// </summary>
    public partial class CheckInCheckOutPayment : MetroWindow
    {
        ObservableCollection<ListViewForReservations> listViewForReservations = new ObservableCollection<ListViewForReservations>();
        // using ObservableCollection instead of List for problem: https://stackoverflow.com/questions/48680748/listview-not-updating-with-bound-list
        public CheckInCheckOutPayment()
        {
            InitializeComponent();
            //tlCustomerCancelled.Visibility = Visibility.Hidden;
            //tlCustomerPayment.Visibility = Visibility.Hidden;
            lbCustomerName.Content = " ";
            lbCustomerRoom.Content = " ";
            lbCustomerTotalPrice.Content = "For more detail select a reservation from the list";
            tlCustomerCancelled.Visibility = Visibility.Hidden;
            tlCustomerPayment.Visibility = Visibility.Hidden;
            tlCustomerCheckedIn.Visibility = Visibility.Hidden;
            tlCustomerCheckedOut.Visibility = Visibility.Hidden;
        }

        private void lvReservations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //tlCustomerCancelled.Visibility = Visibility.Hidden;
            //tlCustomerPayment.Visibility = Visibility.Hidden;
            

            if (lvReservations.SelectedItem is null)
            {
                lbCustomerName.Content = " ";
                lbCustomerRoom.Content = " ";
                lbCustomerTotalPrice.Content = "For more detail select a reservation from the list";
                tlCustomerCancelled.Visibility = Visibility.Hidden;
                tlCustomerPayment.Visibility = Visibility.Hidden;
                tlCustomerCheckedIn.Visibility = Visibility.Hidden;
                tlCustomerCheckedOut.Visibility = Visibility.Hidden;
                return;
            }
            ListViewForReservations itemSelected = lvReservations.SelectedItem as ListViewForReservations;
            lbCustomerName.Content = "Name: " + itemSelected.CustomerFromReservation.Name;
            lbCustomerRoom.Content = "Room: " + itemSelected.RoomFromReservation.Number + " ";
            int totalDays = (int)(itemSelected.CheckOut - itemSelected.CheckIn).TotalDays;
            lbCustomerTotalPrice.Content = string.Format("Total: $ {0:0.00}", itemSelected.Price * (decimal)totalDays);
            tlCustomerCancelled.Visibility = Visibility.Visible;
            tlCustomerPayment.Visibility = Visibility.Visible;
            tlCustomerCheckedIn.Visibility = Visibility.Visible;
            tlCustomerCheckedOut.Visibility = Visibility.Visible;

            if (!itemSelected.ReservationFromReservation.Cancelled)
            {
                tlCustomerCancelled.Title = "Press to\nCancel";
                icCancelled.Visibility = Visibility.Hidden;
            }
            else
            {
                tlCustomerCancelled.Title = "Canceled";
                icCancelled.Visibility = Visibility.Visible;
            }

            if (!itemSelected.ReservationFromReservation.Paid)
            {
                tlCustomerPayment.Title = "Press for\nPayment";
                icPayment.Visibility = Visibility.Hidden;
            }
            else
            {
                tlCustomerPayment.Title = "Paid";
                icPayment.Visibility = Visibility.Visible;
            }

            if (!itemSelected.ReservationFromReservation.CheckedIn)
            {
                tlCustomerCheckedIn.Title = "Press for\nCheck In";
                icCheckedIn.Visibility = Visibility.Hidden;
            }
            else
            {
                tlCustomerCheckedIn.Title = "Checked In";
                icCheckedIn.Visibility = Visibility.Visible;
            }

            if (!itemSelected.ReservationFromReservation.CheckedOut)
            {
                tlCustomerCheckedOut.Title = "Press for\nCheck Out";
                icCheckedOut.Visibility = Visibility.Hidden;
            }
            else
            {
                tlCustomerCheckedOut.Title = "Checked Out";
                icCheckedOut.Visibility = Visibility.Visible;
            }
        }

        private void tbCustomerSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                listViewForReservations = new ObservableCollection<ListViewForReservations>();
                lvReservations.ItemsSource = listViewForReservations;
                if (tbCustomerSearch.Text.Count() > 0)
                {
                    var customersWithName = Globals.ctx.Customers.Where(c => c.Name.Contains(tbCustomerSearch.Text)).ToList();
                    //tbReservationSearch.Text = tbCustomerSearch.Text;
                    //lvReservations.ItemsSource = customersWithName;
                    var customersIdWithName = (from c in customersWithName select c.Id).ToList();
                    var reservationsOfCustomersIdWithName = Globals.ctx.Reservations.Where(r => customersIdWithName.Contains(r.CustomerId)).ToList();
                    if (reservationsOfCustomersIdWithName.Count <= 0)
                    {
                        lvReservations.ItemsSource = null;
                        return;
                    }
                    foreach (Reservation reservation in reservationsOfCustomersIdWithName)
                    {
                        Customer customerFromReservation = Globals.ctx.Customers.FirstOrDefault(y => y.Id == reservation.CustomerId);
                        Room roomFromReservation = Globals.ctx.Rooms.FirstOrDefault(y => y.Id == reservation.RoomId);
                        RoomGroup roomGroupFromReservation = Globals.ctx.RoomGroups.FirstOrDefault(y => y.Id == reservation.RoomGroupId);
                        Employee employeeFromReservation = Globals.ctx.Employees.FirstOrDefault(y => y.Id == reservation.EmployeeId);
                        listViewForReservations.Add(new ListViewForReservations(reservation, customerFromReservation, roomFromReservation,
                                                roomGroupFromReservation, employeeFromReservation));
                    }
                    lvReservations.ItemsSource = listViewForReservations;
                }
                if (tbReservationSearch.Text.Count() > 0)
                {
                    List<Reservation> reservationsWithID = new List<Reservation>();
                    if (tbCustomerSearch.Text.Count() == 0)
                    {
                        reservationsWithID = Globals.ctx.Reservations.Where(r => (r.Id + "").Contains(tbReservationSearch.Text)).ToList();
                    }
                    else
                    {
                        reservationsWithID = listViewForReservations.Where(lvr => (lvr.Id + "").Contains(tbReservationSearch.Text)).Select(r => r.ReservationFromReservation).ToList();
                    }
                    if (reservationsWithID.Count <= 0)
                    {
                        lvReservations.ItemsSource = null;
                        return;
                    }
                    listViewForReservations = new ObservableCollection<ListViewForReservations>();
                    lvReservations.ItemsSource = listViewForReservations;
                    foreach (Reservation reservation in reservationsWithID)
                    {
                        Customer customerFromReservation = Globals.ctx.Customers.FirstOrDefault(y => y.Id == reservation.CustomerId);
                        Room roomFromReservation = Globals.ctx.Rooms.FirstOrDefault(y => y.Id == reservation.RoomId);
                        RoomGroup roomGroupFromReservation = Globals.ctx.RoomGroups.FirstOrDefault(y => y.Id == reservation.RoomGroupId);
                        Employee employeeFromReservation = Globals.ctx.Employees.FirstOrDefault(y => y.Id == reservation.EmployeeId);
                        listViewForReservations.Add(new ListViewForReservations(reservation, customerFromReservation, roomFromReservation,
                                                roomGroupFromReservation, employeeFromReservation));
                    }
                    lvReservations.ItemsSource = listViewForReservations;
                }
                if (!(dtpkCheckInSearch.SelectedDate is null))
                {
                    List<Reservation> reservationsWithCheckIn = new List<Reservation>();
                    if ((tbCustomerSearch.Text.Count() == 0) && (tbReservationSearch.Text.Count() == 0))
                    {
                        reservationsWithCheckIn = Globals.ctx.Reservations.Where(r => (r.CheckIn == dtpkCheckInSearch.SelectedDate)).ToList();
                    }
                    else
                    {
                        reservationsWithCheckIn = listViewForReservations.Where(lvr => (lvr.CheckIn == dtpkCheckInSearch.SelectedDate)).Select(r => r.ReservationFromReservation).ToList();
                    }
                    if (reservationsWithCheckIn.Count <= 0)
                    {
                        lvReservations.ItemsSource = null;
                        return;
                    }
                    listViewForReservations = new ObservableCollection<ListViewForReservations>();
                    lvReservations.ItemsSource = listViewForReservations;
                    foreach (Reservation reservation in reservationsWithCheckIn)
                    {
                        Customer customerFromReservation = Globals.ctx.Customers.FirstOrDefault(y => y.Id == reservation.CustomerId);
                        Room roomFromReservation = Globals.ctx.Rooms.FirstOrDefault(y => y.Id == reservation.RoomId);
                        RoomGroup roomGroupFromReservation = Globals.ctx.RoomGroups.FirstOrDefault(y => y.Id == reservation.RoomGroupId);
                        Employee employeeFromReservation = Globals.ctx.Employees.FirstOrDefault(y => y.Id == reservation.EmployeeId);
                        listViewForReservations.Add(new ListViewForReservations(reservation, customerFromReservation, roomFromReservation,
                                                roomGroupFromReservation, employeeFromReservation));
                    }
                    lvReservations.ItemsSource = listViewForReservations;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void tbReservationSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            tbCustomerSearch_TextChanged(sender, e);
        }

        private void dtpkCheckInSearch_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            tbCustomerSearch_TextChanged(sender, null);
        }

        private void tlCustomerPayment_Click(object sender, RoutedEventArgs e)
        {
            if (lvReservations.SelectedItem is null)
            {
                return;
            }
            try
            {
                ListViewForReservations itemSelected = lvReservations.SelectedItem as ListViewForReservations;
                if (!itemSelected.ReservationFromReservation.Paid)
                {
                    tlCustomerPayment.Title = "Paid";
                    icPayment.Visibility = Visibility.Visible;
                    Globals.ctx.Reservations.FirstOrDefault(x => x.Id == itemSelected.Id).Paid = true;
                    Globals.ctx.SaveChanges();
                }
                lvReservations_SelectionChanged(sender, null);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void tlCustomerCancelled_Click(object sender, RoutedEventArgs e)
        {
            if (lvReservations.SelectedItem is null)
            {
                return;
            }
            try
            {
                ListViewForReservations itemSelected = lvReservations.SelectedItem as ListViewForReservations;
                if (!itemSelected.ReservationFromReservation.Cancelled)
                {
                    var confirmation = MessageBox.Show(this, "Are you sure? The Room will be lost.", "Cancel reservation", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
                    if (confirmation == MessageBoxResult.OK)
                    {
                        tlCustomerCancelled.Title = "Canceled";
                        icCancelled.Visibility = Visibility.Visible;
                        Globals.ctx.Reservations.FirstOrDefault(x => x.Id == itemSelected.Id).Cancelled = true;
                        Globals.ctx.SaveChanges();
                    }
                }
                lvReservations_SelectionChanged(sender, null);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void tlCustomerCheckedIn_Click(object sender, RoutedEventArgs e)
        {
            if (lvReservations.SelectedItem is null)
            {
                return;
            }
            try
            {
                ListViewForReservations itemSelected = lvReservations.SelectedItem as ListViewForReservations;
                if (!itemSelected.ReservationFromReservation.CheckedIn)
                {
                    tlCustomerCheckedIn.Title = "Checked In";
                    icCheckedIn.Visibility = Visibility.Visible;
                    Globals.ctx.Reservations.FirstOrDefault(x => x.Id == itemSelected.Id).CheckedIn = true;
                    Globals.ctx.SaveChanges();
                }
                lvReservations_SelectionChanged(sender, null);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void tlCustomerCheckedOut_Click(object sender, RoutedEventArgs e)
        {
            if (lvReservations.SelectedItem is null)
            {
                return;
            }
            try
            {
                ListViewForReservations itemSelected = lvReservations.SelectedItem as ListViewForReservations;
                if (!itemSelected.ReservationFromReservation.CheckedOut)
                {
                    tlCustomerCheckedOut.Title = "Checked Out";
                    icCheckedOut.Visibility = Visibility.Visible;
                    Globals.ctx.Reservations.FirstOrDefault(x => x.Id == itemSelected.Id).CheckedOut = true;
                    Globals.ctx.SaveChanges();
                }
                lvReservations_SelectionChanged(sender, null);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }
    }

    public class ListViewForReservations
    {
        public ListViewForReservations(Reservation reservation, Customer customerFromReservation, Room roomFromReservation, 
                                        RoomGroup roomGroupFromReservation, Employee employeeFromReservation)
        {
            Id = reservation.Id;
            ReservationFromReservation = reservation;
            CustomerFromReservation = customerFromReservation;
            RoomFromReservation = roomFromReservation;
            RoomGroupFromReservation = roomGroupFromReservation;
            EmployeeFromReservation = employeeFromReservation;
            Price = reservation.Price;
            CheckIn = reservation.CheckIn;
            CheckOut = reservation.CheckOut;
            Paid = reservation.Paid;
            Cancelled = reservation.Cancelled;
        }

        public int Id { get; set; }

        public Reservation ReservationFromReservation { get; set; }

        public Customer CustomerFromReservation { get; set; }

        public Room RoomFromReservation { get; set; }

        public RoomGroup RoomGroupFromReservation { get; set; }

        public Employee EmployeeFromReservation { get; set; }

        public decimal Price { get; set; }

        public DateTime CheckIn { get; set; }

        public DateTime CheckOut { get; set; }

        public bool Paid { get; set; }

        public bool Cancelled { get; set; }
    }
}

﻿#pragma checksum "..\..\CheckInCheckOutPayment.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "F50E2F2D5E90BB2C27B1F533C3E11B42E98FC0CE"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using HotelManagementSystem;
using MahApps.Metro;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.IconPacks;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace HotelManagementSystem {
    
    
    /// <summary>
    /// CheckInCheckOutPayment
    /// </summary>
    public partial class CheckInCheckOutPayment : MahApps.Metro.Controls.MetroWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbCustomerSearch;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbReservationSearch;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dtpkCheckInSearch;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvReservations;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbCustomerName;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbCustomerRoom;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbCustomerTotalPrice;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile tlCustomerCancelled;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.IconPacks.PackIconMaterial icCancelled;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile tlCustomerPayment;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.IconPacks.PackIconMaterial icPayment;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile tlCustomerCheckedIn;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.IconPacks.PackIconMaterial icCheckedIn;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.Tile tlCustomerCheckedOut;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\CheckInCheckOutPayment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.IconPacks.PackIconMaterial icCheckedOut;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/HotelManagementSystem;component/checkincheckoutpayment.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\CheckInCheckOutPayment.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.tbCustomerSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 18 "..\..\CheckInCheckOutPayment.xaml"
            this.tbCustomerSearch.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.tbCustomerSearch_TextChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tbReservationSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 19 "..\..\CheckInCheckOutPayment.xaml"
            this.tbReservationSearch.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.tbReservationSearch_TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.dtpkCheckInSearch = ((System.Windows.Controls.DatePicker)(target));
            
            #line 20 "..\..\CheckInCheckOutPayment.xaml"
            this.dtpkCheckInSearch.SelectedDateChanged += new System.EventHandler<System.Windows.Controls.SelectionChangedEventArgs>(this.dtpkCheckInSearch_SelectedDateChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.lvReservations = ((System.Windows.Controls.ListView)(target));
            
            #line 25 "..\..\CheckInCheckOutPayment.xaml"
            this.lvReservations.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.lvReservations_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.lbCustomerName = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lbCustomerRoom = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.lbCustomerTotalPrice = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.tlCustomerCancelled = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 40 "..\..\CheckInCheckOutPayment.xaml"
            this.tlCustomerCancelled.Click += new System.Windows.RoutedEventHandler(this.tlCustomerCancelled_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.icCancelled = ((MahApps.Metro.IconPacks.PackIconMaterial)(target));
            return;
            case 10:
            this.tlCustomerPayment = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 46 "..\..\CheckInCheckOutPayment.xaml"
            this.tlCustomerPayment.Click += new System.Windows.RoutedEventHandler(this.tlCustomerPayment_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.icPayment = ((MahApps.Metro.IconPacks.PackIconMaterial)(target));
            return;
            case 12:
            this.tlCustomerCheckedIn = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 52 "..\..\CheckInCheckOutPayment.xaml"
            this.tlCustomerCheckedIn.Click += new System.Windows.RoutedEventHandler(this.tlCustomerCheckedIn_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.icCheckedIn = ((MahApps.Metro.IconPacks.PackIconMaterial)(target));
            return;
            case 14:
            this.tlCustomerCheckedOut = ((MahApps.Metro.Controls.Tile)(target));
            
            #line 58 "..\..\CheckInCheckOutPayment.xaml"
            this.tlCustomerCheckedOut.Click += new System.Windows.RoutedEventHandler(this.tlCustomerCheckedOut_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.icCheckedOut = ((MahApps.Metro.IconPacks.PackIconMaterial)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}


﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data.SqlClient;
using System.Security;
using System.IO;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for SelectImagesForRoomGroup.xaml
    /// </summary>
    public partial class SelectImagesForRoomGroup : MetroWindow
    {
        RoomGroup currRoomGroup;

        public SelectImagesForRoomGroup(Window parent, RoomGroup __currRoomGroup = null)
        {
            InitializeComponent();
            currRoomGroup = __currRoomGroup;
            RefreshList();
        }

        void RefreshList()
        {
            try
            {
                if (currRoomGroup is null)
                {
                    return;
                }
                List<Picture> currentPicturesInDB = (from p in Globals.ctx.Pictures select p).ToList();
                List<Picture> currentPicturesInRoomGroup = Globals.ctx.RoomGroups.FirstOrDefault(rg => rg.Id == currRoomGroup.Id).Picturecollection.ToList();
                List<Picture> picturesOutsideRoomGroup = new List<Picture>(currentPicturesInDB);
                foreach(Picture p in currentPicturesInRoomGroup)
                {
                    picturesOutsideRoomGroup.Remove(p);
                }
                //List<Picture> picturesOutsideRoomGroup = ((from p in Globals.ctx.Pictures select p).Except(Globals.ctx.RoomGroups.FirstOrDefault(rg => rg.Id == currRoomGroup.Id).Picturecollection)).ToList();
                lvImageBrowserRoomGroup.ItemsSource = currentPicturesInRoomGroup;
                lvImageBrowserOutsideRoomGroup.ItemsSource = picturesOutsideRoomGroup;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void btLoadImages_Click(object sender, RoutedEventArgs e)
        {
                Picture currPicture = lvImageBrowserOutsideRoomGroup.SelectedItem as Picture;
                if (currPicture == null) { return; }
                try
                {
                    Globals.ctx.RoomGroups.FirstOrDefault(rg => rg.Id == currRoomGroup.Id).Picturecollection.Add(Globals.ctx.Pictures.FirstOrDefault(p => p.Id == currPicture.Id));
                    Globals.ctx.SaveChanges();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(this, ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                RefreshList();
        }

        private void btDeleteImages_Click(object sender, RoutedEventArgs e)
        {
            Picture currPicture = lvImageBrowserRoomGroup.SelectedItem as Picture;
            if (currPicture == null) { return; }
            try
            {
                Globals.ctx.RoomGroups.FirstOrDefault(rg => rg.Id == currRoomGroup.Id).Picturecollection.Remove(Globals.ctx.Pictures.FirstOrDefault(p => p.Id == currPicture.Id));
                Globals.ctx.SaveChanges();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            RefreshList();
        }

    }
}

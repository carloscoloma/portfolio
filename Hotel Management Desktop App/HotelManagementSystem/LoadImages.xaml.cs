﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using System.Data.SqlClient;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for LoadImages.xaml
    /// </summary>
    public partial class LoadImages : MetroWindow
    {
        List<Picture> currentPicturesInDB; 

        public LoadImages()
        {
            InitializeComponent();
            currentPicturesInDB = new List<Picture>();
            RefreshList();
        }

        void RefreshList()
        {
            try
            {
                currentPicturesInDB = (from p in Globals.ctx.Pictures select p).ToList();
                lvImageBrowser.ItemsSource = currentPicturesInDB;
                //no need to do Refresh if we assign to ItemsSource
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void btLoadImages_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Title = "Select the pictures";
                openFileDialog.Filter = "Images (*.JPG,*.GIF,*.PNG)|*.JPG;*.GIF;*.PNG";
                openFileDialog.Multiselect = true;
                if (openFileDialog.ShowDialog() == true)
                {
                    //List<Picture> pictureList = new List<Picture>();
                    foreach (string file in openFileDialog.FileNames)
                    {
                        try
                        {
                            Picture p = new Picture(file);
                            Globals.ctx.Pictures.Add(p); // persist - schedule Picture to be inserted into database table
                            Globals.ctx.SaveChanges();
                            //pictureList.Add(new Picture(file));
                        }
                        catch (SecurityException ex)
                        {
                            // The user lacks appropriate permissions to read files, discover paths, etc.
                            MessageBox.Show("Security error. Please contact your administrator for details.\n\n" +
                                "Error message: " + ex.Message + "\n\n" +
                                "Details (send to Support):\n\n" + ex.StackTrace
                            );
                        }
                        catch (Exception ex)
                        {
                            // Could not load the image - probably related to Windows file system permissions.
                            MessageBox.Show("Cannot display the image: " + file.Substring(file.LastIndexOf('\\'))
                                + ". You may not have permission to read the file, or " +
                                "it may be corrupt.\n\nReported error: " + ex.Message);
                        }
                    }
                    RefreshList();
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show("Could not open the file: " + ex.Message, "Error Opoening file", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btDeleteImages_Click(object sender, RoutedEventArgs e)
        {
            Picture currPicture = lvImageBrowser.SelectedItem as Picture;
            if (currPicture == null) { return; }
            MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete the selected pictures?", "Confirm Delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                foreach (Picture p in lvImageBrowser.SelectedItems)
                {
                    try
                    {
                        Globals.ctx.Pictures.Remove(p);
                        Globals.ctx.SaveChanges();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show(this, ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                RefreshList();
            }

            
        }
    }
}

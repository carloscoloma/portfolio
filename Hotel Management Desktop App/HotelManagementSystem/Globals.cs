﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Globals
    {
        public static HotelContext ctx;
        public static Room currRoom;
        public static RoomGroup currRoomGroup;
        public static Picture currPicture;
        public static Reservation currReservation;
        public static Employee currEmployee;
    }
}

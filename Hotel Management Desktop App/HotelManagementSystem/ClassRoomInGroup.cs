﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class RoomInGroup
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("RoomSelected"), Required] //
        public int RoomId { get; set; }
        public Room RoomSelected { get; set; }

        [ForeignKey("RoomGroupSelected"), Required] //
        public int RoomGroupId { get; set; }
        public RoomGroup RoomGroupSelected { get; set; }

        public RoomInGroup( int roomId, int roomGroupId)
        {
            RoomId = roomId;
            RoomGroupId = roomGroupId;
        }

        [Obsolete("Only needed for serialization and materialization", true)]
        public RoomInGroup() { }

        public RoomInGroup(RoomInGroup roomInGroup)
        {
        }

    }
}

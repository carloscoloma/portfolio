﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for SignIn.xaml
    /// </summary>
    public partial class SignIn : MetroWindow
    {
        public SignIn()
        {
            InitializeComponent();
            Globals.ctx = new HotelContext();
            //tbTest.SetValue(MahApps.Metro.Controls.TextBoxHelper.ClearTextButtonProperty, true);
        }

        private void Button_LoginClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(200), Required]
        public string Name { get; set; }

        [MaxLength(20)]
        public string CreditCard { get; set; }

        [MaxLength(254)]
        public string Email { get; set; }

        [MaxLength(30)]
        public string Telephone { get; set; }

        [MaxLength(100), Required]
        public string Address { get; set; }

        public virtual ICollection<Reservation> ReservationCollection { get; set; } //

        public Customer(string name, string creditCard, string email, string telephone, string address)
        {
            Name = name;
            CreditCard = creditCard;
            Email = email;
            Telephone = telephone;
            Address = address;
        }

        [Obsolete("Only needed for serialization and materialization", true)]
        public Customer() { }

        public Customer(Customer customer)
        {
        }

    }
}

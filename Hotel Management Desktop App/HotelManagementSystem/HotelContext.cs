﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class HotelContext : DbContext
    {
        public HotelContext() : base("name=DbConnectionHotel")
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        //public DbSet<PictureInGroup> PictureInGroups { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomGroup> RoomGroups { get; set; }
        //public DbSet<RoomInGroup> RoomInGroups { get; set; }
        public DbSet<Picture> Pictures { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // configures one-to-many relationship

            modelBuilder.Entity<Reservation>()
            .HasRequired<Employee>(r => r.EmployeeInCharge)
            .WithMany(e => e.ReservationCollection)
            .HasForeignKey<int>(r => r.EmployeeId);

            modelBuilder.Entity<Reservation>()
            .HasRequired<Customer>(r => r.CustomerGuest)
            .WithMany(c => c.ReservationCollection)
            .HasForeignKey<int>(r => r.CustomerId);

            modelBuilder.Entity<Reservation>()
            .HasRequired<Room>(r => r.RoomSelected)
            .WithMany(r => r.ReservationCollection)
            .HasForeignKey<int>(r => r.RoomId);

            modelBuilder.Entity<Reservation>()
            .HasRequired<RoomGroup>(r => r.RoomGroupSelected)
            .WithMany(r => r.ReservationCollection)
            .HasForeignKey<int>(r => r.RoomGroupId);

            //modelBuilder.Entity<RoomInGroup>()
            //.HasRequired<Room>(r => r.RoomSelected)
            //.WithMany(r => r.RoomInGroupCollection)
            //.HasForeignKey<int>(r => r.RoomId)
            //.WillCascadeOnDelete();

            //modelBuilder.Entity<RoomInGroup>()
            //.HasRequired<RoomGroup>(r => r.RoomGroupSelected)
            //.WithMany(r => r.RoomInGroupCollection)
            //.HasForeignKey<int>(r => r.RoomGroupId)
            //.WillCascadeOnDelete();

            //modelBuilder.Entity<PictureInGroup>()
            //.HasRequired<Picture>(p => p.PictureSelected)
            //.WithMany(p => p.PictureInGroupCollection)
            //.HasForeignKey<int>(p => p.PictureId)
            //.WillCascadeOnDelete();

            //modelBuilder.Entity<PictureInGroup>()
            //.HasRequired<RoomGroup>(p => p.RoomGroupSelected)
            //.WithMany(r => r.PictureInGroupCollection)
            //.HasForeignKey<int>(p => p.RoomGroupId)
            //.WillCascadeOnDelete();


            modelBuilder.Entity<Room>()
                           .HasMany<RoomGroup>(r => r.RoomGroups)
                           .WithMany(g => g.Rooms)
                           .Map(rg =>
                           {
                               rg.MapLeftKey("RoomId");
                               rg.MapRightKey("GroupId");
                               rg.ToTable("GroupRoom");
                           });

            modelBuilder.Entity<Picture>()
                           .HasMany<RoomGroup>(p => p.RoomGroups)
                           .WithMany(g => g.Picturecollection)
                           .Map(rg =>
                           {
                               rg.MapLeftKey("PictureId");
                               rg.MapRightKey("GroupId");
                               rg.ToTable("GroupPicture");
                           });

        }
    }
}

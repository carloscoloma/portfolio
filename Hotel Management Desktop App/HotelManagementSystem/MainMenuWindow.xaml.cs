﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro;
using MahApps.Metro.Controls;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for MainMenuWindow.xaml
    /// </summary>
    public partial class MainMenuWindow : MetroWindow
    {
        public MainMenuWindow()
        {
            InitializeComponent();
            this.Hide();
            Globals.ctx = new HotelContext();
            SignIn mainWindow = new SignIn();
            mainWindow.ShowDialog();
            this.Show();
        }

        private void tlAddReservation_Click(object sender, RoutedEventArgs e)
        {
            SearchRoom searchRoom = new SearchRoom();
            searchRoom.ShowDialog();
        }

        private void tlCheckInCheckOutPayment_Click(object sender, RoutedEventArgs e)
        {
            CheckInCheckOutPayment checkInCheckOutPayment = new CheckInCheckOutPayment();
            checkInCheckOutPayment.ShowDialog();
        }

        private void tlCustomerInfo_Click(object sender, RoutedEventArgs e)
        {
            CustomerInfo customerInfo = new CustomerInfo();
            customerInfo.ShowDialog();
        }

        private void tlReportsCheckInAndCheckOut_Click(object sender, RoutedEventArgs e)
        {
            ReportsCheckInAndCheckOut reportsCheckInAndCheckOut = new ReportsCheckInAndCheckOut();
            reportsCheckInAndCheckOut.ShowDialog();
        }

        private void tlManageEmployees_Click(object sender, RoutedEventArgs e)
        {
            BackOfficeManagement backOfficeManagementManageEmployees = new BackOfficeManagement();
            backOfficeManagementManageEmployees.MyTabControlBackOffice.SelectedItem = backOfficeManagementManageEmployees.TabItemManageEmployees;
            backOfficeManagementManageEmployees.ShowDialog();
        }

        private void tlManageHotel_Click(object sender, RoutedEventArgs e)
        {
            BackOfficeManagement backOfficeManagementManageHotel = new BackOfficeManagement();
            backOfficeManagementManageHotel.MyTabControlBackOffice.SelectedItem = backOfficeManagementManageHotel.TabItemManageHotel;
            backOfficeManagementManageHotel.ShowDialog();
        }

        private void tlReports_Click(object sender, RoutedEventArgs e)
        {
            BackOfficeManagement backOfficeManagementReports = new BackOfficeManagement();
            backOfficeManagementReports.MyTabControlBackOffice.SelectedItem = backOfficeManagementReports.TabItemReport;
            backOfficeManagementReports.ShowDialog();

        }

        private void btAbout_Click(object sender, RoutedEventArgs e)
        {
            ThemeManager.ChangeAppStyle(Application.Current,
                                    ThemeManager.GetAccent("Red"),
                                    ThemeManager.GetAppTheme("BaseDark"));
        }


    }
}

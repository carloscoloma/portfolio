﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for ReportsCheckInAndCheckOut.xaml
    /// </summary>
    public partial class ReportsCheckInAndCheckOut : MetroWindow
    {
        ObservableCollection<ListViewForReservations> listViewForReservations = new ObservableCollection<ListViewForReservations>();
        public ReportsCheckInAndCheckOut()
        {
            try
            {
                InitializeComponent();
                var today = DateTime.Today;
                lbTodayDay.Content = ("Daily Check In/Out for Today. DATE: " + today.ToLongDateString());
                List<Reservation> todayReservationsWithCheckIn = new List<Reservation>();
                todayReservationsWithCheckIn = Globals.ctx.Reservations.Where(r => (DbFunctions.TruncateTime(r.CheckIn) == today)).ToList();
                List<Reservation> todayReservationsWithCheckOut = new List<Reservation>();
                todayReservationsWithCheckOut = Globals.ctx.Reservations.Where(r => (DbFunctions.TruncateTime(r.CheckOut) == today)).ToList();
                List<Reservation> todayReservationsWithCheckInAndCheckOut = new List<Reservation>();
                todayReservationsWithCheckInAndCheckOut.AddRange(todayReservationsWithCheckIn);
                todayReservationsWithCheckInAndCheckOut.AddRange(todayReservationsWithCheckOut);

                foreach (Reservation reservation in todayReservationsWithCheckInAndCheckOut)
                {
                    Customer customerFromReservation = Globals.ctx.Customers.FirstOrDefault(y => y.Id == reservation.CustomerId);
                    Room roomFromReservation = Globals.ctx.Rooms.FirstOrDefault(y => y.Id == reservation.RoomId);
                    RoomGroup roomGroupFromReservation = Globals.ctx.RoomGroups.FirstOrDefault(y => y.Id == reservation.RoomGroupId);
                    Employee employeeFromReservation = Globals.ctx.Employees.FirstOrDefault(y => y.Id == reservation.EmployeeId);
                    listViewForReservations.Add(new ListViewForReservations(reservation, customerFromReservation, roomFromReservation,
                                            roomGroupFromReservation, employeeFromReservation));
                }
                lvReservations.ItemsSource = listViewForReservations;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }
    }
}

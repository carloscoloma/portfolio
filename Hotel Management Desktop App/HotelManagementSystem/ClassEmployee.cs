﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(200), Required]
        public string Name { get; set; }

        [MaxLength(20), Required]
        public string Password { get; set; }

        [MaxLength(100), Required]
        public string Position { get; set; }

        public ICollection<Reservation> ReservationCollection { get; set; }//

        public Employee(string name, string password, string position)
        {
            Name = name;
            Password = password;
            Position = position;
        }

        [Obsolete("Only needed for serialization and materialization", true)]
        public Employee() { }

        public Employee(Employee employee)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Room
    {
        public Room()
        {
            this.RoomGroups = new HashSet<RoomGroup>();
        }
        [Key]
        public int Id { get; set; }

        [Required]
        public int Number { get; set; }

        [Required]
        public int BuildingNumber { get; set; }

        [Required]
        public int FloorNumber { get; set; }

        public ICollection<Reservation> ReservationCollection { get; set; } //

        //public ICollection<RoomInGroup> RoomInGroupCollection { get; set; } //

        public virtual ICollection<RoomGroup> RoomGroups { get; set; }


        public Room(int number, int buildingNumber, int floorNumber)
        {
            Number = number;
            BuildingNumber = buildingNumber;
            FloorNumber = floorNumber;
        }

      /*  [Obsolete("Only needed for serialization and materialization", true)]
        public Room() { }*/

        public Room(Room room)
        {
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for CustomerInfo.xaml
    /// </summary>
    public partial class CustomerInfo : MetroWindow
    {
        ObservableCollection<Customer> customerCollection = new ObservableCollection<Customer>();

        public CustomerInfo()
        {
            InitializeComponent();
        }

        private void lvReservations_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvCustomers.SelectedItem is null)
            {
                tbCustomerEmail.Text = " ";
                tbCustomerCreditCard.Text = " ";
                tbCustomerPhone.Text = " ";
                tbCustomerAddress.Text = " ";
                return;
            }
            try
            {
                Customer selectedCustomer = lvCustomers.SelectedItem as Customer;
                Customer customer = Globals.ctx.Customers.FirstOrDefault(y => y.Id == selectedCustomer.Id);
                tbCustomerEmail.Text = customer.Email;
                tbCustomerCreditCard.Text = customer.CreditCard;
                tbCustomerPhone.Text = customer.Telephone;
                tbCustomerAddress.Text = customer.Address;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void tbCustomerSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                customerCollection = new ObservableCollection<Customer>();
                lvCustomers.ItemsSource = customerCollection;
                if (tbCustomerSearch.Text.Count() > 0)
                {
                    var customersWithName = Globals.ctx.Customers.Where(c => c.Name.Contains(tbCustomerSearch.Text)).ToList();
                    if (customersWithName.Count <= 0)
                    {
                        lvCustomers.ItemsSource = null;
                        return;
                    }
                    lvCustomers.ItemsSource = customersWithName;
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvCustomers.SelectedItem is null)
                {
                    return;
                }
                Customer selectedCustomer = lvCustomers.SelectedItem as Customer;
                Globals.ctx.Customers.FirstOrDefault(x => x.Id == selectedCustomer.Id).CreditCard = tbCustomerCreditCard.Text;
                Globals.ctx.Customers.FirstOrDefault(x => x.Id == selectedCustomer.Id).Email = tbCustomerEmail.Text;
                Globals.ctx.Customers.FirstOrDefault(x => x.Id == selectedCustomer.Id).Telephone = tbCustomerPhone.Text;
                Globals.ctx.Customers.FirstOrDefault(x => x.Id == selectedCustomer.Id).Address = tbCustomerAddress.Text;
                Globals.ctx.SaveChanges();
                tbCustomerSearch_TextChanged(sender, null);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

    }


}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Reservation
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("CustomerGuest"), Required] //
        public int CustomerId { get; set; }
        public Customer CustomerGuest { get; set; }

        [ForeignKey("EmployeeInCharge")] //
        public int EmployeeId { get; set; }
        public Employee EmployeeInCharge { get; set; }

        [Required]
        public DateTime CheckIn { get; set; }

        [Required]
        public DateTime CheckOut { get; set; }

        [Required]
        public bool CheckedIn { get; set; }

        [Required]
        public bool CheckedOut { get; set; }

        [ForeignKey("RoomSelected"), Required] //
        public int RoomId { get; set; }
        public Room RoomSelected { get; set; }

        [ForeignKey("RoomGroupSelected"), Required] //
        public int RoomGroupId { get; set; }
        public RoomGroup RoomGroupSelected { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public bool Paid { get; set; }

        [Required]
        public bool Cancelled { get; set; }

        public Reservation(int customerId, int employeeId, DateTime checkIn, DateTime checkOut, bool checkedIn, bool checkedOut, int roomId, int roomGroupId, bool paid, decimal price, bool cancelled)
        {
            CustomerId = customerId;
            EmployeeId = employeeId;
            CheckIn = checkIn;
            CheckOut = checkOut;
            CheckedIn = checkedIn;
            CheckedOut = checkedOut;
            RoomId = roomId;
            RoomGroupId = roomGroupId;
            Paid = paid;
            Price = price;
            Cancelled = cancelled;
        }

        [Obsolete("Only needed for serialization and materialization", true)]
        public Reservation() { }

        public Reservation(Reservation reservation)
        {
        }

    }
}

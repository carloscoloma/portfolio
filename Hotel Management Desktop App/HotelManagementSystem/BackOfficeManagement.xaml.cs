﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MahApps.Metro.Controls;
using SciChart.Charting.Model.DataSeries;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for BackOfficeManagement.xaml
    /// </summary>
    public partial class BackOfficeManagement : MetroWindow
    {
        Employee currEmployee;
        List<Employee> emList;
        List<Room> roomManagementList;
        List<RoomGroup> groupList;

        public BackOfficeManagement()
        {
            try
            {
                InitializeComponent();
                Globals.ctx = new HotelContext();
                RefreshRoomList();
                RefreshEmList();
                RefreshGroupList();
                RefreshReservationList();

                //Display total reservation when page loaded
                int totalReservation = (from Reservation in Globals.ctx.Reservations where (Reservation.Cancelled == false) select Globals.ctx.Reservations.Count()).ToList().Count();

                or_lblTotal.Content = totalReservation;

                //chart
                var dataSeries = new XyDataSeries<int, double>();

                for (int i = 1; i < 12; i++)
                {
                    string priceStr = Globals.ctx.RoomGroups.FirstOrDefault(rg => rg.Id == i).Price.ToString();
                    double priceDouble;
                    if (!double.TryParse(priceStr, out priceDouble))
                    {
                        MessageBox.Show("Can not convert decimal to double.");
                        return;
                    }
                    dataSeries.Append(i, priceDouble);
                }

                columnSeries.DataSeries = dataSeries;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void Button_LoadPicturesClick(object sender, RoutedEventArgs e)
        {
            LoadImages loadImages = new LoadImages();
            loadImages.ShowDialog();
        }


        private void RefreshReservationList()
        {
            try
            {

                var listCombination = (from Reservation in Globals.ctx.Reservations


                                       select new
                                       {
                                           Id = Reservation.Id,
                                           CustomerName = Reservation.CustomerGuest.Name,
                                           CheckInDate = Reservation.CheckIn,
                                           CheckOutDate = Reservation.CheckIn,
                                           GroupName = Reservation.RoomGroupSelected.Name,
                                           BedDescription = Reservation.RoomGroupSelected.BedDescription,
                                           Price = Reservation.Price,
                                           Paid = Reservation.Paid,
                                           Cancelled = Reservation.Cancelled,


                                       }).ToList();
                lvCombination.ItemsSource = listCombination;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void RefreshGroupList()
        {
            try
            {
                groupList = (from g in Globals.ctx.RoomGroups select g).ToList();
            lvRoomGroup.ItemsSource = groupList;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void RefreshEmList()
        {
            try
            {
                emList = (from e in Globals.ctx.Employees select e).ToList();

            lvEmployee.ItemsSource = emList;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void Button_SaveGroupClick(object sender, RoutedEventArgs e)
        {
            try
            {
                string groupName = rm_tbGroupName.Text;
                string groupDescription = rm_tbDescription.Text;
                string pricrStr = rm_tbPrice.Text;
                string bedDescription = rm_cbxBedDescription.Text;
                decimal price;
                if (!decimal.TryParse(pricrStr, out price))
                {
                    MessageBox.Show("Price must be an decimal value");

                    return;
                }
                bool breakfast;
                if (rm_chBreakfast.IsChecked == true)
                {
                    breakfast = true;
                }
                else
                {
                    breakfast = false;
                }

                bool crib;
                if (rm_chCrib.IsChecked == true)
                {
                    crib = true;
                }
                else
                {
                    crib = false;
                }

                int adultCapacity;
                if (!int.TryParse(rm_cbxAduiltCapacity.Text.Substring(0, 1), out adultCapacity))
                {
                    MessageBox.Show("adultCapacity must be an integer value");

                    return;
                }

                int childCapacity;
                if (!int.TryParse(rm_cbxChildCapacity.Text.Substring(0, 1), out childCapacity))
                {
                    MessageBox.Show("childCapacity must be an integer value");

                    return;
                }

                RoomGroup g = new RoomGroup(groupName, groupDescription, price, breakfast, adultCapacity, childCapacity, bedDescription, crib);
                Globals.ctx.RoomGroups.Add(g);
                Globals.ctx.SaveChanges();
                RefreshRoomList();

                RoomgroupClear();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void RoomgroupClear()
        {
            rm_tbGroupName.Text = "";
            rm_tbPrice.Text = "";
            rm_tbDescription.Text = "";
            rm_chBreakfast.IsChecked = false;
            rm_chCrib.IsChecked = false;
        }

        private void RefreshRoomList()
        {
            try
            {
                roomManagementList = (from c in Globals.ctx.Rooms select c).ToList();
                lvRoom.ItemsSource = roomManagementList;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void Button_DeleteGroupClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Globals.currRoomGroup == null) return;

                MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete:\n" +
                    Globals.currRoomGroup.Id, "Confirm delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

                if (result == MessageBoxResult.OK)
                {
                    Globals.ctx.RoomGroups.Remove(Globals.currRoomGroup);
                    Globals.ctx.SaveChanges();


                }

                //clear

                rm_tbGRoupId.Text = "";
                rm_tbGroupName.Text = "";
                rm_tbPrice.Text = "";
                rm_chBreakfast.IsChecked = false;
                rm_chCrib.IsChecked = false;

            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }

        }

        private void Button_AddEmployeeClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((tbEmployeeName.Text.Trim(' ').Length < 1) || (tbEmployeeName.Text.Trim(' ').Length > 100))
                {
                    MessageBox.Show("Name must be between 1 and 100 characteres long");
                    return;
                }
                if ((tbPassword.Text.Trim(' ').Length < 1) || (tbPassword.Text.Trim(' ').Length > 20))
                {
                    MessageBox.Show("Password must be between 1 and 20 characteres long");
                    return;
                }
                if ((tbPosition.Text.Trim(' ').Length < 1) || (tbPosition.Text.Trim(' ').Length > 50))
                {
                    MessageBox.Show("Password must be between 1 and 50 characteres long");
                    return;
                }
                string employeeName = tbEmployeeName.Text;
                string password = tbPassword.Text;
                string position = tbPosition.Text;
                Employee em = new Employee(employeeName, password, position);
                emList.Add(em);
                Globals.ctx.Employees.Add(em);
                Globals.ctx.SaveChanges();
                RefreshEmList();
                EmployeeClear();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void EmployeeClear()
        {
            tbEmployeeName.Text = "";
            tbPassword.Text = "";
            tbPosition.Text = "";
            MessageBox.Show(this, "Employee saved", "Confirmation");
        }

        private void Button_UpdateEmployeeClick(object sender, RoutedEventArgs e)
        {
            try
            {


                currEmployee.Name = tbEmployeeName.Text;
                currEmployee.Password = tbPassword.Text;
                currEmployee.Position = tbPosition.Text;
                Globals.ctx.SaveChanges();
                RefreshEmList();
                EmployeeClear();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void MenuItem_EditClick(object sender, RoutedEventArgs e)
        {
            try
            {
                currEmployee = lvEmployee.SelectedItem as Employee;
                if (currEmployee == null) return;

                tbEmployeeName.Text = currEmployee.Name;
                tbPassword.Text = currEmployee.Password;
                tbPosition.Text = currEmployee.Position;
                Globals.ctx.SaveChanges();
                RefreshEmList();
                EmployeeClear();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void lvEmployee_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currEmployee = lvEmployee.SelectedItem as Employee;
            if (currEmployee == null) return;
            currEmployee = lvEmployee.SelectedItem as Employee;
            if (currEmployee == null) return;

            lbl_EmployeeId.Content = currEmployee.Id;


            tbEmployeeName.Text = currEmployee.Name;
            tbPassword.Text = currEmployee.Password;
            tbPosition.Text = currEmployee.Position;
        }

        private void Button_DeleteEmployeeClick(object sender, RoutedEventArgs e)
        {
            try
            {
                currEmployee = lvEmployee.SelectedItem as Employee;
                if (currEmployee == null) return;
                //
                MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete:\n" +
                    currEmployee.Name, "Confirm delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                //
                if (result == MessageBoxResult.OK)
                {
                    Globals.ctx.Employees.Remove(currEmployee);
                    Globals.ctx.SaveChanges();
                    RefreshEmList();
                    EmployeeClear();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void Button_SaveRoomClick(object sender, RoutedEventArgs e)
        {
            try
            {
                string roomStr = rm_tbRoomNumber.Text;
                int roomNumber;
                if (!int.TryParse(roomStr, out roomNumber))
                {
                    MessageBox.Show("Room Number must be an integer value");

                    return;
                }

                string buildingStr = rm_tbBuilding.Text;
                int buildingId;
                if (!int.TryParse(buildingStr, out buildingId))
                {
                    MessageBox.Show("Building must be an integer value");

                    return;
                }

                string floor = rm_tbFloor.Text;
                int floorId;
                if (!int.TryParse(floor, out floorId))
                {
                    MessageBox.Show("Floor must be an integer value");

                    return;
                }
                if (Globals.currRoom == null)
                {
                    Room r = new Room(roomNumber, buildingId, floorId);
                    Globals.ctx.Rooms.Add(r);
                }
                else
                {
                    string roomNumberStr = rm_tbRoomNumber.Text;
                    int roomNo;
                    if (!int.TryParse(roomNumberStr, out roomNo))
                    {
                        MessageBox.Show("Room number should be an integer value.");
                    }

                    string buildingNoStr = rm_tbBuilding.Text;
                    int buildingNo;
                    if (!int.TryParse(buildingNoStr, out buildingNo))
                    {
                        MessageBox.Show("Bilding number should be an integer value.");
                    }

                    string floorStr = rm_tbFloor.Text;
                    int floorNo;
                    if (!int.TryParse(floorStr, out floorNo))
                    {
                        MessageBox.Show("Floor number should be an integer value.");
                    }
                    Globals.currRoom.Number = roomNo;
                    Globals.currRoom.BuildingNumber = buildingNo;
                    Globals.currRoom.FloorNumber = floorNo;
                }

                Globals.ctx.SaveChanges();
                RefreshRoomList();
                RoomClear();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void RoomClear()
        {
            rm_tbRoomNumber.Text = "";
            rm_tbFloor.Text = "";
            rm_tbBuilding.Text = "";
        }

        private void Button_AddGroupClick(object sender, RoutedEventArgs e)
        {
            try
            {
                /*
                int currRmNo;
                if (!int.TryParse(rm_tbRoomNumber.Text, out currRmNo))
                {
                    MessageBox.Show("Input number should be an integer.");
                    return;
                }*/
                if (Globals.currRoom == null)
                {
                    return;
                }
                int roomGroupInput;
                if (!int.TryParse(rm_tbGRoupId.Text, out roomGroupInput))

                {
                    MessageBox.Show("Input number should be an integer.");
                    return;
                }


                //if (roomGroupInput>.Id)
                RoomGroup rg1 = Globals.ctx.RoomGroups.FirstOrDefault(x => x.Id == roomGroupInput);

                //Globals.ctx.Rooms.FirstOrDefault(x => x.Id == Globals.currRoom.Id).RoomGroups.Add(rg1);

                Globals.ctx.Rooms.FirstOrDefault(x => x.Id == Globals.ctx.Rooms.FirstOrDefault(y => y.Id == Globals.currRoom.Id).Id).RoomGroups.Add(rg1);

                Globals.ctx.SaveChanges();

                MessageBox.Show("Room group added.");

                //Clear
                rm_tbRoomNumber.Text = "";
                rm_tbGRoupId.Text = "";
                rm_tbFloor.Text = "";
                rm_tbBuilding.Text = "";
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }


        private void rm_tbRoomNumber_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string roomNoStr = rm_tbRoomNumber.Text;
            if (roomNoStr != null)
            {

                int roomNo;
                if (!int.TryParse(roomNoStr, out roomNo))
                {
                    MessageBox.Show(this, "invalid room Id data", "Input error");
                    return;
                }

            }
        }



        private void rm_tbGroupId_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                // RoomGroup Globals.currRoomGroup = Globals.ctx.RoomGroups.FirstOrDefault(x => x.Id == int.Parse(rm_tbGRoupId.Text));
                string idStr = rm_tbGRoupId.Text;
                int id;
                if (!int.TryParse(idStr, out id))
                {
                    MessageBox.Show("integer value required.");
                    return;
                }

                var currGroup = (from rg in Globals.ctx.RoomGroups where rg.Id == id select rg).ToList();
                if (currGroup == null)
                {
                    MessageBox.Show("Number doesn't exist.");
                    return;
                }
                else
                {
                    Globals.currRoomGroup = currGroup.First();

                    if (Globals.currRoomGroup == null)
                    {

                        MessageBox.Show("Number doesn't exist.");
                        return;
                    }
                    rm_tbGroupName.Text = Globals.currRoomGroup.Name;
                    rm_tbDescription.Text = Globals.currRoomGroup.Description;
                    rm_cbxBedDescription.Text = currGroup.First().BedDescription;
                    rm_cbxAduiltCapacity.Text = currGroup.First().AdultCapacity + "";
                    rm_cbxChildCapacity.Text = currGroup.First().ChildCapacity + "";
                    rm_tbPrice.Text = currGroup.First().Price + "";
                    rm_chCrib.IsChecked = currGroup.First().Crib;
                    rm_chBreakfast.IsChecked = currGroup.First().Breakfast;
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void Button_EditGroupPicturesClick(object sender, RoutedEventArgs e)
        {
            if (Globals.currRoomGroup is null)
            {
                return;
            }
            SelectImagesForRoomGroup pe = new SelectImagesForRoomGroup(this, Globals.currRoomGroup);
            pe.ShowDialog();
        }

        private void Button_SerchRoomClick(object sender, RoutedEventArgs e)
        {
            try
            {
                string priceSearchStr = or_tbPrice.Text;

                decimal priceSerch;
                if (!decimal.TryParse(priceSearchStr, out priceSerch))
                {
                    MessageBox.Show("Input number is unvalid value.");
                }
                var roomToSerch = (from rg in Globals.ctx.RoomGroups where rg.Price < priceSerch select rg).ToList();

                if (roomToSerch == null)
                {
                    MessageBox.Show("Room not found.");
                    return;
                }

                lvCombination.ItemsSource = roomToSerch;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }

        }

        private void Button_DeleteRoomClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Globals.currRoom == null) return;

                MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to delete:\n" +
                    Globals.currRoom.Id, "Confirm delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);

                if (result == MessageBoxResult.OK)
                {
                    Globals.ctx.Rooms.Remove(Globals.currRoom);
                    Globals.ctx.SaveChanges();
                    RefreshRoomList();
                    RoomClear();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        



        private void lvRoomGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Globals.currRoomGroup = lvRoomGroup.SelectedItem as RoomGroup;

            if (Globals.currRoomGroup == null)
            {

                return;
            }
            else
            {
                rm_lblRoomgroupId.Content = Globals.currRoomGroup.Id;

                rm_tbGroupName.Text = Globals.currRoomGroup.Name;
                rm_tbDescription.Text = Globals.currRoomGroup.Description;
                rm_cbxBedDescription.Text = Globals.currRoomGroup.BedDescription;
                rm_cbxAduiltCapacity.Text = Globals.currRoomGroup.AdultCapacity + "";
                rm_cbxChildCapacity.Text = Globals.currRoomGroup.ChildCapacity + "";
                rm_tbPrice.Text = Globals.currRoomGroup.Price + "";
                rm_chCrib.IsChecked = Globals.currRoomGroup.Crib;
                rm_chBreakfast.IsChecked = Globals.currRoomGroup.Breakfast;
            }
        }

        private void lvRoom_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Globals.currRoom = lvRoom.SelectedItem as Room;
            if (Globals.currRoom == null) return;
            rm_lblRoomId.Content = Globals.currRoom.Id;
            rm_tbRoomNumber.Text = Globals.currRoom.Number.ToString();
            rm_tbFloor.Text = Globals.currRoom.FloorNumber + "";
            rm_tbBuilding.Text = Globals.currRoom.BuildingNumber + "";


        }

        private void Button_SearchReservationClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var listCombination = (from Reservation in Globals.ctx.Reservations


                                       select new
                                       {
                                           Id = Reservation.Id,
                                           CustomerName = Reservation.CustomerGuest.Name,
                                           CheckInDate = Reservation.CheckIn,
                                           CheckOutDate = Reservation.CheckIn,
                                           GroupName = Reservation.RoomGroupSelected.Name,
                                           BedDescription = Reservation.RoomGroupSelected.BedDescription,
                                           Price = Reservation.Price,
                                           Paid = Reservation.Paid,
                                           Cancelled = Reservation.Cancelled,


                                       }).ToList();


                string reservationIdSearchStr = or_tbReservationId.Text;

                int reservationIdSearch;
                if (!int.TryParse(reservationIdSearchStr, out reservationIdSearch))
                {
                    MessageBox.Show("Input number is unvalid value.");
                }
                var reservationToSerch = (from rv in listCombination where rv.Id == reservationIdSearch select rv).ToList();

                if (reservationToSerch == null)
                {
                    MessageBox.Show("Room not found.");
                    return;
                }

                lvCombination.ItemsSource = reservationToSerch;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void Button_SearchPriceClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var listCombination = (from Reservation in Globals.ctx.Reservations


                                       select new
                                       {
                                           Id = Reservation.Id,
                                           CustomerName = Reservation.CustomerGuest.Name,
                                           CheckInDate = Reservation.CheckIn,
                                           CheckOutDate = Reservation.CheckIn,
                                           GroupName = Reservation.RoomGroupSelected.Name,
                                           BedDescription = Reservation.RoomGroupSelected.BedDescription,
                                           Price = Reservation.Price,
                                           Paid = Reservation.Paid,
                                           Cancelled = Reservation.Cancelled,


                                       }).ToList();

                string priceSearchStr = or_tbPrice.Text;

                decimal priceSerch;
                if (!decimal.TryParse(priceSearchStr, out priceSerch))
                {
                    MessageBox.Show("Input number is unvalid value.");
                }
                var priceToSerch = (from rv in listCombination where rv.Price < priceSerch select rv).ToList();

                if (priceToSerch == null)
                {
                    MessageBox.Show("Room not found.");
                    return;
                }

                lvCombination.ItemsSource = priceToSerch;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }
        }

        private void Button_ExportToPdfClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var listCombination = (from Reservation in Globals.ctx.Reservations
                                       select new
                                       {
                                           Id = Reservation.Id,
                                           CustomerName = Reservation.CustomerGuest.Name,
                                           CheckInDate = Reservation.CheckIn,
                                           CheckOutDate = Reservation.CheckIn,
                                           GroupName = Reservation.RoomGroupSelected.Name,
                                           Price = Reservation.Price

                                       }).ToList();

                Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 20, 20);
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream("ReservationReport.pdf", FileMode.Create));
                doc.Open();


                iTextSharp.text.Paragraph paragraph = new iTextSharp.text.Paragraph("Resevation Report of Hotel");
                doc.Add(paragraph);
                iTextSharp.text.Paragraph paragraph2 = new iTextSharp.text.Paragraph(" ");
                doc.Add(paragraph2);

                PdfPTable table = new PdfPTable(6);

                PdfPCell cell = new PdfPCell(new Phrase("Reservation Report", new iTextSharp.text.Font(iTextSharp.text.Font.NORMAL, 15f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK)));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(170, 170, 200);
                cell.Colspan = 6;
                cell.HorizontalAlignment = 1;
                table.AddCell(cell);

                table.AddCell("Reservation");
                table.AddCell("Customer");
                table.AddCell("Check In");
                table.AddCell("Check Out");
                table.AddCell("Package");
                //table.AddCell("Bed Description");
                table.AddCell("Price");
                //table.AddCell("Paid");
                //table.AddCell("Cancelled");


                foreach (var rv in listCombination)
                {
                    table.AddCell(rv.Id.ToString());
                    table.AddCell(rv.CustomerName);
                    table.AddCell(rv.CheckInDate.ToShortDateString());
                    table.AddCell(rv.CheckOutDate.ToShortDateString());
                    table.AddCell(rv.GroupName);
                    //table.AddCell(rv.BedDescription);
                    table.AddCell(rv.Price.ToString());
                    //table.AddCell(rv.Paid.ToString());
                    //table.AddCell(rv.Cancelled.ToString());
                }
                doc.Add(table);

                doc.Close();
                System.Diagnostics.Process.Start("ReservationReport.pdf");
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this, "Error connecting to Database:\n" + ex.Message, "Database Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // it is like system exit
            }

        }

        private void Button_SendPdfEmailClick(object sender, RoutedEventArgs e)
        {
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress(txtFromEmailID.Text);
            mail.To.Add(txtTOEmailID.Text);
            mail.Subject = txtSubject.Text;
            mail.Attachments.Add(new Attachment("ReservationReport.pdf"));
            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(txtFromEmailID.Text, txtPassword.Text);
            SmtpServer.EnableSsl = true;
            SmtpServer.Send(mail);
            MessageBox.Show("Email sent out.");
        }

      
    }
}

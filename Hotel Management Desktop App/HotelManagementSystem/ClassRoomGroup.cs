﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class RoomGroup
    {
        public RoomGroup()
        {
            this.Rooms = new HashSet<Room>();
        }
        [Key]
        public int Id { get; set; }

        [MaxLength(100), Required]
        public string Name { get; set; }

        [MaxLength]
        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public bool Breakfast { get; set; }

        [Required]
        public int AdultCapacity { get; set; }

        [Required]
        public int ChildCapacity { get; set; }

        [MaxLength]
        public string BedDescription { get; set; }

        [Required]
        public bool Crib { get; set; }

        public ICollection<Reservation> ReservationCollection { get; set; } //

        //public ICollection<RoomInGroup> RoomInGroupCollection { get; set; } //

        //public ICollection<PictureInGroup> PictureInGroupCollection { get; set; } //


        public virtual ICollection<Room> Rooms { get; set; }

        public virtual ICollection<Picture> Picturecollection { get; set; }

        public RoomGroup(string name, string description, decimal price, bool breakfast, int adultCapacity, int childCapacity, string bedDescription, bool crib)
        {
            Name = name;
            Description = description;
            Price = price;
            Breakfast = breakfast;
            AdultCapacity = adultCapacity;
            ChildCapacity = childCapacity;
            BedDescription = bedDescription;
            Crib = crib;
        }
/*
        [Obsolete("Only needed for serialization and materialization", true)]
        public RoomGroup() { }*/

        public RoomGroup(RoomGroup roomGroup)
        {
        }

    }
}

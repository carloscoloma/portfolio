﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class PictureInGroup
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("PictureSelected"), Required] //
        public int PictureId { get; set; }
        public Picture PictureSelected { get; set; }

        [ForeignKey("RoomGroupSelected"), Required] //
        public int RoomGroupId { get; set; }
        public RoomGroup RoomGroupSelected { get; set; }

        public PictureInGroup(int pictureId, int roomGroupId)
        {
            PictureId = pictureId;
            RoomGroupId = roomGroupId;
        }

        [Obsolete("Only needed for serialization and materialization", true)]
        public PictureInGroup() { }

        public PictureInGroup(PictureInGroup pictureInGroup)
        {
        }

    }
}

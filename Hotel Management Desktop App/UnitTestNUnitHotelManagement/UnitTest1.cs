﻿using System;
using HotelManagementSystem;
using NUnit.Framework;
using System.Linq;
using System.Data.Entity.Validation;

namespace UnitTestNUnitHotelManagement
{
    [TestFixture]
    public class UnitTest1
    {
        HotelContext context;

        [SetUp]
        public void Initialize()
        {
            context = new HotelContext();
        }

        //[Test]
        //public void test()
        //{
        //    Assert.True(true);
        //    //Assert.AreEqual(true, true);
        //    //Assert.That("hello", Is.Not.Null);
        //}

        [Test, Timeout(15000)]
        public void CheckAddDeleteCustomer()
        {
            Customer customer = new Customer("Gisella Williams", "123456789", "gisella@williams.com", "445566", "216 Elm street");
            context.Customers.Add(customer);
            context.SaveChanges();
            Customer customerInDB = (from c in context.Customers where c.Id == customer.Id select c).First();
            Assert.AreEqual(customerInDB, customer, "customers are different after insert");
            context.Customers.Remove(customer);
            context.SaveChanges();
            Customer customerInDBAfterDelete = (from c in context.Customers where c.Id == customer.Id select c).FirstOrDefault();
            Assert.IsNull(customerInDBAfterDelete);
        }

        [Test, Timeout(15000)]
        public void CheckAddCustomerException()
        {
            Customer customer = new Customer(null, "123456789", "gisella@williams.com", "445566", "216 Elm street");
            context.Customers.Add(customer);
            Assert.Throws<DbEntityValidationException>(() => context.SaveChanges());
        }

        [Test, Timeout(15000)]
        public void CheckAddCustomerAddressException()
        {
            Customer customer = new Customer("Gisella Williams", "123456789", "gisella@williams.com", "445566", null);
            context.Customers.Add(customer);
            Assert.Throws<DbEntityValidationException>(() => context.SaveChanges());
        }

        [Test, Timeout(15000)]
        public void CheckAddDeleteEmployee()
        {
            Employee employee = new Employee("Gisella Williams", "123456789", "Admin");
            context.Employees.Add(employee);
            context.SaveChanges();
            Employee employeeInDB = (from c in context.Employees where c.Id == employee.Id select c).First();
            Assert.AreEqual(employeeInDB, employee, "employees are different after insert");
            context.Employees.Remove(employee);
            context.SaveChanges();
            Employee employeeInDBAfterDelete = (from c in context.Employees where c.Id == employee.Id select c).FirstOrDefault();
            Assert.IsNull(employeeInDBAfterDelete);
        }

        [Test, Timeout(15000)]
        public void CheckAddEmployeeNameException()
        {
            Employee employee = new Employee(null, "123456789", "Admin");
            context.Employees.Add(employee);
            Assert.Throws<DbEntityValidationException>(() => context.SaveChanges());
        }

        [Test, Timeout(15000)]
        public void CheckAddEmployeePasswordException()
        {
            Employee employee = new Employee("Gisella Williams", null, "Admin");
            context.Employees.Add(employee);
            Assert.Throws<DbEntityValidationException>(() => context.SaveChanges());
        }

        [Test, Timeout(15000)]
        public void CheckAddDeleteRoom()
        {
            Room room = new Room(500, 10, 13);
            context.Rooms.Add(room);
            context.SaveChanges();
            Room roomInDB = (from c in context.Rooms where c.Id == room.Id select c).First();
            Assert.AreEqual(roomInDB, room, "rooms are different after insert");
            context.Rooms.Remove(room);
            context.SaveChanges();
            Room roomInDBAfterDelete = (from c in context.Rooms where c.Id == room.Id select c).FirstOrDefault();
            Assert.IsNull(roomInDBAfterDelete);
        }

        [Test, Timeout(15000)]
        public void CheckAddDeleteRoomGroup()
        {
            RoomGroup roomGroup = new RoomGroup("Junior", "Special Room", 10, true, 2, 1, "Bed big", false);
            context.RoomGroups.Add(roomGroup);
            context.SaveChanges();
            RoomGroup roomGroupInDB = (from c in context.RoomGroups where c.Id == roomGroup.Id select c).First();
            Assert.AreEqual(roomGroupInDB, roomGroup, "roomGroups are different after insert");
            context.RoomGroups.Remove(roomGroup);
            context.SaveChanges();
            RoomGroup roomGroupInDBAfterDelete = (from c in context.RoomGroups where c.Id == roomGroup.Id select c).FirstOrDefault();
            Assert.IsNull(roomGroupInDBAfterDelete);
        }

        [Test, Timeout(15000)]
        public void CheckAddGroupRoomException()
        {
            RoomGroup roomGroup = new RoomGroup(null, "Special Room", 10, true, 2, 1, "Bed big", false);
            context.RoomGroups.Add(roomGroup);
            Assert.Throws<DbEntityValidationException>(() => context.SaveChanges());
        }

        [Test, Timeout(15000)]
        public void CheckAddDeleteReservation()
        {
            Customer customer = new Customer("Gisella Williams", "123456789", "gisella@williams.com", "445566", "216 Elm street");
            context.Customers.Add(customer);
            context.SaveChanges();
            Employee employee = new Employee("Gisella Williams", "123456789", "Admin");
            context.Employees.Add(employee);
            context.SaveChanges();
            Room room = new Room(500, 10, 13);
            context.Rooms.Add(room);
            context.SaveChanges();
            RoomGroup roomGroup = new RoomGroup("Junior", "Special Room", 10, true, 2, 1, "Bed big", false);
            context.RoomGroups.Add(roomGroup);
            context.SaveChanges();
            Reservation reservation = new Reservation(customer.Id, employee.Id, DateTime.Now, DateTime.Now.AddDays(1), false, false, room.Id, roomGroup.Id, false, roomGroup.Price, false);
            context.Reservations.Add(reservation);
            context.SaveChanges();
            Reservation reservationInDB = (from c in context.Reservations where c.Id == reservation.Id select c).First();
            Assert.AreEqual(reservationInDB, reservation, "reservations are different after insert");
            context.Reservations.Remove(reservation);
            context.SaveChanges();
            Reservation reservationInDBAfterDelete = (from c in context.Reservations where c.Id == reservation.Id select c).FirstOrDefault();
            Assert.IsNull(reservationInDBAfterDelete);
            context.Customers.Remove(customer);
            context.SaveChanges();
            context.Employees.Remove(employee);
            context.SaveChanges();
            context.Rooms.Remove(room);
            context.SaveChanges();
            context.RoomGroups.Remove(roomGroup);
            context.SaveChanges();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectBlackjack.Models
{
    public class DeckDesign
    {
        public DesignTypeEnum DesignType { get; set; }
        public string DesignFolderPath
        {
            get
            {
                //return designFolderPath.ToString();
                switch (DesignType)
                {
                    case DesignTypeEnum.Standard:
                        return "/Content/Images/Standard/{0}.svg";
                    case DesignTypeEnum.PinUp:
                        return "/Content/Images/Pinup/{0}.jpg";
                    case DesignTypeEnum.PlayingArts:
                        return "/Content/Images/Playingarts/{0}.jpg";
                    default:
                        return "/Content/Images/Standard/{0}.svg";
                }
            }
        }

        public enum DesignTypeEnum
        {
            Standard = 0,
            PinUp = 1,
            PlayingArts = 2,
        }
    }
    public class Card
    {
        public enum SuitName { Clubs = 0, Diamonds = 1, Hearts = 2, Spades = 3 };
        public enum Name
        {
            Ace,
            Two,
            Three,
            Four,
            Five,
            Six,
            Seven,
            Eight,
            Nine,
            Ten,
            Jack,
            Queen,
            King,
        }


        [Range(0,51)]
        public int Position { get; set; }
        public int Number
        {
            get
            {
                return (Position % 13)+1;
            }
        }
        public String CardName
        {
            get
            {
                return ((Name)(Position % 13)).ToString();
            }
        }
        public String Suit
        {
            get
            {
                return ((SuitName)(Position / 13)).ToString() ;
            }
        }
        public int Value
        {
            get
            {
                return (Number > 10) ? 10 : Number;
            }
        }
        public Boolean IsAce
        {
            get
            {
                return Number == 1;
            }
        }

        private string imagePath;
        public string ImagePath {
            get
            {
                return imagePath;
            }
            set
            {
                imagePath = string.Format(value, (Position+1));
            }
        }
    }

    //public static IList<Card> cardsDeck = new List<Card>();

}
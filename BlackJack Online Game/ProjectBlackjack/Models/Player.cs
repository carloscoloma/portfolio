﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectBlackjack.Models
{
    public class Player
    {
        [Range(0, Int32.MaxValue)]
        public int Account { get; set; }

        private int _bet;
        [Range(0,100)]
        public int Bet {
            get
            {
                return _bet;
            }
            set
            {
                _bet = value;
                Account -= _bet;
            }
        }
        public Hand Hand { get; set; }
        public bool ShowBusted { get; set; }
        public bool ShowBlackJack { get; set; }
    }

    public class Hand
    {
        public List<Card> AllCards { get; set; }
        public Card AddCard {
            set
            {
                AllCards.Add(value);
                Points += value.Value;

                if (value.IsAce) HasAce = true;
                if ((HasAce) && (Points <= 11))
                {
                    FinalPoints = Points + 10;
                }
                else
                {
                    FinalPoints = Points;
                }

                if ((AllCards.Count() == 2) && (FinalPoints == 21)) IsBlackJack = true;
                //if ( (AllCards.Count() == 1 ) && (true) ) HasAce = true;
                if (FinalPoints > 21) IsBusted = true;
                if (FinalPoints >= 21) End = true;
                if (FinalPoints >= 17) IsSeventeen = true;

            }
        }
        public bool HasAce { get; set; }
        public bool IsBlackJack { get; set; }
        public bool IsBusted { get; set; }
        public int Points { get; set; }
        public int FinalPoints { get; set; }
        public bool End { get; set; }
        public bool IsSeventeen { get; set; }
    }
}
﻿using ProjectBlackjack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace ProjectBlackjack.Controllers
{
    public class BlackJackController : Controller
    {
        // GET: BlackJack
        public ActionResult Index()
        {
            InitializeDeckDesign(0);
            InitializeDeck();
            ShuffleDeck();
            CreatePlayer();
            CreateDealer();
            return View((List < Card >)Session["CardDeckDemo"]);
        }

        [HttpPost]
        public ActionResult Index(int deckDesignPassed)
        {
            ReInitializeDeckDesign(deckDesignPassed);
            //InitializeDeck();
            //ShuffleDeck();
            return View((List<Card>)Session["CardDeckDemo"]);
        }

        //public ActionResult Index2()
        //{
        //    return View((List<Card>)Session["CardDeck"]);
        //}

        [NonAction]
        public void InitializeDeckDesign(int value)
        {
            DeckDesign deckDesign = new DeckDesign()
            { DesignType = (DeckDesign.DesignTypeEnum)Enum.Parse(typeof(DeckDesign.DesignTypeEnum), value.ToString()) };

            Session["DeckDesign"] = deckDesign;
        }

        [NonAction]
        public void ReInitializeDeckDesign(int value)
        {
            DeckDesign deckDesign = new DeckDesign()
            { DesignType = (DeckDesign.DesignTypeEnum)Enum.Parse(typeof(DeckDesign.DesignTypeEnum), value.ToString()) };

            Session["DeckDesign"] = deckDesign;
            IList<Card> cardDeck = (List<Card>)Session["CardDeck"];
            IList<Card> cardDeckDemo = (List<Card>)Session["CardDeckDemo"];
            foreach (Card card in cardDeck)
            {
                card.ImagePath = ((DeckDesign)Session["DeckDesign"]).DesignFolderPath;
            }
            foreach (Card cardDemo in cardDeckDemo)
            {
                cardDemo.ImagePath = ((DeckDesign)Session["DeckDesign"]).DesignFolderPath;
            }
            Session["CardDeck"] = cardDeck;
            Session["CardDeckDemo"] = cardDeckDemo;
        }


        [NonAction]
        public void InitializeDeck()
        {
            IList<Card> cardDeck = new List<Card>();
            IList<Card> cardDeckDemo = new List<Card>();
            for (int i = 0; i < 52; i++)
            {
                cardDeck.Add(new Card() { Position = i, ImagePath = ((DeckDesign)Session["DeckDesign"]).DesignFolderPath });
                cardDeckDemo.Add(new Card() { Position = i, ImagePath = ((DeckDesign)Session["DeckDesign"]).DesignFolderPath });
            }
            Session["CardDeck"] = cardDeck;
            Session["CardDeckDemo"] = cardDeckDemo;
        }

        [NonAction]
        public void ShuffleDeck()
        {
            //  from wikipedia:
            //  http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
            Random rand = new Random();
            IList<Card> cardDeck = (List<Card>)Session["CardDeck"];
            for (int i = cardDeck.Count - 1; i > 0 ; i--)
            {
                int randomPosition = rand.Next(i+1);
                Card tempCard = cardDeck[i];
                cardDeck[i] = cardDeck[randomPosition];
                cardDeck[randomPosition] = tempCard;
            }
            Session["CardDeck"] = cardDeck;
        }

        // GET: BlackJack/Play
        public ActionResult Play()
        {
            if (Session["CardDeck"] is null)
            {
                return RedirectToAction("Index");
            }
            Player dealer = (Player)Session["Dealer"];
            Player player = (Player)Session["Player"];
            player.Hand = new Hand();
            player.Hand.AllCards = new List<Card>();
            dealer.Hand = new Hand();
            dealer.Hand.AllCards = new List<Card>();
            Session["Dealer"] = dealer;
            Session["Player"] = player;
            Session["Winner"] = "Empty";
            return View((Player)Session["Player"]);
        }

        [NonAction]
        public void CreatePlayer()
        {
            Player player = new Player();
            player.Account = 500;
            player.Bet = 0;
            player.Hand = new Hand();
            player.Hand.AllCards = new List<Card>();
            
            Session["Player"] = player;
        }

        [NonAction]
        public void CreateDealer()
        {
            Player dealer = new Player();
            //dealer.Account = new Balance();
            //dealer.Bet = new Balance();
            dealer.Hand = new Hand();
            dealer.Hand.AllCards = new List<Card>();
            Session["Dealer"] = dealer;
        }

        [HttpGet]
        public PartialViewResult AjaxPlayerHit()
        {
            Player player = (Player)Session["Player"];
            if (player.Hand.End)
            {
                return PartialView("Empty");
            }

            if ( ((List<Card>)Session["CardDeck"]).Count < 11 )
            {
                InitializeDeck();
                ShuffleDeck();
            }
            IList<Card> cardDeck = (List<Card>)Session["CardDeck"];

            
            Card newCard = cardDeck.FirstOrDefault();
            player.Hand.AddCard = newCard;
            cardDeck.Remove(newCard);

            player.ShowBusted = player.Hand.IsBusted;
            player.ShowBlackJack = player.Hand.IsBlackJack;

            Session["CardDeck"] = cardDeck;
            Session["Player"] = player;

            return PartialView("AjaxAddCard", newCard);
        }

        [HttpGet]
        public PartialViewResult AjaxDealerHit()
        {
            Player dealer = (Player)Session["Dealer"];
            Player player = (Player)Session["Player"];
            if (dealer.Hand.End)
            {
                return PartialView("Empty");
            }
            if ((!player.Hand.End) && (dealer.Hand.AllCards.Count>=1))
            {
                return PartialView("Empty");
            }

            if (((List<Card>)Session["CardDeck"]).Count < 11)
            {
                InitializeDeck();
                ShuffleDeck();
            }

            IList<Card> cardDeck = (List<Card>)Session["CardDeck"];


            Card newCard = cardDeck.FirstOrDefault();
            dealer.Hand.AddCard = newCard;
            cardDeck.Remove(newCard);

            dealer.ShowBusted = dealer.Hand.IsBusted;
            dealer.ShowBlackJack = dealer.Hand.IsBlackJack;

            if (dealer.Hand.IsSeventeen)
            {
                dealer.Hand.End = true;
            }
            if ((player.Hand.IsBusted) && (dealer.Hand.AllCards.Count > 1))
            {
                dealer.Hand.End = true;
            }

            if ((player.Hand.End) && (dealer.Hand.End) )
            {
                CalculateWinner();
            }

            Session["CardDeck"] = cardDeck;
            Session["Dealer"] = dealer;

            return PartialView("AjaxAddCard", newCard);
        }

        [NonAction]
        public void CalculateWinner()
        {
            Player dealer = (Player)Session["Dealer"];
            Player player = (Player)Session["Player"];
            if (player.Hand.IsBlackJack && !dealer.Hand.IsBlackJack)
            {
                Session["Winner"] = "You Win!";
                player.Account += (int)(2.5 * ((double)player.Bet));
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
            if (player.Hand.IsBlackJack && dealer.Hand.IsBlackJack)
            {
                Session["Winner"] = "You Lose!";
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
            if (!player.Hand.IsBlackJack && dealer.Hand.IsBlackJack)
            {
                Session["Winner"] = "You Lose!";
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
            if (!player.Hand.IsBusted && dealer.Hand.IsBusted)
            {
                Session["Winner"] = "You Win!";
                player.Account += (2 * (player.Bet));
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
            if (player.Hand.IsBusted && dealer.Hand.IsBusted)
            {
                Session["Winner"] = "Push";
                player.Account += player.Bet;
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
            if (player.Hand.IsBusted && !dealer.Hand.IsBusted)
            {
                Session["Winner"] = "You Lose!";
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
            if (player.Hand.FinalPoints > dealer.Hand.FinalPoints)
            {
                Session["Winner"] = "You Win!";
                player.Account += (2 * (player.Bet));
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
            if (player.Hand.FinalPoints == dealer.Hand.FinalPoints)
            {
                Session["Winner"] = "Push";
                player.Account += player.Bet;
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
            if (player.Hand.FinalPoints < dealer.Hand.FinalPoints)
            {
                Session["Winner"] = "You Lose!";
                player.Bet = 0;
                Session["Player"] = player;
                Session["Dealer"] = dealer;
                return;
            }
        }

        [HttpGet]
        public PartialViewResult AjaxPlayerStand()
        {
            //Player player = (Player)Session["Player"];
            ((Player)Session["Player"]).Hand.End = true;
            //Session["Player"] = player;

            return PartialView("AjaxUpdatePlayerInfo", (Player)Session["Player"]);
        }

        [HttpGet]
        public PartialViewResult AjaxDealerStand()
        {
            //Player player = (Player)Session["Player"];
            ((Player)Session["Dealer"]).Hand.End = true;
            //Session["Player"] = player;

            return PartialView("AjaxUpdateDealerInfo", (Player)Session["Dealer"]);
        }

        [HttpGet]
        public PartialViewResult AjaxUpdatePlayerInfo()
        {
            return PartialView("AjaxUpdatePlayerInfo", (Player)Session["Player"]);
        }

        [HttpGet]
        public PartialViewResult AjaxUpdateDealerInfo()
        {
            return PartialView("AjaxUpdateDealerInfo", (Player)Session["Dealer"]);
        }

        [HttpGet]
        public PartialViewResult AjaxGetWinner()
        {
            return PartialView("AjaxGetWinner", (string)Session["Winner"]);
        }

        [HttpPost]
        public PartialViewResult AjaxSetPlayerBet(int betvalue)
        {
            Player player = (Player)Session["Player"];
            if (betvalue < 1) {
                return PartialView("AjaxUpdatePlayerInfo", player);
            }
            if (betvalue > 100) {
                return PartialView("AjaxUpdatePlayerInfo", player);
            }
            if (betvalue > player.Account) {
                return PartialView("AjaxUpdatePlayerInfo", player);
            }
            //player.Account -= betvalue;
            player.Bet = betvalue;
            Session["Player"] = player;

            return PartialView("AjaxUpdatePlayerInfo", (Player)Session["Player"]);
        }

        
    }
}

function addNewComment() {
  'use strict';
  console.log("in addNewComment function");

  var name = document.getElementById("visitorID").value;
  var newComment = document.getElementById("commentsArea").value;
  var isValid = true;

  if (name.length == 0) {
    console.log(" name is missing");
    document.getElementById("nameError").textContent = "Please enter your name.";

    document.getElementById("visitorID").classList.add("redborder");
    document.getElementById("nameError").classList.add("fieldRequired");

    isValid = false;
  } else {
    //clearing the error message
    document.getElementById("nameError").textContent = "";
    document.getElementById("visitorID").classList.remove("redborder");
    document.getElementById("nameError").classList.remove("fieldRequired");
  }

  if (newComment.length == 0) {
    console.log(" Comment is missing");
    document.getElementById("commentError").textContent = "Please enter your comments";

    document.getElementById("commentsArea").classList.add("redborder");
    document.getElementById("commentError").classList.add("fieldRequired")
    isValid = false;
  } else {
    //learing the error message
    document.getElementById("commentError").textContent = "";
    document.getElementById("commentsArea").classList.remove("redborder");
    document.getElementById("commentError").classList.remove("fieldRequired")
  }

  if (isValid) {
    document.getElementById("nameError").textContent = "";
    document.getElementById("commentError").textContent = "";
    document.getElementById("");

    var obj = document.querySelector(".commentsDiv ul");
    var listItemObj = document.createElement("li");
    listItemObj.classList.add("list-group-item");
    obj.appendChild(listItemObj);

    //displays current date and time
    var currentDate = new Date();
    listItemObj.textContent = name + " wrote on " + currentDate.toDateString() + ", at " +
      currentDate.toLocaleTimeString('en-US');

    //displays visitor comment
    var commentSpan = document.createElement("div");
    listItemObj.appendChild(commentSpan);
    commentSpan.innerHTML = document.getElementById("commentsArea").value;
    console.log(listItemObj.textContent);

    //nulifying the fields to prompt empty fieldsfor the next comment
    var name = document.getElementById("visitorID").value = "";
    var newComment = document.getElementById("commentsArea").value = "";
  }

  return false;
} // End of addNewComment() function.

function init() {
  'use strict';
  console.log("------in init --------");

  commentsFormID.onsubmit = addNewComment;
} // End of init() function.

document.addEventListener('DOMContentLoaded', init);

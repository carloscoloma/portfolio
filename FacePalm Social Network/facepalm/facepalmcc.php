<?php

// NOTE: This code must never execute but it helps Netbeans provide auto-completion
if (false) {
    $app = new \Slim\Slim();
    $log = new \Monolog\Logger('main');
}

function createDefaultAlbums($userId) {
    $albumName = "Profile Pictures";
    DB::insert('albums', array(
        'albumName' => $albumName,
        'albumUserId' => $userId,
        'editable' => 0
    ));
    $albumName = "Cover Photos";
    DB::insert('albums', array(
        'albumName' => $albumName,
        'albumUserId' => $userId,
        'editable' => 0
    ));
    $albumName = "Uploads";
    DB::insert('albums', array(
        'albumName' => $albumName,
        'albumUserId' => $userId,
        'editable' => 0
    ));    
}

function getSentRequestedFriendship($userId) {
//QUERY FRIENDS FROM DB
    $sentRequestedFriendshipIds = DB::queryFirstColumn("SELECT userAnswerId FROM friends WHERE userRequestId=%i AND friendshipAccepted=%i", $userId, 0);
    return $sentRequestedFriendshipIds;
}

function getReceivedRequestedFriendship($userId) {
//QUERY FRIENDS FROM DB
    $receivedRequestedFriendshipIds = DB::queryFirstColumn("SELECT userRequestId FROM friends WHERE userAnswerId=%i AND friendshipAccepted=%i", $userId, 0);
    return $receivedRequestedFriendshipIds;
}
        
$app->get('/:user/friends', function($user) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $friendList = getFriendsList($userId); // id, firstName, lastName, friendname
    $navcolor['friends'] = "text-white";
    $app->render('user_friends.html.twig', array(
        'user' => $_SESSION['user'],
        'friendList' => $friendList,
        'navcolor' => $navcolor
    ));
});

$app->get('/:user/search/:name', function($user, $name) use ($app, $log) {
    if ($user != $_SESSION['user']['username'] ) {
        $app->notFound();
        return;
    }
    $name = trim($name); 
    $userId = $_SESSION['user']['id'];
    
    $friendIds = getFriends($userId);
//    array_push ( $friendIds, $userId);
    $sentRequestedFriendshipIds = getSentRequestedFriendship($userId);
    $receivedRequestedFriendshipIds = getReceivedRequestedFriendship($userId);
//    print_r($friendIds); 
//    print_r('<br><br>');
//    print_r($sentRequestedFriendshipIds); 
//    print_r('<br><br>');
//    print_r($receivedRequestedFriendshipIds); 
//    print_r('<br><br>');
    $friendsAndSentAndReceivedRequestIds = array_merge($friendIds, $sentRequestedFriendshipIds, $receivedRequestedFriendshipIds);
    array_push ( $friendsAndSentAndReceivedRequestIds, $userId);
    
    $searchedAndFriends = array();
    if ($friendIds) {
    $searchedAndFriends = DB::query("SELECT id, firstName, lastName, profilePictureId FROM users WHERE (firstName LIKE %ss OR lastName LIKE %ss) AND id IN %li", $name, $name, $friendIds);}
//    if (!$searchedAndFriends) {$searchedAndFriends = array();}
    
    $searchedAndSentRequest = array();
    if ($sentRequestedFriendshipIds){
    $searchedAndSentRequest = DB::query("SELECT id, firstName, lastName, profilePictureId FROM users WHERE (firstName LIKE %ss OR lastName LIKE %ss) AND id IN %li", $name, $name, $sentRequestedFriendshipIds);}
//    if (!$searchedAndSentRequest) {$searchedAndSentRequest = array();}
    
    $searchedAndReceivedRequest = array();
    if ($receivedRequestedFriendshipIds){
    $searchedAndReceivedRequest = DB::query("SELECT id, firstName, lastName, profilePictureId FROM users WHERE (firstName LIKE %ss OR lastName LIKE %ss) AND id IN %li", $name, $name, $receivedRequestedFriendshipIds);}
    
    $searchedNotFriendsNotSentNotReceivedRequest = array();
    $searchedNotFriendsNotSentNotReceivedRequest = DB::query("SELECT id, firstName, lastName, profilePictureId FROM users WHERE (firstName LIKE %ss OR lastName LIKE %ss) AND id NOT IN %li", $name, $name, $friendsAndSentAndReceivedRequestIds);
//    print_r($userList);    
//    
    $friendList = getFriendsList($userId); // id, firstName, lastName, friendname  

    $app->render('user_search.html.twig', array(
        'user' => $_SESSION['user'],
        'friendList' => $friendList, 
        'searchedAndFriends' => $searchedAndFriends, 
        'searchedAndReceivedRequest' => $searchedAndReceivedRequest, 
        'searchedAndSentRequest' => $searchedAndSentRequest, 
        'searchedNotFriendsNotSentNotReceivedRequest' => $searchedNotFriendsNotSentNotReceivedRequest
            ));
});

$app->get('/:user/search/:usersearchedid/image/:profilePictureId', function($user, $usersearchedid, $profilePictureId) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $realPictureId = DB::queryFirstRow('SELECT profilePictureId FROM users WHERE id=%i AND profilePictureId=%i', $usersearchedid, $profilePictureId);
    if (!$realPictureId) {
        $app->notFound();
        return;
    }
    $pictureInfo = DB::queryFirstRow('SELECT * FROM pictures WHERE id=%i ', $profilePictureId);
    if (!$pictureInfo) {
        $app->notFound();
        return;
    }    
    $app->response()->header('content-type', $pictureInfo['mimeType']);
    echo $pictureInfo['pictureBlob'];
});

$app->get('/:user/pictures', function($user) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $navcolor['pictures'] = "text-white";
    $userId = $_SESSION['user']['id'];
    $friendList = getFriendsList($userId);
    $albumList = DB::query('SELECT * FROM albums WHERE albumUserId=%i', $userId);
//    $arrAlbumlength = count($albumList);
//    for ($i = 0; $i < $arrAlbumlength; $i++){
//        $albumList[$i]['albumCoverPictureId'] = DB::queryFirstField('SELECT id FROM pictures WHERE pictureAlbumId=%i', $albumList[$i]['id']);
////        print_r($albumList[$i]['albumCoverPictureId']);
////        print_r('<br><br>');
//    }
//    print_r('<br><br>');
//    print_r($albumList);
//    return;
        $app->render('user_pictures.html.twig', array(
        'user' => $_SESSION['user'], 
        'albumList' => $albumList,
        'friendList' => $friendList,
        'navcolor' => $navcolor
                
            ));
});

$app->delete('/:user/unfriend/:otheruserid', function($user, $otheruserid) use ($app, $log) {//FROM CRUD on DB: Delete
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }    
    $userId = $_SESSION['user']['id'];
    $friendshipId1 = DB::queryFirstField("SELECT id FROM friends WHERE userRequestId=%i AND userAnswerId=%i", $userId, $otheruserid);
    $friendshipId2 = DB::queryFirstField("SELECT id FROM friends WHERE userRequestId=%i AND userAnswerId=%i", $otheruserid, $userId);
    $friendshipIdReal = $friendshipId1 ? $friendshipId1 : $friendshipId2;
    if (!$friendshipIdReal) {
        $app->notFound();
        return;
    }    
    DB::delete('friends', 'id=%i', $friendshipIdReal);
    echo json_encode(DB::affectedRows() != 0);
});

$app->put('/:user/acceptrequest/:otheruserid', function($user, $otheruserid) use ($app, $log) {//FROM CRUD on DB: Delete
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $json = $app->request()->getBody();
    $friendship = json_decode($json, true); 
    if ($friendship['friendshipAccepted'] != 1) {
        $log->err("PUT /friends failed: " . 'invalid accept value');
        $app->response()->status(400);
        echo json_encode($result);
        return;
    }
    $result = isFriendshipValid($friendship);
    if ($result !== TRUE) {
        $log->err("PUT /friends failed: " . $result);
        $app->response()->status(400);
        echo json_encode($result);
        return;
    }
    $verifyRequestId = DB::queryFirstField("SELECT id FROM friends WHERE userRequestId=%i AND userAnswerId=%i AND friendshipAccepted=%i", $otheruserid, $userId, 0);
    if (!$verifyRequestId) {
        $app->response()->status(404);
        echo json_encode(false);
    } else {
        DB::update('friends', $friendship, 'id=%i', $verifyRequestId);
        $log->debug(sprintf("PUT /friends/%s accept request succeeded", $id));
        echo json_encode(true);
    }    
});

$app->post('/:user/addfriend/:otheruserid', function($user, $otheruserid) use ($app, $log) {//FROM CRUD on DB: Delete
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $json = $app->request()->getBody();
    $friendship = json_decode($json, true); 
    if ($friendship['friendshipAccepted'] != 0) {
        $log->err("POST /friends failed: " . 'invalid request value');
        $app->response()->status(400);
        echo json_encode($result);
        return;
    }    
    $result = isFriendshipValid($friendship);
    if ($result !== TRUE) {
        $log->err("POST /friends failed: " . $result);
        $app->response()->status(400);
        echo json_encode($result);
        return;
    }
    DB::insert('friends', $friendship);
    $app->response()->status(201);
    echo json_encode(DB::insertId());    
});

function isFriendshipValid($friendship) {
    if (is_null($friendship)) {
        return "JSON parsing failed";
    }
    if (count($friendship) != 3) {
        return "Invalid number of values";
    }
    if (!isset($friendship['userRequestId']) || !isset($friendship['userAnswerId']) || !isset($friendship['friendshipAccepted'])) {
        return "Required field missing";
    }
    if (!is_numeric($friendship['userRequestId']) || !is_numeric($friendship['userAnswerId'])) {
        return "userRequestId and userAnswerId must be numbers";
    }
    if ($friendship['friendshipAccepted'] != 0 && $friendship['friendshipAccepted'] != 1) {
        return "friendshipAccepted must be 0 or 1";
    }
    return TRUE;
}

$app->post('/:user/pictures', function($user) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $friendList = getFriendsList($userId);
    $navcolor['pictures'] = "text-white";
    $albumName = $app->request()->post('albumName');
    $albumDescription = $app->request()->post('albumDescription');
    $albumDescription = substr($albumDescription, 0, 1000);
    $description = array();
    $errorList = array();
    if (strlen($albumName) < 1 || strlen($albumName) > 100) {
        array_push($errorList, "Name must be 1-100 characters long");
    }
    
    $arrlength = count($_FILES["images"]["name"]);

    if (!$_FILES["images"]["tmp_name"][0]){
        array_push($errorList, "Load at least one image");
    }else{
        for ($i = 0; $i < $arrlength; $i++){
            $fileCounter = $i+1;
            $images = $_FILES['images'];
            $imageInfo = getimagesize($images['tmp_name'][$i]);
        //    print_r($image);
            if (!$imageInfo){
                array_push($errorList, "File " . $fileCounter . " does not look like a valid image");
            }else{
                // never allow '..' in the file name
                if (strstr($images['name'][$i], '..')){
                    array_push($errorList, "File " . $fileCounter . " name invalid");
                }
                // only allow select extensios
                $ext = strtolower(pathinfo($images['name'][$i], PATHINFO_EXTENSION));
                if (!in_array($ext, array('jpg', 'gif', 'jpeg', 'png'))){
                    array_push($errorList, "File " . $fileCounter . " extension invalid");
                }
                //check mime-type submted
        //        $mimeType = $image['mime'];
                $mimeType = $imageInfo['mime'];
                if (!in_array($mimeType, array('image/gif', 'image/jpeg', 'image/png'))){
                    array_push($errorList, "File " . $fileCounter . " type invalid");
                }        
            }        
        }        
    }
    if ($errorList) {
//        $app->render('passport_add.html.twig', array(
//            'v' => $valueList,
//            'errorList' => $errorList
//        ));
          print_r($errorList);  
        $albumList = DB::query('SELECT * FROM albums WHERE albumUserId=%i', $userId);
//        
        $app->render('user_pictures.html.twig', array(
            'user' => $_SESSION['user'], 
            'albumList' => $albumList,
            'friendList' => $friendList,
            'errorList' => $errorList,
            'albumName' => $albumName,
            'albumDescription' => $albumDescription,
            'navcolor' => $navcolor                
        ));
    }else{
//        TODO
//        move_uploaded_file($image['tmp_name'], 'uploads/' . $image['name']);
        
        DB::insert('albums', array(
            'albumName' => $albumName,
            'albumUserId' => $userId,
            'description' => $albumDescription
        ));
        $albumId = DB::insertId();        
        for ($i = 0; $i < $arrlength; $i++){
            $fileCounter = $i+1;
            $images = $_FILES['images'];
            $imageInfo = getimagesize($images['tmp_name'][$i]);
            $imageData = file_get_contents($images['tmp_name'][$i]);
            $mimeType = $imageInfo['mime'];
//            $descriptionName = 
            $description = $app->request()->post('description_' . $i);
//            print_r($description);
           DB::insert('pictures', array(
                'pictureAlbumId' => $albumId,
                'mimeType' => $mimeType,
                'pictureBlob' => $imageData,
                'description' => $description
            ));   
        }
        $lastPictureId = DB::insertId(); //TODO: add albumCoverPictureId and thumbnail
        
        DB::update('albums', array(
          'albumCoverPictureId' => $lastPictureId
          ), "id=%i", $albumId);        
//        echo "success";
        $albumList = DB::query('SELECT * FROM albums WHERE albumUserId=%i', $userId);
//    print_r($albumList);
        $app->render('user_pictures.html.twig', array(
        'user' => $_SESSION['user'], 
        'albumList' => $albumList,
        'friendList' => $friendList,
        'navcolor' => $navcolor
            ));
    } 
   
});  



$app->delete('/:user/pictures/album/:albumid/delete', function($user, $albumid) use ($app, $log) {//FROM CRUD on DB: Delete
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }    
    $userId = $_SESSION['user']['id'];
    $albumInfo = DB::queryFirstRow('SELECT * FROM albums WHERE id=%i AND albumUserId=%i', $albumid, $userId);
    if (!$albumInfo) {
        $app->notFound();
        return;
    }    
    DB::delete('albums', 'id=%i', $albumid);
    echo json_encode(DB::affectedRows() != 0);
});

$app->get('/:user/pictures/album/:albumid/image/:pictureid', function($user, $albumid, $pictureid) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $albumInfo = DB::queryFirstRow('SELECT * FROM albums WHERE id=%i AND albumUserId=%i', $albumid, $userId);
    if (!$albumInfo) {
        $app->notFound();
        return;
    }
    $pictureInfo = DB::queryFirstRow('SELECT * FROM pictures WHERE id=%i AND pictureAlbumId=%i', $pictureid, $albumid);
    if (!$pictureInfo) {
        $app->notFound();
        return;
    }    
    $app->response()->header('content-type', $pictureInfo['mimeType']);
    echo $pictureInfo['pictureBlob'];
});

$app->get('/:user/pictures/album/:albumid', function($user, $albumid) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $albumInfo = DB::queryFirstRow('SELECT * FROM albums WHERE id=%i AND albumUserId=%i', $albumid, $userId);
    if (!$albumInfo) {
        $app->notFound();
        return;
    }   
    $navcolor['pictures'] = "text-white";
    $friendList = getFriendsList($userId);
    $pictureList = DB::query('SELECT * FROM pictures WHERE pictureAlbumId=%i', $albumid);
//    print_r($albumInfo);
//    print_r('<br><br>');
//    print_r($pictureList);
        $app->render('user_pictures_album.html.twig', array(
        'user' => $_SESSION['user'], 
        'albumInfo' => $albumInfo,
        'pictureList' => $pictureList,    
        'friendList' => $friendList,
        'navcolor' => $navcolor
            ));
});

$app->post('/:user/pictures/album/:albumid', function($user, $albumid) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $albumInfo = DB::queryFirstRow('SELECT * FROM albums WHERE id=%i AND albumUserId=%i', $albumid, $userId);
    if (!$albumInfo) {
        $app->notFound();
        return;
    }
    $pictureList = DB::query('SELECT * FROM pictures WHERE pictureAlbumId=%i', $albumid);
    $friendList = getFriendsList($userId);
    $navcolor['pictures'] = "text-white";
//    $albumName = $app->request()->post('albumName');
//    $albumDescription = $app->request()->post('albumDescription');
//    $albumDescription = substr($albumDescription, 0, 1000);
//    $description = array();
    $errorList = array();
//    if (strlen($albumName) < 1 || strlen($albumName) > 100) {
//        array_push($errorList, "Name must be 1-100 characters long");
//    }
//    
    $arrlength = count($_FILES["images"]["name"]);

    if (!$_FILES["images"]["tmp_name"][0]){
        array_push($errorList, "Load at least one image");
    }else{
        for ($i = 0; $i < $arrlength; $i++){
            $fileCounter = $i+1;
            $images = $_FILES['images'];
            $imageInfo = getimagesize($images['tmp_name'][$i]);
        //    print_r($image);
            if (!$imageInfo){
                array_push($errorList, "File " . $fileCounter . " does not look like a valid image");
            }else{
                // never allow '..' in the file name
                if (strstr($images['name'][$i], '..')){
                    array_push($errorList, "File " . $fileCounter . " name invalid");
                }
                // only allow select extensios
                $ext = strtolower(pathinfo($images['name'][$i], PATHINFO_EXTENSION));
                if (!in_array($ext, array('jpg', 'gif', 'jpeg', 'png'))){
                    array_push($errorList, "File " . $fileCounter . " extension invalid");
                }
                //check mime-type submted
        //        $mimeType = $image['mime'];
                $mimeType = $imageInfo['mime'];
                if (!in_array($mimeType, array('image/gif', 'image/jpeg', 'image/png'))){
                    array_push($errorList, "File " . $fileCounter . " type invalid");
                }        
            }        
        }        
    }
    if ($errorList) {
//        $app->render('passport_add.html.twig', array(
//            'v' => $valueList,
//            'errorList' => $errorList
//        ));
          print_r($errorList);  
        $albumList = DB::query('SELECT * FROM albums WHERE albumUserId=%i', $userId);
//        
        $app->render('user_pictures_album.html.twig', array(
        'user' => $_SESSION['user'], 
        'albumInfo' => $albumInfo,
        'errorList' => $errorList,    
        'pictureList' => $pictureList,    
        'friendList' => $friendList,
        'navcolor' => $navcolor
            ));        
    }else{
//        TODO
//        move_uploaded_file($image['tmp_name'], 'uploads/' . $image['name']);
        
//        DB::insert('albums', array(
//            'albumName' => $albumName,
//            'albumUserId' => $userId,
//            'description' => $albumDescription
//        ));
//        $albumId = DB::insertId();        
        for ($i = 0; $i < $arrlength; $i++){
            $fileCounter = $i+1;
            $images = $_FILES['images'];
            $imageInfo = getimagesize($images['tmp_name'][$i]);
            $imageData = file_get_contents($images['tmp_name'][$i]);
            $mimeType = $imageInfo['mime'];
//            $descriptionName = 
            $description = $app->request()->post('description_' . $i);
//            print_r($description);
           DB::insert('pictures', array(
                'pictureAlbumId' => $albumid,
                'mimeType' => $mimeType,
                'pictureBlob' => $imageData,
                'description' => $description
            ));   
        }
        $lastPictureId = DB::insertId(); //TODO: add albumCoverPictureId and thumbnail
        
        DB::update('albums', array(
          'albumCoverPictureId' => $lastPictureId
          ), "id=%i", $albumid); 
        
        if (($albumInfo['albumName']=="Profile Pictures")&&($albumInfo['editable']==0)){
            DB::update('users', array(
              'profilePictureId' => $lastPictureId
              ), "id=%i", $userId); 
            $_SESSION['user']['profilePictureId'] = $lastPictureId;       
        }
//        echo "success";
//        $albumList = DB::query('SELECT * FROM albums WHERE albumUserId=%i', $userId);
//    print_r($albumList);
        $pictureList = DB::query('SELECT * FROM pictures WHERE pictureAlbumId=%i', $albumid);
        $app->render('user_pictures_album.html.twig', array(
        'user' => $_SESSION['user'], 
        'albumInfo' => $albumInfo, 
        'pictureList' => $pictureList,    
        'friendList' => $friendList,
        'navcolor' => $navcolor
            )); 
        
    } 
   
});  
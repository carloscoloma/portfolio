<?php

session_start();
require_once 'vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

// declare all data for database access

if (false){
    // declare all data for database access
	DB::$user = 'facepalm';
	DB::$dbName = 'facepalm';
	DB::$password = 'bmIYiFiPjcyT2Zv4';
	DB::$port = 3333;
	DB::$host = 'localhost';
	DB::$encoding = 'utf8';
}else{
    // declare all data for database access
    DB::$user = 'cp4907_facepalm';
    DB::$dbName = 'cp4907_facepalm';
    DB::$password = 'bmIYiFiPjcyT2Zv4';
    DB::$encoding = 'utf8';
}


DB::$error_handler = 'db_error_handler';

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig() //rendering is useing twig
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'  // directory name for cache = /cache
);

$view->parserExtensions = [
        new \Slim\Views\TwigExtension(),
        new \Twig_Extension_Debug()
    ];
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates'); // directory name for templates = /templates

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    $app->render('fatal_error.html.twig');
    die; // don't want to keep going if a query broke
}

function getFriends($userId) {
//QUERY FRIENDS FROM DB
    $friends = DB::query("SELECT * FROM friends WHERE (userRequestId=%i OR userAnswerId=%i ) AND friendshipAccepted=%i", $userId, $userId, 1);
    $friendsIds = array(null);
    if ($friends) {
        $arrlength = count($friends);
        for ($i = 0; $i < $arrlength; $i++) {
            if ($friends[$i]['userRequestId'] != $_SESSION['user']['id']) {
                array_push($friendsIds, $friends[$i]['userRequestId']);
            } else {
                array_push($friendsIds, $friends[$i]['userAnswerId']);
            }
        }
    }
    return $friendsIds;
}

function createSessionVariables() {//CHANGE_CC
    $_SESSION['user']['username'] = strtolower($_SESSION['user']['firstName'] . $_SESSION['user']['lastName']); 
    $_SESSION['user']['albumProfilePicturesId'] = DB::queryFirstField("SELECT id FROM albums"
                    . " WHERE albumUserId=%i AND editable=%i AND albumName=%s", $_SESSION['user']['id'], 0, "Profile Pictures");
    $_SESSION['user']['albumCoverPhotosId'] = DB::queryFirstField("SELECT id FROM albums"
                    . " WHERE albumUserId=%i AND editable=%i AND albumName=%s", $_SESSION['user']['id'], 0, "Cover Photos");
    $_SESSION['user']['albumUploadsId'] = DB::queryFirstField("SELECT id FROM albums"
                    . " WHERE albumUserId=%i AND editable=%i AND albumName=%s", $_SESSION['user']['id'], 0, "Uploads"); 
    if (!$_SESSION['user']['profilePictureId']){
        $autoSelectImageIdFromAlbumProfilePictures = DB::queryFirstField("SELECT id FROM pictures"
                . " WHERE pictureAlbumId=%i ", $_SESSION['user']['albumProfilePicturesId']);
        if (!$autoSelectImageIdFromAlbumProfilePictures){
            $imageInfo = getimagesize('images/account-green.png');
            $imageData = file_get_contents('images/account-green.png');
            $mimeType = $imageInfo['mime'];
//            $descriptionName = 
            $description = 'Temporal profile picture';
//            print_r($description);
            DB::insert('pictures', array(
                'pictureAlbumId' => $_SESSION['user']['albumProfilePicturesId'],
                'mimeType' => $mimeType,
                'pictureBlob' => $imageData,
                'description' => $description
            ));
            $_SESSION['user']['profilePictureId'] = DB::insertId();
            DB::update('albums', array(
                      'albumCoverPictureId' => $_SESSION['user']['profilePictureId']
                      ), "id=%i", $_SESSION['user']['albumProfilePicturesId']);            
        } else {
            DB::update('users', array('profilePictureId' => $autoSelectImageIdFromAlbumProfilePictures), 'id=%i', $_SESSION['user']['id']);
            $_SESSION['user']['profilePictureId'] = $autoSelectImageIdFromAlbumProfilePictures;
        }
    }
}

function getFriendsList($userId) {
    $friendsIds = getFriends($userId);
    if (!$friendsIds) {
        $friendsIds = array(null);
    }
    $friendList = DB::query("SELECT * FROM users WHERE id IN %li", $friendsIds);
    for ($i = 0; $i < count($friendList); $i++) {
        $friendList[$i]['friendname'] = strtolower($friendList[$i]['firstName'] . $friendList[$i]['lastName']);
        unset($friendList[$i]['password']);
    }
    return $friendList;
}

function getAllPosts($userId) {
    $friendsIds = getFriends($userId);
    array_push($friendsIds, $userId);
    $posts = DB::query("SELECT * FROM posts WHERE postUserId IN %li", $friendsIds);

    if ($posts == null) {
        $commentList = array();
    } else {
        $postIds = DB::queryFirstColumn('SELECT id FROM posts WHERE postUserId IN %li', $friendsIds);
//        print_r($postIds);
//        print_r('<br>');
        $commentList = DB::query('SELECT * FROM comments where commentPostId IN %li', $postIds);
    }
    $navcolor['posts'] = "text-white";
    return array('user' => $_SESSION['user'], 'posts' => $posts, 'commentList' => $commentList, 'navcolor' => $navcolor);
}

function getMyPosts($userId) {
    $friendList = getFriendsList($userId);
    $posts = DB::query('SELECT * FROM posts WHERE postUserId=%i', $userId);

    if ($posts == null) {
        $commentList = array();
    } else {
        $postIds = DB::query('SELECT id FROM posts WHERE postUserId=%i', $userId);
        $commentList = DB::query('SELECT * FROM comments where commentPostId IN %li', $postIds);
    }
    $navcolor['posts'] = "text-white";
    return array('user' => $_SESSION['user'], 'posts' => $posts, 'commentList' => $commentList, 'friendList' => $friendList, 'navcolor' => $navcolor);
}

$app->get('/', function() use ($app) {
    // state 1: first show
    $app->render('signup.html.twig');
});

$app->post('/', function() use ($app) {
    // RECEIVING SUBMISSION FOR LOGIN
    $emailLogin = $app->request()->post('emailLogin');
    $passLogin = $app->request()->post('passLogin');
    // verify submission
    $isLoginSuccessful = false;

    if (isset($_POST['doLogin'])) {
        $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $emailLogin);
        if ($user && ($user['password'] == $passLogin)) {
            $isLoginSuccessful = true;
        }
//
        if ($isLoginSuccessful) {
            // state 2: successful submission
            unset($user['password']);
            $_SESSION['user'] = $user;
            createSessionVariables();
            $app->render('login_success.html.twig', array('user' => $_SESSION['user'])); //CHANGE_CC 
        } else {
            // state 3: failed submission
            $app->render('signup.html.twig', array('error' => true));
        }
    }

    if (isset($_POST['doRegister'])) {
        $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
        $recaptcha_secret = '6Le_nYwUAAAAALmHO_NCVsFeOE9z2HBcgM4fktsf';
        $recaptcha_response = $app->request()->post('recaptcha_response');

        // Make and decode POST request:
        $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response);
        $recaptcha = json_decode($recaptcha);		
        // RECEIVING SUBMISSION FOR REGISTRATION
        $fName = $app->request()->post('fName');
        $lName = $app->request()->post('lName');
        $email = $app->request()->post('email');
        $pass1 = $app->request()->post('pass1');
        $pass2 = $app->request()->post('pass2');
        $valueList = array('email' => $email, 'fName' => $fName, 'lName' => $lName);
        // verify submission
        $errorList = array();
        
	if (false){
            if ($recaptcha->score < 0.5) {
                array_push($errorList, "Very likely a bot");
            }
        }     
        $emaiExists = DB::queryFirstField('SELECT email FROM users WHERE email=%s', $email);
        if ($emaiExists) {
            array_push($errorList, "Email already registered");
            unset($valueList['email']);
        }        
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
            array_push($errorList, "Email is invalid");
            unset($valueList['email']);
        }
        if (strlen($fName) < 1 || strlen($fName) > 100) {
            array_push($errorList, "First name must be between 1-100 characters long");
            unset($valueList['fName']);
        }
        if (strlen($lName) < 1 || strlen($lName) > 100) {
            array_push($errorList, "Last name must be between 1-100 characters long");
            unset($valueList['lName']);
        }
        if ($pass1 != $pass2) {
            array_push($errorList, "Passwords do not match");
        } else {
            if (strlen($pass1) < 6) {
                array_push($errorList, "Password must be at least 6 characters long");
            }
        }

//
        if (!$errorList) {
            // state 2: successful submission
            DB::insert('users', array(
                'firstName' => $fName,
                'lastName' => $lName,
                'email' => $email,
                'password' => $pass1
            ));
            $userId = DB::insertId();
            createDefaultAlbums($userId);
            $user = DB::queryFirstRow("SELECT * FROM users WHERE id=%i", $userId);
            unset($user['password']);
            $_SESSION['user'] = $user;
            createSessionVariables();
            $app->render('login_success.html.twig', array('user' => $_SESSION['user']));
        } else {
            // state 3: failed submission
            $app->render('signup.html.twig', array(
                'v' => $valueList,
                'errorList' => $errorList
            ));
        }
    }
});

$app->get('/logout', function() use ($app) {
    unset($_SESSION['user']);
    $app->render('logout.html.twig');
});

$app->get('/:user/news', function($user) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];

    $values = getAllPosts($userId);
    //print_r($values['commentList']);
    $app->render('news.html.twig', $values);
});

$app->post('/:user/news', function($user) use ($app, $log) {
    $userId = $_SESSION['user']['id'];

    // verify submission
    $errorList = array();

    if (isset($_POST['doPost'])) {
        $message = $app->request()->post('message');

        if (isset($_POST['post_type']) && $_POST['post_type'] == 'image') {
            $image = $_FILES['image'];
            $imageInfo = getimagesize($image['tmp_name']);
            if (!$imageInfo) {
                array_push($errorList, "File does not look like a valid image");
            } else {
                // never allow '..' in the file name
                if (strstr($image['name'], '..')) {
                    array_push($errorList, "File name invalid");
                }
                // only allow select extensions
                $ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
                if (!in_array($ext, array('jpg', 'jpeg', 'gif', 'png'))) {
                    array_push($errorList, "File extension invalid");
                }
                $mimeType = $imageInfo['mime'];
                if (!in_array($mimeType, array('image/gif', 'image/jpeg', 'image/png'))) {
                    array_push($errorList, "File type invalid");
                }

                if (!$errorList) {
                    // state 2: successful submission
                    $imageData = file_get_contents($image['tmp_name']);
                    //TO FIX: Cannot add or update a child row: a foreign key constraint fails
                    DB::insert('pictures', array('pictureBlob' => $imageData, 'mimeType' => $mimeType, 'pictureAlbumId' => 1));
                    $pictureId = DB::insertId();
                    //print_r($pictureId);
                    DB::insert('posts', array('message' => $message, 'postUserId' => $userId, 'postPictureId' => $pictureId));

                    $values = getAllPosts($userId);
                    $app->render('news.html.twig', $values);
                } else {
                    // state 3: failed submission
                    $values = getAllPosts($userId);
                    array_push($errorList, $values);
                    $app->render('news.html.twig', $values);
                }
            }
        } elseif (isset($_POST['post_type']) && $_POST['post_type'] == 'video') {
            $video_url = $app->request()->post('video_url');
            $data = array();
            parse_str(parse_url($video_url, PHP_URL_QUERY), $data);
            $video_id = $data['v'];

            DB::insert('posts', array('message' => $message, 'postUserId' => $userId, 'postVideoId' => $video_id));
            $values = getAllPosts($userId);
            $app->render('news.html.twig', $values);
        } elseif (isset($_POST['post_type']) && $_POST['post_type'] == 'link') {
            $link_url = $app->request()->post('link_url');

            DB::insert('posts', array('message' => $message, 'postUserId' => $userId, 'link' => $link_url));
            $values = getAllPosts($userId);
            $app->render('news.html.twig', $values);
        } else {
            DB::insert('posts', array('message' => $message, 'postUserId' => $userId));
            $values = getAllPosts($userId);
            $app->render('news.html.twig', $values);
        }
    }

    if (isset($_POST['doComment'])) {
        $comment = $app->request()->post('comment');
        $postId = $app->request()->post('postId');
        
        if (strlen($comment) < 1 || strlen($comment) > 1000) {
            array_push($errorList, "Comment must be between 1 and 1000 characters");
        }

        if (!$errorList) {
            // state 2: successful submission
            DB::insert('comments', array('commentPostId' => $postId, 'commentUserId' => $userId, 'message' => $comment));
            $values = getAllPosts($userId);
            $app->render('news.html.twig', $values);
        } else {
            // state 3: failed submission
            $values = getAllPosts($userId);
            array_push($errorList, $values);
            $app->render('news.html.twig', $values);
        }
    }
});

$app->get('/:user/about', function($user) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $userData = DB::query('SELECT * FROM users WHERE id=%i', $userId);
    $friendList = getFriendsList($userId);
    $navcolor['about'] = "text-white";
    $app->render('user_about.html.twig', array('user' => $_SESSION['user'], 'aboutMe' => $userData, 'friendList' => $friendList, 'navcolor' => $navcolor));
});

$app->post('/:user/about', function($user) use ($app, $log) {
    $userId = $_SESSION['user']['id'];
    $friendList = getFriendsList($userId);
    //GET FORM POST DATA
    $fName = $app->request()->post('fName');
    $lName = $app->request()->post('lName');
    $email = $app->request()->post('email');
    $phone = $app->request()->post('phone');
    $language = $app->request()->post('language');
    $bday = $app->request()->post('bday');
    $gender = $app->request()->post('gender');
    $location = $app->request()->post('location');

    $valueList = array('fname' => $fName, 'lName' => $lName, 'email' => $email, 'phone' => $phone, 'language' => $language, 'bday' => $bday, 'gender' => $gender, 'location' => $location);
    // verify submission
    $errorList = array();
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        array_push($errorList, "Email is invalid");
        unset($valueList['email']);
    }
    if (strlen($fName) < 1 || strlen($fName) > 100) {
        array_push($errorList, "First name must be between 1-100 characters long");
        unset($valueList['fName']);
    }
    if (strlen($lName) < 1 || strlen($lName) > 100) {
        array_push($errorList, "Last name must be between 1-100 characters long");
        unset($valueList['lName']);
    }
    if (!date("Y-m-d", strtotime($bday))) {
        array_push($errorList, "Date format is invalid");
        unset($valueList['bday']);
    }
    if (!preg_match("/^[0-9]{3}-[0-9]{3}-[0-9]{4}$/", $phone)) {
        array_push($errorList, "Phone format is invalid");
        unset($valueList['phone']);
    }
    $navcolor['post'] = "text-white";
//
    if (!$errorList) {
        // state 2: successful submission
        DB::update('users', array('firstName' => $fName, 'lastName' => $lName, 'email' => $email, 'phone' => $phone, 'language' => $language, 'birthday' => $bday, 'gender' => $gender, 'location' => $location), 'id=%i', $userId);
        $app->render('user_about.html.twig', array('friends' => $friendList, 'navcolor' => $navcolor));
    } else {
        // state 3: failed submission
        $app->render('user_about.html.twig', array('v' => $valueList, 'errorList' => $errorList, 'navcolor' => $navcolor));
    }
});

$app->get('/:user/posts', function($user) use ($app, $log) {
    if ($user != $_SESSION['user']['username']) {
        $app->notFound();
        return;
    }
    $userId = $_SESSION['user']['id'];
    $values = getMyPosts($userId);
    $app->render('user_posts.html.twig', $values);
});

$app->get('/:user/posts/:id/image', function($id) use ($app, $log) {
    $postImage = DB::query("SELECT image,mimeType FROM pictures WHERE id=%i", $id);
    if (!$postImage) {
        $app->notFound();
        return;
    }
    $app->response()->header('content-type', $postImage['mimeType']);
    echo $postImage['image'];
});

$app->post('/:user/posts', function($user) use ($app, $log) {
    $userId = $_SESSION['user']['id'];
    $navcolor['posts'] = "text-white";
    // verify submission
    $errorList = array();

    if (isset($_POST['doPost'])) {
        $message = $app->request()->post('message');

        if (isset($_POST['post_type']) && $_POST['post_type'] == 'image') {
            $image = $_FILES['image'];
            $imageInfo = getimagesize($image['tmp_name']);
            if (!$imageInfo) {
                array_push($errorList, "File does not look like a valid image");
            } else {
                // never allow '..' in the file name
                if (strstr($image['name'], '..')) {
                    array_push($errorList, "File name invalid");
                }
                // only allow select extensions
                $ext = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));
                if (!in_array($ext, array('jpg', 'jpeg', 'gif', 'png'))) {
                    array_push($errorList, "File extension invalid");
                }
                $mimeType = $imageInfo['mime'];
                if (!in_array($mimeType, array('image/gif', 'image/jpeg', 'image/png'))) {
                    array_push($errorList, "File type invalid");
                }

                if (!$errorList) {
                    // state 2: successful submission
                    $imageData = file_get_contents($image['tmp_name']);
                    //TO FIX: Cannot add or update a child row: a foreign key constraint fails
                    DB::insert('pictures', array('pictureBlob' => $imageData, 'mimeType' => $mimeType, 'pictureAlbumId' => 1));
                    $pictureId = DB::insertId();
                    //print_r($pictureId);
                    DB::insert('posts', array('message' => $message, 'postUserId' => $userId, 'postPictureId' => $pictureId));

                    $values = getMyPosts($userId);
                    $app->render('user_posts.html.twig', $values);
                } else {
                    // state 3: failed submission
                    $values = getMyPosts($userId);
                    array_push($values, $errorList);
                    $app->render('user_posts.html.twig', $values);
                }
            }
        } elseif (isset($_POST['post_type']) && $_POST['post_type'] == 'video') {
            $video_url = $app->request()->post('video_url');
            $data = array();
            parse_str(parse_url($video_url, PHP_URL_QUERY), $data);
            $video_id = $data['v'];

            DB::insert('posts', array('message' => $message, 'postUserId' => $userId, 'postVideoId' => $video_id));
            $values = getMyPosts($userId);
            $app->render('user_posts.html.twig', $values);
        } elseif (isset($_POST['post_type']) && $_POST['post_type'] == 'link') {
            $link_url = $app->request()->post('link_url');

            DB::insert('posts', array('message' => $message, 'postUserId' => $userId, 'link' => $link_url));
            $values = getMyPosts($userId);
            $app->render('user_posts.html.twig', $values);
        } else {
            DB::insert('posts', array('message' => $message, 'postUserId' => $userId));
            $values = getMyPosts($userId);
            $app->render('user_posts.html.twig', $values);
        }
    }

    if (isset($_POST['doComment'])) {
        $comment = $app->request()->post('comment');
        $postId = $app->request()->post('postId');

        if (strlen($comment) < 1 || strlen($comment) > 1000) {
            array_push($errorList, "Comment must be between 1 and 1000 characters");
        }

        if (!$errorList) {
            // state 2: successful submission
            DB::insert('comments', array('commentPostId' => $postId, 'commentUserId' => $userId, 'message' => $comment));
            $values = getMyPosts($userId);
            $app->render('user_posts.html.twig', $values);
        } else {
            // state 3: failed submission
            $values = getMyPosts($userId);
            array_push($errorList, $values);
            $app->render('user_posts.html.twig', $values);
        }
    }
});

require_once 'facepalmcc.php';

$app->run();


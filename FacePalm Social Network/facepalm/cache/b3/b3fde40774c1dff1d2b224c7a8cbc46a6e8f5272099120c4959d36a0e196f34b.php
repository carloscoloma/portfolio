<?php

/* master.html.twig */
class __TwigTemplate_4af65d0d1ace238c8af7f5a52ea00ee689db0387988a7eb2638699586e12b2c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css\" integrity=\"sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS\" crossorigin=\"anonymous\">
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js\" integrity=\"sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" integrity=\"sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k\" crossorigin=\"anonymous\"></script>
        <title>";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        echo " - FacePalm</title>
        <script>
            \$(document).ready(function () {
                \$(\"#searchUserText\").keyup(function () {
                    var inputText = \$(\"#searchUserText\").val();
                    var link = document.getElementById(\"searchUserAddress\");
                    link.setAttribute('href', \"/";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/search/\" + inputText);
                });
            });
        </script>
        ";
        // line 18
        $this->displayBlock('addhead', $context, $blocks);
        // line 20
        echo "    </head>

    <body>
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light \">
                <div class=\"col-md-1\"></div>            
                <a class=\"navbar-brand\" href=\"#\">FACEPALM</a>
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>

                <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                    <form class=\"form-inline my-2 my-lg-0\" method=\"post\">
                        <input class=\"form-control mr-sm-2\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\" name=\"searchUser\" id=\"searchUserText\">
                        <a href=\"\" id=\"searchUserAddress\" class=\"btn btn-outline-success my-2 my-sm-0\">Search</a>
";
        // line 35
        echo "                    </form>
                    <ul class=\"navbar-nav ml-auto\">
                        <li class=\"nav-item active\">
                            <a class=\"nav-link\" href=\"/";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/news\">News <span class=\"sr-only\">(current)</span></a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/posts\">Profile</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/friends\">Friends</a>
                        </li>
                        <li class=\"nav-item disabled\">
                            <a class=\"nav-link\" href=\"/";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/messages\">Messages</a>
                        </li>
                        <li class=\"nav-item disabled\">
                            <a class=\"nav-link\" href=\"/";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/about\">Settings</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/logout\">Log Out</a>
                        </li>
                    </ul>
                </div>
                <div class=\"col-md-1\"></div> 
        </nav>           

        <div id=\"centeredContent\" class=\"container-fluid\">
           
            ";
        // line 62
        $this->displayBlock('content', $context, $blocks);
        // line 64
        echo "        </div>
    </body>
</html>";
    }

    // line 8
    public function block_title($context, array $blocks = array())
    {
    }

    // line 18
    public function block_addhead($context, array $blocks = array())
    {
        echo "        
        ";
    }

    // line 62
    public function block_content($context, array $blocks = array())
    {
        // line 63
        echo "            ";
    }

    public function getTemplateName()
    {
        return "master.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 63,  129 => 62,  122 => 18,  117 => 8,  111 => 64,  109 => 62,  94 => 50,  88 => 47,  82 => 44,  76 => 41,  70 => 38,  65 => 35,  49 => 20,  47 => 18,  40 => 14,  31 => 8,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css\" integrity=\"sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS\" crossorigin=\"anonymous\">
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js\" integrity=\"sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" integrity=\"sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k\" crossorigin=\"anonymous\"></script>
        <title>{% block title %}{% endblock %} - FacePalm</title>
        <script>
            \$(document).ready(function () {
                \$(\"#searchUserText\").keyup(function () {
                    var inputText = \$(\"#searchUserText\").val();
                    var link = document.getElementById(\"searchUserAddress\");
                    link.setAttribute('href', \"/{{user.username}}/search/\" + inputText);
                });
            });
        </script>
        {% block addhead %}        
        {% endblock %}
    </head>

    <body>
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light \">
                <div class=\"col-md-1\"></div>            
                <a class=\"navbar-brand\" href=\"#\">FACEPALM</a>
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>

                <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                    <form class=\"form-inline my-2 my-lg-0\" method=\"post\">
                        <input class=\"form-control mr-sm-2\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\" name=\"searchUser\" id=\"searchUserText\">
                        <a href=\"\" id=\"searchUserAddress\" class=\"btn btn-outline-success my-2 my-sm-0\">Search</a>
{#                        <button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"submit\">Search</button>#}
                    </form>
                    <ul class=\"navbar-nav ml-auto\">
                        <li class=\"nav-item active\">
                            <a class=\"nav-link\" href=\"/{{user.username}}/news\">News <span class=\"sr-only\">(current)</span></a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/{{user.username}}/posts\">Profile</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/{{user.username}}/friends\">Friends</a>
                        </li>
                        <li class=\"nav-item disabled\">
                            <a class=\"nav-link\" href=\"/{{user.username}}/messages\">Messages</a>
                        </li>
                        <li class=\"nav-item disabled\">
                            <a class=\"nav-link\" href=\"/{{user.username}}/about\">Settings</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/logout\">Log Out</a>
                        </li>
                    </ul>
                </div>
                <div class=\"col-md-1\"></div> 
        </nav>           

        <div id=\"centeredContent\" class=\"container-fluid\">
           
            {% block content %}
            {% endblock %}
        </div>
    </body>
</html>", "master.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\master.html.twig");
    }
}

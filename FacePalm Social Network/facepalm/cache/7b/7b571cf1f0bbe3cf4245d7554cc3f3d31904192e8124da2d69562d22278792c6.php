<?php

/* user.html.twig */
class __TwigTemplate_ed106daa636fb1982b5880186b4fad9e9f4e49d58244c27959c193f7d927609e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "user.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "News";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <!-- main -->
<br>
<main class=\"container-fluid\" >
    <div class=\"row justify-content-md-center\">
        <div class=\"col-10\" id=\"grad2\">
            <!-- user profile -->
            <div class=\"media\">
                <div class=\"mr-3 \">
";
        // line 17
        echo "                    <a href=\"/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "albumProfilePicturesId", array()), "html", null, true);
        echo "/image/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "profilePictureId", array()), "html", null, true);
        echo "\">
                        <img src=\"/";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "albumProfilePicturesId", array()), "html", null, true);
        echo "/image/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "profilePictureId", array()), "html", null, true);
        echo "\"
                             alt=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo "\"
                             class=\"rounded-circle border border-white profilepicture\" style=\"width: auto; height: 150px;\"/>
                    </a>
                </div>
                <div class=\"media-body\">
                    <br>
                    <h2 class=\"mt-0 display-4\">";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo "</h2>
                    <p></p>
                </div>
            </div>
            <ul class=\"nav justify-content-center  lead\">
                <li class=\"nav-item \">
                    <a class=\"nav-link ";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute(($context["navcolor"] ?? null), "about", array()), "html", null, true);
        echo "\" href=\"/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/about\">About</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link ";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["navcolor"] ?? null), "friends", array()), "html", null, true);
        echo "\" href=\"/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/friends\">Friends</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link ";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute(($context["navcolor"] ?? null), "posts", array()), "html", null, true);
        echo "\" href=\"/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/posts\">Posts</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link ";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute(($context["navcolor"] ?? null), "pictures", array()), "html", null, true);
        echo "\" href=\"/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures\">Pictures</a>
                </li>
            </ul>
            <hr>
            ";
        // line 44
        $this->displayBlock('addsection', $context, $blocks);
        // line 46
        echo "        </div>

    </div>
</main>
<!-- ./main -->
";
    }

    // line 44
    public function block_addsection($context, array $blocks = array())
    {
        echo "        
            ";
    }

    public function getTemplateName()
    {
        return "user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 44,  123 => 46,  121 => 44,  112 => 40,  104 => 37,  96 => 34,  88 => 31,  77 => 25,  66 => 19,  58 => 18,  49 => 17,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}News{% endblock %}

{% block content %}
    <!-- main -->
<br>
<main class=\"container-fluid\" >
    <div class=\"row justify-content-md-center\">
        <div class=\"col-10\" id=\"grad2\">
            <!-- user profile -->
            <div class=\"media\">
                <div class=\"mr-3 \">
{#                    {{user.username}}<br>#}
{#                    {{user.albumProfilePicturesId}}<br>#}
{#                    {{user.profilePictureId}}<br>#}
                    <a href=\"/{{user.username}}/pictures/album/{{user.albumProfilePicturesId}}/image/{{user.profilePictureId}}\">
                        <img src=\"/{{user.username}}/pictures/album/{{user.albumProfilePicturesId}}/image/{{user.profilePictureId}}\"
                             alt=\"{{user.firstName}} {{user.lastName}}\"
                             class=\"rounded-circle border border-white profilepicture\" style=\"width: auto; height: 150px;\"/>
                    </a>
                </div>
                <div class=\"media-body\">
                    <br>
                    <h2 class=\"mt-0 display-4\">{{user.firstName}} {{user.lastName}}</h2>
                    <p></p>
                </div>
            </div>
            <ul class=\"nav justify-content-center  lead\">
                <li class=\"nav-item \">
                    <a class=\"nav-link {{navcolor.about}}\" href=\"/{{user.username}}/about\">About</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link {{navcolor.friends}}\" href=\"/{{user.username}}/friends\">Friends</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link {{navcolor.posts}}\" href=\"/{{user.username}}/posts\">Posts</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link {{navcolor.pictures}}\" href=\"/{{user.username}}/pictures\">Pictures</a>
                </li>
            </ul>
            <hr>
            {% block addsection %}        
            {% endblock %}
        </div>

    </div>
</main>
<!-- ./main -->
{% endblock %}", "user.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user.html.twig");
    }
}

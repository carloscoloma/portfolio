<?php

/* login_success.html.twig */
class __TwigTemplate_b49c0a87f48049107a7850348cf7d4066d6bb46e9dcd0f80ccc39f77f1a49dae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "login_success.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "first_name", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "last_name", array()), "html", null, true);
        echo " - Logged in";
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        echo "  
            <meta http-equiv=\"refresh\" content=\"3; url=";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/news\" />
        ";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
        // line 9
        echo "    <h1>You've been looged !!. You'll be redirected in 3 seconds</h1>
        <p>username: ";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo " <br></p>
        <p>email: ";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "email", array()), "html", null, true);
        echo " <br></p>
        <p>password: ";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "password", array()), "html", null, true);
        echo " <br></p>
        <p>first_name: ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "first_name", array()), "html", null, true);
        echo " <br></p>
        <p>last_name: ";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "last_name", array()), "html", null, true);
        echo " <br></p>
        <p><a href='friends'>Go to friends</a></p>
        <p><a href=\"/\">Go to index page </a></p>
";
    }

    public function getTemplateName()
    {
        return "login_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 14,  68 => 13,  64 => 12,  60 => 11,  56 => 10,  53 => 9,  50 => 8,  44 => 5,  39 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}{{user.first_name}} {{user.last_name}} - Logged in{% endblock %}
        {% block addhead %}  
            <meta http-equiv=\"refresh\" content=\"3; url={{user.username}}/news\" />
        {% endblock %}

{% block content %}
    <h1>You've been looged !!. You'll be redirected in 3 seconds</h1>
        <p>username: {{user.username}} <br></p>
        <p>email: {{user.email}} <br></p>
        <p>password: {{user.password}} <br></p>
        <p>first_name: {{user.first_name}} <br></p>
        <p>last_name: {{user.last_name}} <br></p>
        <p><a href='friends'>Go to friends</a></p>
        <p><a href=\"/\">Go to index page </a></p>
{% endblock %}", "login_success.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\login_success.html.twig");
    }
}

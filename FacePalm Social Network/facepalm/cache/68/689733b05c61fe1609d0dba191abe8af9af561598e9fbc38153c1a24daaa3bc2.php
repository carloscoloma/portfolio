<?php

/* user_pictures_album.html.twig */
class __TwigTemplate_3777aa8f97cc727cf9080edc452071e101499c14a2f2c27a4f738be3dc09c78a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_pictures_album.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 5
    public function block_addsection($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Picture list for album: ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["albumInfo"] ?? null), "albumName", array()), "html", null, true);
        echo " here</h1>
<div class=\"container\">
    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["pictureList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["picture"]) {
            // line 9
            echo "        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 10
                echo "            <div class=\"row\">
        ";
            }
            // line 12
            echo "        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "
                <a href=\"/";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/pictures/album/";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["albumInfo"] ?? null), "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["picture"], "id", array()), "html", null, true);
            echo "\">
                    <img src=\"/";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/pictures/album/";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["albumInfo"] ?? null), "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["picture"], "id", array()), "html", null, true);
            echo "\" height=\"100\"/>
                </a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        ";
            // line 24
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 25
                echo "            </div>
        ";
            }
            // line 26
            echo "    
        ";
            // line 27
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 28
                echo "            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 29
                    echo "                </div>
            ";
                }
                // line 30
                echo "  
        ";
            }
            // line 32
            echo "            <br>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['picture'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "    

</div>
";
    }

    public function getTemplateName()
    {
        return "user_pictures_album.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  123 => 32,  119 => 30,  115 => 29,  112 => 28,  110 => 27,  107 => 26,  103 => 25,  101 => 24,  87 => 17,  79 => 16,  75 => 15,  70 => 12,  66 => 10,  63 => 9,  46 => 8,  40 => 6,  37 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}

{% block addsection %}
    <h1>Picture list for album: {{albumInfo.albumName}} here</h1>
<div class=\"container\">
    {% for picture in pictureList %}
        {% if loop.index is odd %}
            <div class=\"row\">
        {% endif %}
        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>{{ loop.index }}
                <a href=\"/{{user.username}}/pictures/album/{{albumInfo.id}}/image/{{picture.id}}\">
                    <img src=\"/{{user.username}}/pictures/album/{{albumInfo.id}}/image/{{picture.id}}\" height=\"100\"/>
                </a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        {% if loop.index is even %}
            </div>
        {% endif %}    
        {% if loop.last %}
            {% if loop.index is odd %}
                </div>
            {% endif %}  
        {% endif %}
            <br>
    {% endfor %}    

</div>
{% endblock %}", "user_pictures_album.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_pictures_album.html.twig");
    }
}

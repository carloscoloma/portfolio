<?php

/* user_friends.html.twig */
class __TwigTemplate_ddadd3d8f1ca0448c7de35fc8fe09ffc38116592851d4a778ac920305b1b41f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_friends.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 5
    public function block_addsection($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Friend list for ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo " here</h1>
<div class=\"container\">
    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["friendList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["friend"]) {
            // line 9
            echo "        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 10
                echo "            <div class=\"row\">
        ";
            }
            // line 12
            echo "        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo " 
                <a href='/";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/friends/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "firstName", array()), "html", null, true);
            echo ".";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "lastName", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "id", array()), "html", null, true);
            echo "'><b>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "lastName", array()), "html", null, true);
            echo "</b></a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        ";
            // line 24
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 25
                echo "            </div>
        ";
            }
            // line 26
            echo "    
        ";
            // line 27
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 28
                echo "            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 29
                    echo "                </div>
            ";
                }
                // line 30
                echo "  
        ";
            }
            // line 32
            echo "            <br>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friend'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "    

</div>
";
    }

    public function getTemplateName()
    {
        return "user_friends.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 33,  124 => 32,  120 => 30,  116 => 29,  113 => 28,  111 => 27,  108 => 26,  104 => 25,  102 => 24,  83 => 18,  79 => 17,  72 => 12,  68 => 10,  65 => 9,  48 => 8,  40 => 6,  37 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}

{% block addsection %}
    <h1>Friend list for {{user.firstName}} {{user.lastName}} here</h1>
<div class=\"container\">
    {% for friend in friendList %}
        {% if loop.index is odd %}
            <div class=\"row\">
        {% endif %}
        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                {{ loop.index }} 
                <a href='/{{user.username}}/friends/{{friend.firstName}}.{{friend.lastName}}/{{friend.id}}'><b>{{friend.firstName}} {{friend.lastName}}</b></a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        {% if loop.index is even %}
            </div>
        {% endif %}    
        {% if loop.last %}
            {% if loop.index is odd %}
                </div>
            {% endif %}  
        {% endif %}
            <br>
    {% endfor %}    

</div>
{% endblock %}", "user_friends.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_friends.html.twig");
    }
}

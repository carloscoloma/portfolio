<?php

/* user_search.html.twig */
class __TwigTemplate_a2e8b4da2c30c0c6032ff6cd32a09b4b7b9d60c96b761b04b3275c44ac963dfd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_search.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 5
    public function block_addsection($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Search list for ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo " here</h1>
<div class=\"container\">
    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["userList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["userSearched"]) {
            // line 9
            echo "        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 10
                echo "            <div class=\"row\">
        ";
            }
            // line 12
            echo "        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                ";
            // line 17
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo " 
                ";
            // line 18
            if (($this->getAttribute($context["userSearched"], "addFriend", array()) == 0)) {
                // line 19
                echo "                    <br>
                    <a class=\"btn btn-info btn-sm\" href='/";
                // line 20
                echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
                echo "/addfriend/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
                echo "'><b>Add Friend</b></a>
                    <br>
                    <a href='/user/";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
                echo "'><b>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
                echo "</b></a>
                ";
            } else {
                // line 24
                echo "                    ";
                if (($this->getAttribute($context["userSearched"], "addFriend", array()) == 1)) {
                    // line 25
                    echo "                        <br>
                        <a class=\"btn btn-warning btn-sm\" href='/";
                    // line 26
                    echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
                    echo "/cancelrequest/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
                    echo "'><b>Cancel Request</b></a>                        
                        <br>
                        <a href='/user/";
                    // line 28
                    echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
                    echo "'><b>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
                    echo "</b></a>
                    ";
                } else {
                    // line 30
                    echo "                        <br>
                        <a href='/user/";
                    // line 31
                    echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
                    echo "'><b>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
                    echo "</b></a>                        
                    ";
                }
                // line 33
                echo "                ";
            }
            // line 34
            echo "                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        ";
            // line 39
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 40
                echo "            </div>
        ";
            }
            // line 41
            echo "    
        ";
            // line 42
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 43
                echo "            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 44
                    echo "                </div>
            ";
                }
                // line 45
                echo "  
        ";
            }
            // line 47
            echo "            <br>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userSearched'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "    

</div>
";
    }

    public function getTemplateName()
    {
        return "user_search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 48,  170 => 47,  166 => 45,  162 => 44,  159 => 43,  157 => 42,  154 => 41,  150 => 40,  148 => 39,  141 => 34,  138 => 33,  129 => 31,  126 => 30,  117 => 28,  110 => 26,  107 => 25,  104 => 24,  95 => 22,  88 => 20,  85 => 19,  83 => 18,  79 => 17,  72 => 12,  68 => 10,  65 => 9,  48 => 8,  40 => 6,  37 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}

{% block addsection %}
    <h1>Search list for {{user.firstName}} {{user.lastName}} here</h1>
<div class=\"container\">
    {% for userSearched in userList %}
        {% if loop.index is odd %}
            <div class=\"row\">
        {% endif %}
        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                {{ loop.index }} 
                {% if userSearched.addFriend == 0 %}
                    <br>
                    <a class=\"btn btn-info btn-sm\" href='/{{user.username}}/addfriend/{{userSearched.id}}'><b>Add Friend</b></a>
                    <br>
                    <a href='/user/{{userSearched.id}}'><b>{{userSearched.firstName}} {{userSearched.lastName}}</b></a>
                {% else %}
                    {% if userSearched.addFriend == 1 %}
                        <br>
                        <a class=\"btn btn-warning btn-sm\" href='/{{user.username}}/cancelrequest/{{userSearched.id}}'><b>Cancel Request</b></a>                        
                        <br>
                        <a href='/user/{{userSearched.id}}'><b>{{userSearched.firstName}} {{userSearched.lastName}}</b></a>
                    {% else %}
                        <br>
                        <a href='/user/{{userSearched.id}}'><b>{{userSearched.firstName}} {{userSearched.lastName}}</b></a>                        
                    {% endif %}
                {% endif %}
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        {% if loop.index is even %}
            </div>
        {% endif %}    
        {% if loop.last %}
            {% if loop.index is odd %}
                </div>
            {% endif %}  
        {% endif %}
            <br>
    {% endfor %}    

</div>
{% endblock %}", "user_search.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_search.html.twig");
    }
}

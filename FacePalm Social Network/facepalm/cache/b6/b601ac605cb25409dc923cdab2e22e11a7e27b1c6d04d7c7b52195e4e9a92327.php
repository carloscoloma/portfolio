<?php

/* pictures.html.twig */
class __TwigTemplate_92730d2df4779182cae8b64b31bde69a2c1a248736c0501f5d7617e629536f6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "pictures.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Album list for ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo " here</h1>
";
        // line 14
        echo "
<!-- Button trigger modal -->
<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModalCenter\">
  + Create new album
</button>

<!-- Modal -->
<div class=\"modal fade\" id=\"exampleModalCenter\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Album Creation</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
          Album name: <input type=\"text\" name=\"albumName\" value=\"\"><br><br>
        <button onclick=\"document.getElementById('inputCreateNewAlbum').click()\">
            Add pictures</button>
        <input type=\"file\" id=\"inputCreateNewAlbum\" 
               style=\"display:none\" accept=\"image/gif, image/jpeg, image/png\" multiple/>
        <br>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
        <button type=\"button\" class=\"btn btn-primary\">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class=\"container\">
    ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["albumList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["album"]) {
            // line 47
            echo "        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 48
                echo "            <div class=\"row\">
        ";
            }
            // line 50
            echo "        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                    <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                ";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo " 
                <a href='pictures/album/";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "id", array()), "html", null, true);
            echo "'><b>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "albumName", array()), "html", null, true);
            echo "</b></a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        ";
            // line 62
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 63
                echo "            </div>
        ";
            }
            // line 64
            echo "    
        ";
            // line 65
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 66
                echo "            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 67
                    echo "                </div>
            ";
                }
                // line 68
                echo "  
        ";
            }
            // line 70
            echo "            <br>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "    

</div>
";
    }

    public function getTemplateName()
    {
        return "pictures.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 71,  149 => 70,  145 => 68,  141 => 67,  138 => 66,  136 => 65,  133 => 64,  129 => 63,  127 => 62,  116 => 56,  112 => 55,  105 => 50,  101 => 48,  98 => 47,  81 => 46,  47 => 14,  40 => 6,  37 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}

{% block content %}
    <h1>Album list for {{user.firstName}} {{user.lastName}} here</h1>
{#        <p>username: {{user.username}} <br></p>
        <p>email: {{user.email}} <br></p>
        <p>password: {{user.password}} <br></p>
        <p>first_name: {{user.first_name}} <br></p>
        <p>last_name: {{user.last_name}} <br></p>
        <p><a href='friends'>Go to friends</a></p>
        <p><a href=\"/\">Go to index page </a></p>#}

<!-- Button trigger modal -->
<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#exampleModalCenter\">
  + Create new album
</button>

<!-- Modal -->
<div class=\"modal fade\" id=\"exampleModalCenter\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
    <div class=\"modal-content\">
      <div class=\"modal-header\">
        <h5 class=\"modal-title\" id=\"exampleModalLongTitle\">Album Creation</h5>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
          <span aria-hidden=\"true\">&times;</span>
        </button>
      </div>
      <div class=\"modal-body\">
          Album name: <input type=\"text\" name=\"albumName\" value=\"\"><br><br>
        <button onclick=\"document.getElementById('inputCreateNewAlbum').click()\">
            Add pictures</button>
        <input type=\"file\" id=\"inputCreateNewAlbum\" 
               style=\"display:none\" accept=\"image/gif, image/jpeg, image/png\" multiple/>
        <br>
      </div>
      <div class=\"modal-footer\">
        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
        <button type=\"button\" class=\"btn btn-primary\">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class=\"container\">
    {% for album in albumList %}
        {% if loop.index is odd %}
            <div class=\"row\">
        {% endif %}
        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                    <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                {{ loop.index }} 
                <a href='pictures/album/{{album.id}}'><b>{{album.albumName}}</b></a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        {% if loop.index is even %}
            </div>
        {% endif %}    
        {% if loop.last %}
            {% if loop.index is odd %}
                </div>
            {% endif %}  
        {% endif %}
            <br>
    {% endfor %}    

</div>
{% endblock %}", "pictures.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\pictures.html.twig");
    }
}

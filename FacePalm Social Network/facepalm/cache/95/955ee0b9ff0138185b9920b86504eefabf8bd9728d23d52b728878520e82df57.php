<?php

/* news.html.twig */
class __TwigTemplate_87bd5df35b7d176fe95b63a457b3b2ff8a187ca2d91bb397719583ee34c3ce50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "news.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "News";
    }

    // line 5
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script>
        \$(function () {
            \$('.extra_field').hide();
            \$(\"input[name='post_type']\").change(function () {
                \$('.extra_field').hide();
                \$('.' + \$(\"input[name='post_type']:checked\").val() + '_input').show();
            });
        });
    </script>
";
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        // line 18
        echo "    <main class=\"container-fluid\" >
        <div class=\"row justify-content-md-center\">
            <div class=\"col-10\" style=\"\">
                <!-- news feed -->
                <!--- \\\\\\\\\\\\\\Post-->
                
                <form method=\"post\" enctype=\"multipart/form-data\">
                    <div class=\"card gedf-card text-white bg-secondary\">
                        <div class=\"card-header navbar-dark bg-dark\">
                            <p><b>WHAT'S ON YOUR MIND?</b></p>
                        </div>
                        <div class=\"card-body\">
                            <div class=\"media\">
                                <div class=\"mr-3 \">        
                                    <a href=\"/";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "albumProfilePicturesId", array()), "html", null, true);
        echo "/image/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "profilePictureId", array()), "html", null, true);
        echo "\">
                                        <img src=\"/";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "albumProfilePicturesId", array()), "html", null, true);
        echo "/image/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "profilePictureId", array()), "html", null, true);
        echo "\" 
                                             class=\"rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"/>
                                    </a>
                                </div>
                                <div class=\"media-body\">

                            
                                    <div class=\"form-group\">
                                        <textarea id=\"status\" name=\"message\" style=\"width: 100%\"></textarea>
                                        <br />
                                        <input type=\"radio\" name=\"post_type\" value=\"update\" checked/>Update
                                        <input type=\"radio\" name=\"post_type\" value=\"video\" />Video
                                        <input type=\"radio\" name=\"post_type\" value=\"image\" />Image
                                        <input type=\"radio\" name=\"post_type\" value=\"link\" />Link
                                        <br />
                                        <div class=\"video_input extra_field\">
                                            <label for=\"video_url\" >YouTube URL</label>
                                            <input type=\"text\"  name=\"video_url\"  /><br />
                                        </div>
                                        <div class=\"image_input extra_field\">
                                            <label for=\"image_file\" >Upload image</label>
                                            <input type=\"file\"  name=\"image\"  /><br />
                                        </div>
                                        <div class=\"link_input extra_field\">
                                            <label for=\"link_url\" >Link</label>
                                            <input type=\"text\"  name=\"link_url\"  /><br />
                                        </div>
                                        <div class=\"btn-toolbar justify-content-between\">
                                            <div class=\"btn-group\">
                                                <input type=\"submit\" name=\"doPost\" class=\"btn btn-primary\" value=\"Post\">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                    
                        </div>
                    </div>
                </form>
                <br>
                <!-- Post /////-->

                <!--- \\\\\\\\\\\\\\Post-->
                ";
        // line 75
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 76
            echo "                    <div class=\"card gedf-card bg-light\">
                        <div class=\"card-header navbar-dark bg-dark\">
                            <div class=\"d-flex justify-content-between align-items-center\">
                                <div class=\"d-flex justify-content-between align-items-center\">
                                    <div class=\"mr-2\">
                                        <img class=\"rounded-circle\" width=\"45\" src='/images/mark-zuckerberg-4.jpg' alt=\"\">
                                    </div>
                                    <div class=\"ml-2\">
                                        <div class=\"h5 m-0\">@USERNAME</div>
                                        <div class=\"h7 text-muted\">Posted by ";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postUserId", array()), "html", null, true);
            echo "</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"card-body text-dark\">
                            <div class=\"text-muted h7 mb-2\"> <i class=\"fa fa-clock-o\"></i>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "timeStamp", array()), "html", null, true);
            echo "</div>
                            <a class=\"card-link\" href=\"#\">
                                <h5 class=\"card-title\">Title</h5>
                            </a>
                            <!-— message -->
                            <p class=\"card-text\">
                            <p>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "message", array()), "html", null, true);
            echo "</p>
                            <!-— ./message -->
                            ";
            // line 99
            if ($this->getAttribute($context["p"], "postPictureId", array())) {
                // line 100
                echo "                                <!-— image -->
                                <img src=\"/";
                // line 101
                echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
                echo "/posts/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postPictureId", array()), "html", null, true);
                echo "/image\" width=\"200\">
                                <!-— ./image -->
                            ";
            }
            // line 103
            echo "  

                            ";
            // line 105
            if ($this->getAttribute($context["p"], "link", array())) {
                // line 106
                echo "                                <!-— link -->
                                <a href=\"";
                // line 107
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "link", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "link", array()), "html", null, true);
                echo "</a>
                                <!-— ./link -->
                            ";
            }
            // line 109
            echo "   

                            ";
            // line 111
            if ($this->getAttribute($context["p"], "postVideoId", array())) {
                echo " 
                                <!-— video -->
                                <iframe width=\"420\" height=\"315\"
                                        src=\"https://www.youtube.com/embed/";
                // line 114
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postVideoId", array()), "html", null, true);
                echo "\">
                                </iframe>
                                <!-— ./video -->
                            ";
            }
            // line 118
            echo "                        </div>
                        <div class=\"card-footer text-dark\">
                            <form method=\"post\">
                                <textarea name=\"comment\" style=\"width: 100%\"></textarea>
                                <input type=\"hidden\" name=\"postId\" value=";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo " /><br>
                                <input type=\"submit\" name=\"doComment\" value=\"Comment\" class=\"btn btn-primary\"/>
                            </form>
                            ";
            // line 125
            if (($context["commentList"] ?? null)) {
                echo "   
                                ";
                // line 126
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["commentList"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                    // line 127
                    echo "                                    ";
                    if (($this->getAttribute($context["c"], "commentPostId", array()) === $this->getAttribute($context["p"], "id", array()))) {
                        // line 128
                        echo "                                        <p><b>";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "message", array()), "html", null, true);
                        echo "</b></p>
                                        <p>Posted by ";
                        // line 129
                        echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "commentUserId", array()), "html", null, true);
                        echo " on ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "timeStamp", array()), "html", null, true);
                        echo "</p> 
                                        <hr>
                                    ";
                    }
                    // line 132
                    echo "                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 133
                echo "                            ";
            }
            // line 134
            echo "                        </div>
                    </div>
                    <br>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 138
        echo "                <!-- Post /////-->
            </div>
        </div>
    </main>
";
    }

    public function getTemplateName()
    {
        return "news.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  267 => 138,  258 => 134,  255 => 133,  249 => 132,  241 => 129,  236 => 128,  233 => 127,  229 => 126,  225 => 125,  219 => 122,  213 => 118,  206 => 114,  200 => 111,  196 => 109,  188 => 107,  185 => 106,  183 => 105,  179 => 103,  171 => 101,  168 => 100,  166 => 99,  161 => 97,  152 => 91,  143 => 85,  132 => 76,  128 => 75,  79 => 33,  71 => 32,  55 => 18,  52 => 17,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}News{% endblock %}

{% block addhead %}   
    <script>
        \$(function () {
            \$('.extra_field').hide();
            \$(\"input[name='post_type']\").change(function () {
                \$('.extra_field').hide();
                \$('.' + \$(\"input[name='post_type']:checked\").val() + '_input').show();
            });
        });
    </script>
{% endblock %}

{% block content %}
    <main class=\"container-fluid\" >
        <div class=\"row justify-content-md-center\">
            <div class=\"col-10\" style=\"\">
                <!-- news feed -->
                <!--- \\\\\\\\\\\\\\Post-->
                
                <form method=\"post\" enctype=\"multipart/form-data\">
                    <div class=\"card gedf-card text-white bg-secondary\">
                        <div class=\"card-header navbar-dark bg-dark\">
                            <p><b>WHAT'S ON YOUR MIND?</b></p>
                        </div>
                        <div class=\"card-body\">
                            <div class=\"media\">
                                <div class=\"mr-3 \">        
                                    <a href=\"/{{user.username}}/pictures/album/{{user.albumProfilePicturesId}}/image/{{user.profilePictureId}}\">
                                        <img src=\"/{{user.username}}/pictures/album/{{user.albumProfilePicturesId}}/image/{{user.profilePictureId}}\" 
                                             class=\"rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"/>
                                    </a>
                                </div>
                                <div class=\"media-body\">

                            
                                    <div class=\"form-group\">
                                        <textarea id=\"status\" name=\"message\" style=\"width: 100%\"></textarea>
                                        <br />
                                        <input type=\"radio\" name=\"post_type\" value=\"update\" checked/>Update
                                        <input type=\"radio\" name=\"post_type\" value=\"video\" />Video
                                        <input type=\"radio\" name=\"post_type\" value=\"image\" />Image
                                        <input type=\"radio\" name=\"post_type\" value=\"link\" />Link
                                        <br />
                                        <div class=\"video_input extra_field\">
                                            <label for=\"video_url\" >YouTube URL</label>
                                            <input type=\"text\"  name=\"video_url\"  /><br />
                                        </div>
                                        <div class=\"image_input extra_field\">
                                            <label for=\"image_file\" >Upload image</label>
                                            <input type=\"file\"  name=\"image\"  /><br />
                                        </div>
                                        <div class=\"link_input extra_field\">
                                            <label for=\"link_url\" >Link</label>
                                            <input type=\"text\"  name=\"link_url\"  /><br />
                                        </div>
                                        <div class=\"btn-toolbar justify-content-between\">
                                            <div class=\"btn-group\">
                                                <input type=\"submit\" name=\"doPost\" class=\"btn btn-primary\" value=\"Post\">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                                    
                        </div>
                    </div>
                </form>
                <br>
                <!-- Post /////-->

                <!--- \\\\\\\\\\\\\\Post-->
                {% for p in posts %}
                    <div class=\"card gedf-card bg-light\">
                        <div class=\"card-header navbar-dark bg-dark\">
                            <div class=\"d-flex justify-content-between align-items-center\">
                                <div class=\"d-flex justify-content-between align-items-center\">
                                    <div class=\"mr-2\">
                                        <img class=\"rounded-circle\" width=\"45\" src='/images/mark-zuckerberg-4.jpg' alt=\"\">
                                    </div>
                                    <div class=\"ml-2\">
                                        <div class=\"h5 m-0\">@USERNAME</div>
                                        <div class=\"h7 text-muted\">Posted by {{p.postUserId}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=\"card-body text-dark\">
                            <div class=\"text-muted h7 mb-2\"> <i class=\"fa fa-clock-o\"></i>{{p.timeStamp}}</div>
                            <a class=\"card-link\" href=\"#\">
                                <h5 class=\"card-title\">Title</h5>
                            </a>
                            <!-— message -->
                            <p class=\"card-text\">
                            <p>{{p.message}}</p>
                            <!-— ./message -->
                            {% if p.postPictureId %}
                                <!-— image -->
                                <img src=\"/{{user.username}}/posts/{{p.postPictureId}}/image\" width=\"200\">
                                <!-— ./image -->
                            {% endif %}  

                            {% if p.link %}
                                <!-— link -->
                                <a href=\"{{p.link}}\">{{p.link}}</a>
                                <!-— ./link -->
                            {% endif %}   

                            {% if p.postVideoId %} 
                                <!-— video -->
                                <iframe width=\"420\" height=\"315\"
                                        src=\"https://www.youtube.com/embed/{{p.postVideoId}}\">
                                </iframe>
                                <!-— ./video -->
                            {% endif %}
                        </div>
                        <div class=\"card-footer text-dark\">
                            <form method=\"post\">
                                <textarea name=\"comment\" style=\"width: 100%\"></textarea>
                                <input type=\"hidden\" name=\"postId\" value={{p.id}} /><br>
                                <input type=\"submit\" name=\"doComment\" value=\"Comment\" class=\"btn btn-primary\"/>
                            </form>
                            {% if commentList %}   
                                {% for c in commentList %}
                                    {% if c.commentPostId is same as(p.id) %}
                                        <p><b>{{c.message}}</b></p>
                                        <p>Posted by {{c.commentUserId}} on {{c.timeStamp}}</p> 
                                        <hr>
                                    {% endif %}
                                {% endfor %}
                            {% endif %}
                        </div>
                    </div>
                    <br>
                {% endfor %}
                <!-- Post /////-->
            </div>
        </div>
    </main>
{% endblock %}", "news.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\news.html.twig");
    }
}

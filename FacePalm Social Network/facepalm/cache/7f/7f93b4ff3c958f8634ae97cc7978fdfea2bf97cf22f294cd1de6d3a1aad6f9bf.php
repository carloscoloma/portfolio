<?php

/* user.html.twig */
class __TwigTemplate_20a45670304ad02dcd555ab9f9cb749d82f5e9328620ee7bd833e6f1caa6bb37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "user.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "News";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <!-- main -->
<main class=\"container-fluid\" >
    <div class=\"row justify-content-md-center\">
            <!-- friends -->
            <div class=\"col-2\" style=\"background-color:buttonface\">
                <h4>Friends</h4>
                <div class=\"container\">
                    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["friendList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["friend"]) {
            // line 14
            echo "                        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 15
                echo "                            <div class=\"row\">
                            ";
            }
            // line 17
            echo "
                            <div class=\"col-5 border border-dark\">
                                <p>
                                    <br>
                                    <img src='/images/mark-zuckerberg-4.jpg' height=\"25\"/>
                                    ";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo " 
                                    <a href='/";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "friendname", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "id", array()), "html", null, true);
            echo "'><b>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "lastName", array()), "html", null, true);
            echo "</b></a>
                                    <br>
                                </p>
                            </div>
                            <div class=\"col-1 \"></div>

                            ";
            // line 29
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 30
                echo "                            </div>
                        ";
            }
            // line 31
            echo "    
                        ";
            // line 32
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 33
                echo "                            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 34
                    echo "                            </div>
                        ";
                }
                // line 35
                echo "  
                    ";
            }
            // line 37
            echo "                    <br>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friend'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "    

            </div>
        </div>
        <!-- ./friends -->
        <div class=\"col-6\" style=\"background-color:lavenderblush\">
            <!-- user profile -->
            <div class=\"media\">
                <div class=\"media-left\">
";
        // line 50
        echo "                    <a href=\"/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "albumProfilePicturesId", array()), "html", null, true);
        echo "/image/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "profilePictureId", array()), "html", null, true);
        echo "\">
                        <img src=\"/";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "albumProfilePicturesId", array()), "html", null, true);
        echo "/image/";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "profilePictureId", array()), "html", null, true);
        echo "\" 
                             class=\"media-object\" style=\"width: 128px; height: 128px;\"/>
                    </a>
                </div>
                <div class=\"media-body\">
                    <h2 class=\"media-heading\">";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo "</h2>
                    <p></p>
                </div>
            </div>
            <ul class=\"nav justify-content-center\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"/";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/about\">About</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"/";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/friends\">Friends</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"/";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/posts\">Posts</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"/";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures\">Pictures</a>
                </li>
            </ul>
            <hr>
            ";
        // line 75
        $this->displayBlock('addsection', $context, $blocks);
        // line 77
        echo "        </div>
        <div class=\"col-2\" style=\"background-color:buttonface\">
            <!-- notifications -->
            <h4>Notifications</h4>
            <!-- ./notifications -->
        </div>
    </div>
</main>
<!-- ./main -->
";
    }

    // line 75
    public function block_addsection($context, array $blocks = array())
    {
        echo "        
            ";
    }

    public function getTemplateName()
    {
        return "user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 75,  205 => 77,  203 => 75,  196 => 71,  190 => 68,  184 => 65,  178 => 62,  167 => 56,  155 => 51,  146 => 50,  135 => 38,  120 => 37,  116 => 35,  112 => 34,  109 => 33,  107 => 32,  104 => 31,  100 => 30,  98 => 29,  83 => 23,  79 => 22,  72 => 17,  68 => 15,  65 => 14,  48 => 13,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}News{% endblock %}

{% block content %}
    <!-- main -->
<main class=\"container-fluid\" >
    <div class=\"row justify-content-md-center\">
            <!-- friends -->
            <div class=\"col-2\" style=\"background-color:buttonface\">
                <h4>Friends</h4>
                <div class=\"container\">
                    {% for friend in friendList %}
                        {% if loop.index is odd %}
                            <div class=\"row\">
                            {% endif %}

                            <div class=\"col-5 border border-dark\">
                                <p>
                                    <br>
                                    <img src='/images/mark-zuckerberg-4.jpg' height=\"25\"/>
                                    {{ loop.index }} 
                                    <a href='/{{friend.friendname}}/{{friend.id}}'><b>{{friend.firstName}} {{friend.lastName}}</b></a>
                                    <br>
                                </p>
                            </div>
                            <div class=\"col-1 \"></div>

                            {% if loop.index is even %}
                            </div>
                        {% endif %}    
                        {% if loop.last %}
                            {% if loop.index is odd %}
                            </div>
                        {% endif %}  
                    {% endif %}
                    <br>
                {% endfor %}    

            </div>
        </div>
        <!-- ./friends -->
        <div class=\"col-6\" style=\"background-color:lavenderblush\">
            <!-- user profile -->
            <div class=\"media\">
                <div class=\"media-left\">
{#                    {{user.username}}<br>#}
{#                    {{user.albumProfilePicturesId}}<br>#}
{#                    {{user.profilePictureId}}<br>#}
                    <a href=\"/{{user.username}}/pictures/album/{{user.albumProfilePicturesId}}/image/{{user.profilePictureId}}\">
                        <img src=\"/{{user.username}}/pictures/album/{{user.albumProfilePicturesId}}/image/{{user.profilePictureId}}\" 
                             class=\"media-object\" style=\"width: 128px; height: 128px;\"/>
                    </a>
                </div>
                <div class=\"media-body\">
                    <h2 class=\"media-heading\">{{user.firstName}} {{user.lastName}}</h2>
                    <p></p>
                </div>
            </div>
            <ul class=\"nav justify-content-center\">
                <li class=\"nav-item\">
                    <a class=\"nav-link active\" href=\"/{{user.username}}/about\">About</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"/{{user.username}}/friends\">Friends</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"/{{user.username}}/posts\">Posts</a>
                </li>
                <li class=\"nav-item\">
                    <a class=\"nav-link\" href=\"/{{user.username}}/pictures\">Pictures</a>
                </li>
            </ul>
            <hr>
            {% block addsection %}        
            {% endblock %}
        </div>
        <div class=\"col-2\" style=\"background-color:buttonface\">
            <!-- notifications -->
            <h4>Notifications</h4>
            <!-- ./notifications -->
        </div>
    </div>
</main>
<!-- ./main -->
{% endblock %}", "user.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user.html.twig");
    }
}

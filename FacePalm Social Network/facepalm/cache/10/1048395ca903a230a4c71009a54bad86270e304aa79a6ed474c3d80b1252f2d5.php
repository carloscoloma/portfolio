<?php

/* signup.html.twig */
class __TwigTemplate_7ef8cfc02fbd4111133a491d92747e1731d57510064b1f1e9067c3c46f5f68f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
        <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
        <title>FacePalm</title>
    </head>
    <body>
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light\">
            <a class=\"navbar-brand\" href=\"#\">FACEPALM</a>
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
            </button>

            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                <form class=\"form-inline my-2 my-lg-0\" method=\"post\">
                    <input class=\"form-control mr-sm-2\" type=\"email\" name=\"emailLogin\" placeholder=\"Email\" aria-label=\"Search\">
                    <input class=\"form-control mr-sm-2\" type=\"password\" name=\"passLogin\" placeholder=\"Password\" aria-label=\"Search\">
                    <button class=\"btn btn-outline-success my-2 my-sm-0\" name=\"doLogin\" type=\"submit\">Login</button>
                </form>
            </div>
        </nav>
        <div class=\"row mt-5\">
            <div class=\"col-lg-4\">
                Connect with friends and the world around you on FacePalm.
                <br><br>
                <img class=\"rounded mx-auto d-block\" src='/images/Mega facepalm.gif' maxwidth=\"343\"/>
                <br><br>
            </div>
            <div class=\"col-lg-4\">

                ";
        // line 34
        if (($context["errorList"] ?? null)) {
            echo "    
                    <ul class=\"error\">
                        ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 37
                echo "                            <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "                    </ul>
                ";
        }
        // line 41
        echo "                <form method=\"post\">
                    <div class=\"form-group\">

                        <input class=\"form-control\" placeholder=\"Enter First Name\" type=\"text\" name=\"fName\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "fName", array()), "html", null, true);
        echo "\" ><br>
                        <input class=\"form-control\" placeholder=\"Enter Last Name\" type=\"text\" name=\"lName\" value=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "lName", array()), "html", null, true);
        echo "\" ><br>
                        <input class=\"form-control\" placeholder=\"Enter Email\" type=\"email\" name=\"email\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", array()), "html", null, true);
        echo "\" ><br>                 
                        <input class=\"form-control\" placeholder=\"Enter Password\" type=\"password\" name=\"pass1\"><br>
                        <input class=\"form-control\" placeholder=\"Retype Password\" type=\"password\" name=\"pass2\"><br>
                    </div>
                    <input type=\"submit\" class=\"btn\" value=\"Register\" name=\"doRegister\">
                </form>
            </div>
        </div>
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "signup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 46,  85 => 45,  81 => 44,  76 => 41,  72 => 39,  63 => 37,  59 => 36,  54 => 34,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
        <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
        <title>FacePalm</title>
    </head>
    <body>
        <nav class=\"navbar navbar-expand-lg navbar-light bg-light\">
            <a class=\"navbar-brand\" href=\"#\">FACEPALM</a>
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
            </button>

            <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                <form class=\"form-inline my-2 my-lg-0\" method=\"post\">
                    <input class=\"form-control mr-sm-2\" type=\"email\" name=\"emailLogin\" placeholder=\"Email\" aria-label=\"Search\">
                    <input class=\"form-control mr-sm-2\" type=\"password\" name=\"passLogin\" placeholder=\"Password\" aria-label=\"Search\">
                    <button class=\"btn btn-outline-success my-2 my-sm-0\" name=\"doLogin\" type=\"submit\">Login</button>
                </form>
            </div>
        </nav>
        <div class=\"row mt-5\">
            <div class=\"col-lg-4\">
                Connect with friends and the world around you on FacePalm.
                <br><br>
                <img class=\"rounded mx-auto d-block\" src='/images/Mega facepalm.gif' maxwidth=\"343\"/>
                <br><br>
            </div>
            <div class=\"col-lg-4\">

                {% if errorList %}    
                    <ul class=\"error\">
                        {% for error in errorList %}
                            <li>{{error}}</li>
                            {% endfor %}
                    </ul>
                {% endif %}
                <form method=\"post\">
                    <div class=\"form-group\">

                        <input class=\"form-control\" placeholder=\"Enter First Name\" type=\"text\" name=\"fName\" value=\"{{v.fName}}\" ><br>
                        <input class=\"form-control\" placeholder=\"Enter Last Name\" type=\"text\" name=\"lName\" value=\"{{v.lName}}\" ><br>
                        <input class=\"form-control\" placeholder=\"Enter Email\" type=\"email\" name=\"email\" value=\"{{v.email}}\" ><br>                 
                        <input class=\"form-control\" placeholder=\"Enter Password\" type=\"password\" name=\"pass1\"><br>
                        <input class=\"form-control\" placeholder=\"Retype Password\" type=\"password\" name=\"pass2\"><br>
                    </div>
                    <input type=\"submit\" class=\"btn\" value=\"Register\" name=\"doRegister\">
                </form>
            </div>
        </div>
    </body>
</html>", "signup.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\signup.html.twig");
    }
}

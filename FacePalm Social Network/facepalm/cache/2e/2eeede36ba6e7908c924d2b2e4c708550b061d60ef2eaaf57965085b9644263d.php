<?php

/* user_posts.html.twig */
class __TwigTemplate_6e34a1ece5fd09accdc63c4730952928dd8dec37ce1591f1b08cfbee074d179c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_posts.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Posts";
    }

    // line 5
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script type=\"text/javascript\">
        \$(function () {
            \$('.extra_field').hide();
            \$(\"input[name='post_type']\").change(function () {
                \$('.extra_field').hide();
                \$('.' + \$(\"input[name='post_type']:checked\").val() + '_input').show();
            });
        });
    </script>
";
    }

    // line 17
    public function block_addsection($context, array $blocks = array())
    {
        echo "     
    <!--- \\\\\\\\\\\\\\Post-->
    <form method=\"post\" enctype=\"multipart/form-data\">
        <div class=\"card gedf-card\">
            <div class=\"card-header\">
                <ul class=\"nav nav-tabs card-header-tabs\" id=\"myTab\" role=\"tablist\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link active\" id=\"posts-tab\" data-toggle=\"tab\" href=\"#posts\" role=\"tab\" aria-controls=\"posts\" aria-selected=\"true\">Message</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" id=\"images-tab\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"images\" aria-selected=\"false\" href=\"#images\">Images</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" id=\"videos-tab\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"videos\" aria-selected=\"false\" href=\"#videos\">Videos</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" id=\"links-tab\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"links\" aria-selected=\"false\" href=\"#links\">Links</a>
                    </li>
                </ul>
            </div>
            <div class=\"card-body\">
                <div class=\"tab-content\" id=\"myTabContent\">
                    <div class=\"tab-pane fade show active\" id=\"posts\" role=\"tabpanel\" aria-labelledby=\"posts-tab\">
                        <div class=\"form-group\">
                            <label class=\"sr-only\" for=\"message\">post</label>
                            <textarea name=\"message\" class=\"form-control\" id=\"message\" rows=\"3\" placeholder=\"Tell your network what's on your mind!\"></textarea>
                        </div>
                    </div>
                    <div class=\"tab-pane fade\" id=\"images\" role=\"tabpanel\" aria-labelledby=\"images-tab\">
                        <div class=\"form-group\">
                            <div class=\"custom-file\">
                                <input name=\"image\" type=\"file\" class=\"custom-file-input\" id=\"customFile\">
                                <label class=\"custom-file-label\" for=\"customFile\">Upload image</label>
                            </div>
                        </div>
                        <div class=\"py-4\"></div>
                    </div>
                    <div class=\"tab-pane fade\" id=\"videos\" role=\"tabpanel\" aria-labelledby=\"videos-tab\">
                        <div class=\"form-group\">
                            <input name=\"video_url\" type=\"text\" class=\"form-control\" id=\"video_url\" placeholder=\"YouTube\">
                        </div>
                        <div class=\"py-4\"></div>
                    </div>
                    <div class=\"tab-pane fade\" id=\"links\" role=\"tabpanel\" aria-labelledby=\"links-tab\">
                        <div class=\"form-group\">
                            <input name=\"link_url\" type=\"text\" class=\"form-control\" id=\"link_url\" placeholder=\"URL\">
                        </div>
                        <div class=\"py-4\"></div>
                    </div>
                </div>
                <div class=\"btn-toolbar justify-content-between\">
                    <div class=\"btn-group\">
                        <input type=\"submit\" name=\"doPost\" class=\"btn btn-primary\" value=\"Post\">
                    </div>
                    <div class=\"btn-group\">
                        <button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-link dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\"
                                aria-expanded=\"false\">
                            <i class=\"fa fa-globe\"></i>
                        </button>
                        <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"btnGroupDrop1\">
                            <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-globe\"></i> Public</a>
                            <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-users\"></i> Friends</a>
                            <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-user\"></i> Just me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- Post /////-->


    <hr>
    <!-— ./post form -->

    <!--- \\\\\\\\\\\\\\Post-->
    ";
        // line 93
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 94
            echo "        <div class=\"card gedf-card\">
            <div class=\"card-header\">
                <div class=\"d-flex justify-content-between align-items-center\">
                    <div class=\"d-flex justify-content-between align-items-center\">
                        <div class=\"mr-2\">
                            <img class=\"rounded-circle\" width=\"45\" src=\"https://picsum.photos/50/50\" alt=\"\">
                        </div>
                        <div class=\"ml-2\">
                            <div class=\"h5 m-0\">@USERNAME</div>
                            <div class=\"h7 text-muted\">Posted by ";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postUserId", array()), "html", null, true);
            echo "</div>
                        </div>
                    </div>
                    <div>
                        <div class=\"dropdown\">
                            <button class=\"btn btn-link dropdown-toggle\" type=\"button\" id=\"gedf-drop1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                <i class=\"fa fa-ellipsis-h\"></i>
                            </button>
                            <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"gedf-drop1\">
                                <div class=\"h6 dropdown-header\">Configuration</div>
                                <a class=\"dropdown-item\" href=\"#\">Save</a>
                                <a class=\"dropdown-item\" href=\"#\">Hide</a>
                                <a class=\"dropdown-item\" href=\"#\">Report</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"card-body\">
                <div class=\"text-muted h7 mb-2\"> <i class=\"fa fa-clock-o\"></i>";
            // line 123
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "timeStamp", array()), "html", null, true);
            echo "</div>
                <a class=\"card-link\" href=\"#\">
                    <h5 class=\"card-title\">Title</h5>
                </a>
                <!-— message -->
                <p class=\"card-text\">
                <p>";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "message", array()), "html", null, true);
            echo "</p>
                </p>
                <!-— ./message -->
                ";
            // line 132
            if ($this->getAttribute($context["p"], "postPictureId", array())) {
                // line 133
                echo "                    <!-— image -->
                    <img src=\"/";
                // line 134
                echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
                echo "/posts/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postPictureId", array()), "html", null, true);
                echo "/image\" width=\"200\">
                    <!-— ./image -->
                ";
            }
            // line 136
            echo "  

                ";
            // line 138
            if ($this->getAttribute($context["p"], "link", array())) {
                // line 139
                echo "                    <!-— link -->
                    <a href=\"";
                // line 140
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "link", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "link", array()), "html", null, true);
                echo "</a>
                    <!-— ./link -->
                ";
            }
            // line 142
            echo "   

                ";
            // line 144
            if ($this->getAttribute($context["p"], "postVideoId", array())) {
                echo " 
                    <!-— video -->
                    <iframe width=\"420\" height=\"315\"
                            src=\"https://www.youtube.com/embed/";
                // line 147
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postVideoId", array()), "html", null, true);
                echo "\">
                    </iframe>
                    <!-— ./video -->
                ";
            }
            // line 151
            echo "            </div>
            <div class=\"card-footer\">
                <a href=\"#\" class=\"card-link\"><i class=\"fa fa-gittip\"></i> Like</a>
                <a href=\"#\" class=\"card-link\"><i class=\"fa fa-comment\"></i> Comment</a>
                <a href=\"#\" class=\"card-link\"><i class=\"fa fa-mail-forward\"></i> Share</a>
            </div>
        </div>
        <br>
        <!-- Post /////-->

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "user_posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 151,  225 => 147,  219 => 144,  215 => 142,  207 => 140,  204 => 139,  202 => 138,  198 => 136,  190 => 134,  187 => 133,  185 => 132,  179 => 129,  170 => 123,  147 => 103,  136 => 94,  132 => 93,  52 => 17,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}Posts{% endblock %}

{% block addhead %}   
    <script type=\"text/javascript\">
        \$(function () {
            \$('.extra_field').hide();
            \$(\"input[name='post_type']\").change(function () {
                \$('.extra_field').hide();
                \$('.' + \$(\"input[name='post_type']:checked\").val() + '_input').show();
            });
        });
    </script>
{% endblock %}

{% block addsection %}     
    <!--- \\\\\\\\\\\\\\Post-->
    <form method=\"post\" enctype=\"multipart/form-data\">
        <div class=\"card gedf-card\">
            <div class=\"card-header\">
                <ul class=\"nav nav-tabs card-header-tabs\" id=\"myTab\" role=\"tablist\">
                    <li class=\"nav-item\">
                        <a class=\"nav-link active\" id=\"posts-tab\" data-toggle=\"tab\" href=\"#posts\" role=\"tab\" aria-controls=\"posts\" aria-selected=\"true\">Message</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" id=\"images-tab\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"images\" aria-selected=\"false\" href=\"#images\">Images</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" id=\"videos-tab\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"videos\" aria-selected=\"false\" href=\"#videos\">Videos</a>
                    </li>
                    <li class=\"nav-item\">
                        <a class=\"nav-link\" id=\"links-tab\" data-toggle=\"tab\" role=\"tab\" aria-controls=\"links\" aria-selected=\"false\" href=\"#links\">Links</a>
                    </li>
                </ul>
            </div>
            <div class=\"card-body\">
                <div class=\"tab-content\" id=\"myTabContent\">
                    <div class=\"tab-pane fade show active\" id=\"posts\" role=\"tabpanel\" aria-labelledby=\"posts-tab\">
                        <div class=\"form-group\">
                            <label class=\"sr-only\" for=\"message\">post</label>
                            <textarea name=\"message\" class=\"form-control\" id=\"message\" rows=\"3\" placeholder=\"Tell your network what's on your mind!\"></textarea>
                        </div>
                    </div>
                    <div class=\"tab-pane fade\" id=\"images\" role=\"tabpanel\" aria-labelledby=\"images-tab\">
                        <div class=\"form-group\">
                            <div class=\"custom-file\">
                                <input name=\"image\" type=\"file\" class=\"custom-file-input\" id=\"customFile\">
                                <label class=\"custom-file-label\" for=\"customFile\">Upload image</label>
                            </div>
                        </div>
                        <div class=\"py-4\"></div>
                    </div>
                    <div class=\"tab-pane fade\" id=\"videos\" role=\"tabpanel\" aria-labelledby=\"videos-tab\">
                        <div class=\"form-group\">
                            <input name=\"video_url\" type=\"text\" class=\"form-control\" id=\"video_url\" placeholder=\"YouTube\">
                        </div>
                        <div class=\"py-4\"></div>
                    </div>
                    <div class=\"tab-pane fade\" id=\"links\" role=\"tabpanel\" aria-labelledby=\"links-tab\">
                        <div class=\"form-group\">
                            <input name=\"link_url\" type=\"text\" class=\"form-control\" id=\"link_url\" placeholder=\"URL\">
                        </div>
                        <div class=\"py-4\"></div>
                    </div>
                </div>
                <div class=\"btn-toolbar justify-content-between\">
                    <div class=\"btn-group\">
                        <input type=\"submit\" name=\"doPost\" class=\"btn btn-primary\" value=\"Post\">
                    </div>
                    <div class=\"btn-group\">
                        <button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-link dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\"
                                aria-expanded=\"false\">
                            <i class=\"fa fa-globe\"></i>
                        </button>
                        <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"btnGroupDrop1\">
                            <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-globe\"></i> Public</a>
                            <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-users\"></i> Friends</a>
                            <a class=\"dropdown-item\" href=\"#\"><i class=\"fa fa-user\"></i> Just me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- Post /////-->


    <hr>
    <!-— ./post form -->

    <!--- \\\\\\\\\\\\\\Post-->
    {% for p in posts %}
        <div class=\"card gedf-card\">
            <div class=\"card-header\">
                <div class=\"d-flex justify-content-between align-items-center\">
                    <div class=\"d-flex justify-content-between align-items-center\">
                        <div class=\"mr-2\">
                            <img class=\"rounded-circle\" width=\"45\" src=\"https://picsum.photos/50/50\" alt=\"\">
                        </div>
                        <div class=\"ml-2\">
                            <div class=\"h5 m-0\">@USERNAME</div>
                            <div class=\"h7 text-muted\">Posted by {{p.postUserId}}</div>
                        </div>
                    </div>
                    <div>
                        <div class=\"dropdown\">
                            <button class=\"btn btn-link dropdown-toggle\" type=\"button\" id=\"gedf-drop1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                <i class=\"fa fa-ellipsis-h\"></i>
                            </button>
                            <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"gedf-drop1\">
                                <div class=\"h6 dropdown-header\">Configuration</div>
                                <a class=\"dropdown-item\" href=\"#\">Save</a>
                                <a class=\"dropdown-item\" href=\"#\">Hide</a>
                                <a class=\"dropdown-item\" href=\"#\">Report</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"card-body\">
                <div class=\"text-muted h7 mb-2\"> <i class=\"fa fa-clock-o\"></i>{{p.timeStamp}}</div>
                <a class=\"card-link\" href=\"#\">
                    <h5 class=\"card-title\">Title</h5>
                </a>
                <!-— message -->
                <p class=\"card-text\">
                <p>{{p.message}}</p>
                </p>
                <!-— ./message -->
                {% if p.postPictureId %}
                    <!-— image -->
                    <img src=\"/{{user.username}}/posts/{{p.postPictureId}}/image\" width=\"200\">
                    <!-— ./image -->
                {% endif %}  

                {% if p.link %}
                    <!-— link -->
                    <a href=\"{{p.link}}\">{{p.link}}</a>
                    <!-— ./link -->
                {% endif %}   

                {% if p.postVideoId %} 
                    <!-— video -->
                    <iframe width=\"420\" height=\"315\"
                            src=\"https://www.youtube.com/embed/{{p.postVideoId}}\">
                    </iframe>
                    <!-— ./video -->
                {% endif %}
            </div>
            <div class=\"card-footer\">
                <a href=\"#\" class=\"card-link\"><i class=\"fa fa-gittip\"></i> Like</a>
                <a href=\"#\" class=\"card-link\"><i class=\"fa fa-comment\"></i> Comment</a>
                <a href=\"#\" class=\"card-link\"><i class=\"fa fa-mail-forward\"></i> Share</a>
            </div>
        </div>
        <br>
        <!-- Post /////-->

    {% endfor %}
{% endblock %}
", "user_posts.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_posts.html.twig");
    }
}

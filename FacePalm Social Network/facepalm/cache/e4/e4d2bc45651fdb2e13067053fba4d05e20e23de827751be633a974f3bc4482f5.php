<?php

/* login_success.html.twig */
class __TwigTemplate_4f8aee940b6aae9480923d7d99d0b8aa553b194cb6693d76e24d78693c43a28e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "login_success.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "first_name", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "last_name", array()), "html", null, true);
        echo " - Logged in";
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        echo "  
            <meta http-equiv=\"refresh\" content=\"0; url=";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/news\" />
        ";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "login_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 8,  44 => 5,  39 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}{{user.first_name}} {{user.last_name}} - Logged in{% endblock %}
        {% block addhead %}  
            <meta http-equiv=\"refresh\" content=\"0; url={{user.username}}/news\" />
        {% endblock %}

{% block content %}
{#    <h1>You've been looged !!. You'll be redirected in 0 seconds</h1>#}
{% endblock %}", "login_success.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\login_success.html.twig");
    }
}

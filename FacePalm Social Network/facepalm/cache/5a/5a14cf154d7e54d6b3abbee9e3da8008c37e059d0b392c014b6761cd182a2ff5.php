<?php

/* news.html.twig */
class __TwigTemplate_fff17ca731e88bfea78fb8b432b4e75524620216d2fb42c4403a32fde5d202de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "news.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "News";
    }

    // line 5
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script type=\"text/javascript\">
        \$(function () {
            \$('.extra_field').hide();
            \$(\"input[name='post_type']\").change(function () {
                \$('.extra_field').hide();
                \$('.' + \$(\"input[name='post_type']:checked\").val() + '_input').show();
            });
        });
    </script>
";
    }

    // line 17
    public function block_content($context, array $blocks = array())
    {
        // line 18
        echo "    <!-- main -->
    <main class=\"container\">
        <div class=\"row\">
            <!-- friends -->
            <div class=\"col-md-3\">
            </div>
            <!-- ./friends -->
            <div class=\"col-md-6\">
                <!-- news feed -->
                <!-— post form -->
                <p><b>WELCOME TO YOUR  NEWS FEED</b></p>
                <form method=\"post\" enctype=\"multipart/form-data\">
                    <textarea id=\"status\" name=\"message\"></textarea>
                    <br />
                    <input type=\"radio\" name=\"post_type\" value=\"update\" checked/>Update
                    <input type=\"radio\" name=\"post_type\" value=\"video\" />Video
                    <input type=\"radio\" name=\"post_type\" value=\"image\" />Image
                    <input type=\"radio\" name=\"post_type\" value=\"link\" />Link
                    <br />
                    <div class=\"video_input extra_field\">
                        <label for=\"video_url\" class=\"\">YouTube URL</label>
                        <input type=\"text\" id=\"\" name=\"video_url\" class=\"\" /><br />
                    </div>
                    <div class=\"image_input extra_field\">
                        <label for=\"image_file\" class=\"\">Upload image</label>
                        <input type=\"file\" id=\"\" name=\"image\" class=\"\" /><br />
                    </div>
                    <div class=\"link_input extra_field\">
                        <label for=\"link_url\" class=\"\">Link</label>
                        <input type=\"text\" id=\"\" name=\"link_url\" class=\"\" /><br />
                        <label for=\"link_description\" class=\"\">Description</label>
                        <input type=\"text\" id=\"\" name=\"link_description\" class=\"\" /><br />
                    </div>
                    <input type=\"submit\" name=\"doPost\" value=\"Post\" />
                </form>

                <hr>
                <!-— ./post form -->

                <div>
                    ";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 59
            echo "                        <br>
                        <div class=\"panel panel-default\">
                            <div class=\"panel-body\">
                                <!-— message -->
                                <p><b>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "message", array()), "html", null, true);
            echo "</b></p>
                                <!-— ./message -->

                                ";
            // line 66
            if ($this->getAttribute($context["p"], "postPictureId", array())) {
                // line 67
                echo "                                    <!-— image -->
                                    <img src=\"/";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
                echo "/posts/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postPictureId", array()), "html", null, true);
                echo "/image\" width=\"200\">
                                    <!-— ./image -->
                                ";
            }
            // line 70
            echo "  

                                ";
            // line 72
            if ($this->getAttribute($context["p"], "link", array())) {
                // line 73
                echo "                                    <!-— link -->
                                    <a href=\"";
                // line 74
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "link", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "link", array()), "html", null, true);
                echo "</a>
                                    <!-— ./link -->
                                ";
            }
            // line 76
            echo "   

                                ";
            // line 78
            if ($this->getAttribute($context["p"], "postVideoId", array())) {
                echo " 
                                    <!-— video -->
                                    <iframe width=\"420\" height=\"315\"
                                            src=\"https://www.youtube.com/embed/";
                // line 81
                echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postVideoId", array()), "html", null, true);
                echo "\">
                                    </iframe>
                                    <!-— ./video -->
                                ";
            }
            // line 85
            echo "                            </div>
                            <div class=\"panel-footer\">
                                </p>Posted by ";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "postUserId", array()), "html", null, true);
            echo " on ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "timeStamp", array()), "html", null, true);
            echo "</p>       
                                ";
            // line 88
            if (($context["errorList"] ?? null)) {
                echo "    
                                    <ul class=\"error\">
                                        ";
                // line 90
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 91
                    echo "                                            <li>";
                    echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                    echo "</li>
                                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 93
                echo "                                    </ul>
                                ";
            }
            // line 95
            echo "                                <form method=\"post\">
                                    <textarea name=\"comment\"></textarea>
                                    <input type=\"hidden\" name=\"postId\" value=";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo " />
                                    <input type=\"submit\" name=\"doComment\" value=\"Comment\" />
                                </form>
                                ";
            // line 100
            if (($context["commentList"] ?? null)) {
                echo "   
                                    <ul>
                                        ";
                // line 102
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["commentList"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
                    // line 103
                    echo "                                            ";
                    if (($this->getAttribute($context["c"], "commentPostId", array()) == $this->getAttribute($context["p"], "id", array()))) {
                        // line 104
                        echo "                                                <li>";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "message", array()), "html", null, true);
                        echo "</li>
                                                </p>Posted by ";
                        // line 105
                        echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "commentUserId", array()), "html", null, true);
                        echo " on ";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "timeStamp", array()), "html", null, true);
                        echo "</p>  
                                            ";
                    }
                    // line 107
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 108
                echo "                                    </ul>
                                ";
            }
            // line 110
            echo "                                <hr>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "                </div>
                <div class=\"col-md-3\">
                    <!-- notifications -->
                </div>
                <!-- ./notifications -->
            </div>
    </main>
    <!-- ./main -->
";
    }

    public function getTemplateName()
    {
        return "news.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 114,  236 => 110,  232 => 108,  226 => 107,  219 => 105,  214 => 104,  211 => 103,  207 => 102,  202 => 100,  196 => 97,  192 => 95,  188 => 93,  179 => 91,  175 => 90,  170 => 88,  164 => 87,  160 => 85,  153 => 81,  147 => 78,  143 => 76,  135 => 74,  132 => 73,  130 => 72,  126 => 70,  118 => 68,  115 => 67,  113 => 66,  107 => 63,  101 => 59,  97 => 58,  55 => 18,  52 => 17,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}News{% endblock %}

{% block addhead %}   
    <script type=\"text/javascript\">
        \$(function () {
            \$('.extra_field').hide();
            \$(\"input[name='post_type']\").change(function () {
                \$('.extra_field').hide();
                \$('.' + \$(\"input[name='post_type']:checked\").val() + '_input').show();
            });
        });
    </script>
{% endblock %}

{% block content %}
    <!-- main -->
    <main class=\"container\">
        <div class=\"row\">
            <!-- friends -->
            <div class=\"col-md-3\">
            </div>
            <!-- ./friends -->
            <div class=\"col-md-6\">
                <!-- news feed -->
                <!-— post form -->
                <p><b>WELCOME TO YOUR  NEWS FEED</b></p>
                <form method=\"post\" enctype=\"multipart/form-data\">
                    <textarea id=\"status\" name=\"message\"></textarea>
                    <br />
                    <input type=\"radio\" name=\"post_type\" value=\"update\" checked/>Update
                    <input type=\"radio\" name=\"post_type\" value=\"video\" />Video
                    <input type=\"radio\" name=\"post_type\" value=\"image\" />Image
                    <input type=\"radio\" name=\"post_type\" value=\"link\" />Link
                    <br />
                    <div class=\"video_input extra_field\">
                        <label for=\"video_url\" class=\"\">YouTube URL</label>
                        <input type=\"text\" id=\"\" name=\"video_url\" class=\"\" /><br />
                    </div>
                    <div class=\"image_input extra_field\">
                        <label for=\"image_file\" class=\"\">Upload image</label>
                        <input type=\"file\" id=\"\" name=\"image\" class=\"\" /><br />
                    </div>
                    <div class=\"link_input extra_field\">
                        <label for=\"link_url\" class=\"\">Link</label>
                        <input type=\"text\" id=\"\" name=\"link_url\" class=\"\" /><br />
                        <label for=\"link_description\" class=\"\">Description</label>
                        <input type=\"text\" id=\"\" name=\"link_description\" class=\"\" /><br />
                    </div>
                    <input type=\"submit\" name=\"doPost\" value=\"Post\" />
                </form>

                <hr>
                <!-— ./post form -->

                <div>
                    {% for p in posts %}
                        <br>
                        <div class=\"panel panel-default\">
                            <div class=\"panel-body\">
                                <!-— message -->
                                <p><b>{{p.message}}</b></p>
                                <!-— ./message -->

                                {% if p.postPictureId %}
                                    <!-— image -->
                                    <img src=\"/{{user.username}}/posts/{{p.postPictureId}}/image\" width=\"200\">
                                    <!-— ./image -->
                                {% endif %}  

                                {% if p.link %}
                                    <!-— link -->
                                    <a href=\"{{p.link}}\">{{p.link}}</a>
                                    <!-— ./link -->
                                {% endif %}   

                                {% if p.postVideoId %} 
                                    <!-— video -->
                                    <iframe width=\"420\" height=\"315\"
                                            src=\"https://www.youtube.com/embed/{{p.postVideoId}}\">
                                    </iframe>
                                    <!-— ./video -->
                                {% endif %}
                            </div>
                            <div class=\"panel-footer\">
                                </p>Posted by {{p.postUserId}} on {{p.timeStamp}}</p>       
                                {% if errorList %}    
                                    <ul class=\"error\">
                                        {% for error in errorList %}
                                            <li>{{error}}</li>
                                            {% endfor %}
                                    </ul>
                                {% endif %}
                                <form method=\"post\">
                                    <textarea name=\"comment\"></textarea>
                                    <input type=\"hidden\" name=\"postId\" value={{p.id}} />
                                    <input type=\"submit\" name=\"doComment\" value=\"Comment\" />
                                </form>
                                {% if commentList %}   
                                    <ul>
                                        {% for c in commentList %}
                                            {% if c.commentPostId == p.id %}
                                                <li>{{c.message}}</li>
                                                </p>Posted by {{c.commentUserId}} on {{c.timeStamp}}</p>  
                                            {% endif %}
                                        {% endfor %}
                                    </ul>
                                {% endif %}
                                <hr>
                            </div>
                        </div>
                    {% endfor %}
                </div>
                <div class=\"col-md-3\">
                    <!-- notifications -->
                </div>
                <!-- ./notifications -->
            </div>
    </main>
    <!-- ./main -->
{% endblock %}", "news.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\news.html.twig");
    }
}

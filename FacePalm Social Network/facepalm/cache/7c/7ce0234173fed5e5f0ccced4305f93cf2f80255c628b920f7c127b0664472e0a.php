<?php

/* friends.html.twig */
class __TwigTemplate_6ad8fe2f8fb41c34d0da32b43b9d9ac60938ed6961f7e5ab1917fff3d26fedfb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "friends.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "    <h1>Friend list for ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo " here</h1>
";
        // line 14
        echo "<div class=\"container\">
    ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["friendList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["friend"]) {
            // line 16
            echo "        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 17
                echo "            <div class=\"row\">
        ";
            }
            // line 19
            echo "        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                ";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo " 
                <a href='/";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "friendname", array()), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "id", array()), "html", null, true);
            echo "'><b>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "lastName", array()), "html", null, true);
            echo "</b></a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        ";
            // line 31
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 32
                echo "            </div>
        ";
            }
            // line 33
            echo "    
        ";
            // line 34
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 35
                echo "            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 36
                    echo "                </div>
            ";
                }
                // line 37
                echo "  
        ";
            }
            // line 39
            echo "            <br>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friend'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "    

</div>
";
    }

    public function getTemplateName()
    {
        return "friends.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 40,  122 => 39,  118 => 37,  114 => 36,  111 => 35,  109 => 34,  106 => 33,  102 => 32,  100 => 31,  85 => 25,  81 => 24,  74 => 19,  70 => 17,  67 => 16,  50 => 15,  47 => 14,  40 => 6,  37 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}

{% block content %}
    <h1>Friend list for {{user.firstName}} {{user.lastName}} here</h1>
{#        <p>username: {{user.username}} <br></p>
        <p>email: {{user.email}} <br></p>
        <p>password: {{user.password}} <br></p>
        <p>first_name: {{user.first_name}} <br></p>
        <p>last_name: {{user.last_name}} <br></p>
        <p><a href='friends'>Go to friends</a></p>
        <p><a href=\"/\">Go to index page </a></p>#}
<div class=\"container\">
    {% for friend in friendList %}
        {% if loop.index is odd %}
            <div class=\"row\">
        {% endif %}
        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                {{ loop.index }} 
                <a href='/{{friend.friendname}}/{{friend.id}}'><b>{{friend.firstName}} {{friend.lastName}}</b></a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        {% if loop.index is even %}
            </div>
        {% endif %}    
        {% if loop.last %}
            {% if loop.index is odd %}
                </div>
            {% endif %}  
        {% endif %}
            <br>
    {% endfor %}    

</div>
{% endblock %}", "friends.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\friends.html.twig");
    }
}

<?php

/* fatal_error.html.twig */
class __TwigTemplate_1dc0a480b3301fb712ab2f2e52e0c1d5c8760ecbc8e67b66c216e98e9d3fa5ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "fatal_error.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    Fatal Error
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    <p>We're sorry, an internal fatal error ocurred.
        Our team of coding Ninjas has been notified of the issue.
    <a href=\"/\">click to continue</a>.</p>
    <img src='/images/ninja.png' height=\"300\"/><br><br>
";
    }

    public function getTemplateName()
    {
        return "fatal_error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 8,  37 => 7,  32 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}
    Fatal Error
{% endblock %}

{% block content %}
    <p>We're sorry, an internal fatal error ocurred.
        Our team of coding Ninjas has been notified of the issue.
    <a href=\"/\">click to continue</a>.</p>
    <img src='/images/ninja.png' height=\"300\"/><br><br>
{% endblock %}
", "fatal_error.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\fatal_error.html.twig");
    }
}

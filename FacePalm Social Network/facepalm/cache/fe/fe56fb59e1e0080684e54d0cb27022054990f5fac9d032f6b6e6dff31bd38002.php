<?php

/* user_search.html.twig */
class __TwigTemplate_63ce27e6cad641de232a913eeab01b6411a5b63bddc50773031a898b5e730afa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_search.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 5
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script>
        function unfriend(userid) {
                if (confirm(\"Are you sure ?\")){
                        \$.ajax({
                            url: \"/";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/unfriend/\" + userid,
                            // data: {},
                            type: \"DELETE\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Deleted successfully\");
                        });                    
                }
                location.reload();
            }
        
        function acceptRequest(userid) {
                        var friendship = {userRequestId: userid + \"\", userAnswerId: ";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "id", array()), "html", null, true);
        echo " + \"\", friendshipAccepted: 1 + \"\"};  
                        \$.ajax({
                            url: \"/";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/acceptrequest/\" + userid,
                            data: JSON.stringify(friendship),
                            type: \"PUT\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Request accepted successfully, you are friends!\");
                        });                    
                location.reload();
            }
            
        function addFriend(userid) {
                        var friendship = {userRequestId: ";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "id", array()), "html", null, true);
        echo " + \"\", userAnswerId: userid + \"\", friendshipAccepted: 0 + \"\"};
                        \$.ajax({
                            url: \"/";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/addfriend/\" + userid,
                            data: JSON.stringify(friendship),
                            type: \"POST\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Request Sent successfully\");
                        });                    
                location.reload();
            }            
    </script>
";
    }

    // line 49
    public function block_addsection($context, array $blocks = array())
    {
        // line 51
        echo "<div class=\"container\">
    ";
        // line 52
        if (($context["searchedAndFriends"] ?? null)) {
            echo "   
    ";
        } else {
            // line 54
            echo "        ";
            if (($context["searchedAndReceivedRequest"] ?? null)) {
                // line 55
                echo "        ";
            } else {
                // line 56
                echo "            ";
                if (($context["searchedAndSentRequest"] ?? null)) {
                    // line 57
                    echo "            ";
                } else {
                    // line 58
                    echo "                ";
                    if (($context["searchedNotFriendsNotSentNotReceivedRequest"] ?? null)) {
                        // line 59
                        echo "                ";
                    } else {
                        // line 60
                        echo "                    <h1>Hello ";
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
                        echo " ";
                        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
                        echo ",<br> Sorry, but there are no accounts matching your search criteria  :(</h1>
                ";
                    }
                    // line 61
                    echo "                
            ";
                }
                // line 62
                echo "             
        ";
            }
            // line 63
            echo "          
    ";
        }
        // line 64
        echo "    
    
    <div class=\"card-columns\">
    ";
        // line 67
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["searchedAndFriends"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["userSearched"]) {
            // line 68
            echo "            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/";
            // line 70
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "profilePictureId", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
            echo "\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "profilePictureId", array()), "html", null, true);
            echo "'> 
                        ";
            // line 74
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
            echo "
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-success dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton";
            // line 78
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Friends
                      </button>
                      <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton";
            // line 81
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "\">
                        <span class=\"dropdown-item\" onclick=\"unfriend(";
            // line 82
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo ")\" >Unfriend</span>
                      </div>
                    </div>
              </div>
            </div>    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userSearched'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "    
    ";
        // line 88
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["searchedAndReceivedRequest"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["userSearched"]) {
            // line 89
            echo "            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "profilePictureId", array()), "html", null, true);
            echo "\" 
                   alt=\"";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
            echo "\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/";
            // line 95
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "profilePictureId", array()), "html", null, true);
            echo "'> 
                        ";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
            echo "
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-warning dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Answer Request
                      </button>
                      <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "\">
                        <span class=\"dropdown-item\" onclick=\"acceptRequest(";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo ")\" >Accept Request</span>
                        <span class=\"dropdown-item\" onclick=\"unfriend(";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo ")\" >Delete Request</span>
                      </div>
                    </div>
              </div>
            </div>    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userSearched'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "  
    ";
        // line 111
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["searchedAndSentRequest"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["userSearched"]) {
            // line 112
            echo "            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/";
            // line 114
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "profilePictureId", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
            echo "\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "profilePictureId", array()), "html", null, true);
            echo "'> 
                        ";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
            echo "
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-info \" onclick=\"unfriend(";
            // line 122
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo ")\" type=\"button\" id=\"dropdownMenuButton";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "\"  aria-haspopup=\"true\" aria-expanded=\"false\">
                        Cancel Request
                      </button>
                    </div>
              </div>
            </div>    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userSearched'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 129
        echo "    
    ";
        // line 130
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["searchedNotFriendsNotSentNotReceivedRequest"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["userSearched"]) {
            // line 131
            echo "            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/";
            // line 133
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "profilePictureId", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
            echo "\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/";
            // line 136
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "profilePictureId", array()), "html", null, true);
            echo "'> 
                        ";
            // line 137
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "lastName", array()), "html", null, true);
            echo "
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-danger \" onclick=\"addFriend(";
            // line 141
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo ")\" type=\"button\" id=\"dropdownMenuButton";
            echo twig_escape_filter($this->env, $this->getAttribute($context["userSearched"], "id", array()), "html", null, true);
            echo "\"  aria-haspopup=\"true\" aria-expanded=\"false\">
                        Add Friend
                      </button>
                    </div>
              </div>
            </div>    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userSearched'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 148
        echo "    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "user_search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  400 => 148,  385 => 141,  376 => 137,  368 => 136,  354 => 133,  350 => 131,  346 => 130,  343 => 129,  328 => 122,  319 => 118,  311 => 117,  297 => 114,  293 => 112,  289 => 111,  286 => 110,  274 => 105,  270 => 104,  266 => 103,  260 => 100,  251 => 96,  243 => 95,  235 => 92,  227 => 91,  223 => 89,  219 => 88,  216 => 87,  204 => 82,  200 => 81,  194 => 78,  185 => 74,  177 => 73,  163 => 70,  159 => 68,  155 => 67,  150 => 64,  146 => 63,  142 => 62,  138 => 61,  130 => 60,  127 => 59,  124 => 58,  121 => 57,  118 => 56,  115 => 55,  112 => 54,  107 => 52,  104 => 51,  101 => 49,  86 => 37,  81 => 35,  67 => 24,  62 => 22,  47 => 10,  38 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}

{% block addhead %}   
    <script>
        function unfriend(userid) {
                if (confirm(\"Are you sure ?\")){
                        \$.ajax({
                            url: \"/{{user.username}}/unfriend/\" + userid,
                            // data: {},
                            type: \"DELETE\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Deleted successfully\");
                        });                    
                }
                location.reload();
            }
        
        function acceptRequest(userid) {
                        var friendship = {userRequestId: userid + \"\", userAnswerId: {{user.id}} + \"\", friendshipAccepted: 1 + \"\"};  
                        \$.ajax({
                            url: \"/{{user.username}}/acceptrequest/\" + userid,
                            data: JSON.stringify(friendship),
                            type: \"PUT\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Request accepted successfully, you are friends!\");
                        });                    
                location.reload();
            }
            
        function addFriend(userid) {
                        var friendship = {userRequestId: {{user.id}} + \"\", userAnswerId: userid + \"\", friendshipAccepted: 0 + \"\"};
                        \$.ajax({
                            url: \"/{{user.username}}/addfriend/\" + userid,
                            data: JSON.stringify(friendship),
                            type: \"POST\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Request Sent successfully\");
                        });                    
                location.reload();
            }            
    </script>
{% endblock %}

{% block addsection %}
{#    <h1>Search list for {{user.firstName}} {{user.lastName}} here</h1>#}
<div class=\"container\">
    {% if searchedAndFriends %}   
    {% else %}
        {% if searchedAndReceivedRequest %}
        {% else %}
            {% if searchedAndSentRequest %}
            {% else %}
                {% if searchedNotFriendsNotSentNotReceivedRequest %}
                {% else %}
                    <h1>Hello {{user.firstName}} {{user.lastName}},<br> Sorry, but there are no accounts matching your search criteria  :(</h1>
                {% endif %}                
            {% endif %}             
        {% endif %}          
    {% endif %}    
    
    <div class=\"card-columns\">
    {% for userSearched in searchedAndFriends %}
            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/{{user.username}}/search/{{userSearched.id}}/image/{{userSearched.profilePictureId}}\" alt=\"{{userSearched.firstName}} {{userSearched.lastName}}\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/{{user.username}}/search/{{userSearched.id}}/image/{{userSearched.profilePictureId}}'> 
                        {{userSearched.firstName}} {{userSearched.lastName}}
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-success dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton{{userSearched.id}}\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Friends
                      </button>
                      <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton{{userSearched.id}}\">
                        <span class=\"dropdown-item\" onclick=\"unfriend({{userSearched.id}})\" >Unfriend</span>
                      </div>
                    </div>
              </div>
            </div>    
    {% endfor %}    
    {% for userSearched in searchedAndReceivedRequest %}
            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/{{user.username}}/search/{{userSearched.id}}/image/{{userSearched.profilePictureId}}\" 
                   alt=\"{{userSearched.firstName}} {{userSearched.lastName}}\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/{{user.username}}/search/{{userSearched.id}}/image/{{userSearched.profilePictureId}}'> 
                        {{userSearched.firstName}} {{userSearched.lastName}}
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-warning dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton{{userSearched.id}}\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Answer Request
                      </button>
                      <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton{{userSearched.id}}\">
                        <span class=\"dropdown-item\" onclick=\"acceptRequest({{userSearched.id}})\" >Accept Request</span>
                        <span class=\"dropdown-item\" onclick=\"unfriend({{userSearched.id}})\" >Delete Request</span>
                      </div>
                    </div>
              </div>
            </div>    
    {% endfor %}  
    {% for userSearched in searchedAndSentRequest %}
            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/{{user.username}}/search/{{userSearched.id}}/image/{{userSearched.profilePictureId}}\" alt=\"{{userSearched.firstName}} {{userSearched.lastName}}\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/{{user.username}}/search/{{userSearched.id}}/image/{{userSearched.profilePictureId}}'> 
                        {{userSearched.firstName}} {{userSearched.lastName}}
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-info \" onclick=\"unfriend({{userSearched.id}})\" type=\"button\" id=\"dropdownMenuButton{{userSearched.id}}\"  aria-haspopup=\"true\" aria-expanded=\"false\">
                        Cancel Request
                      </button>
                    </div>
              </div>
            </div>    
    {% endfor %}
    
    {% for userSearched in searchedNotFriendsNotSentNotReceivedRequest %}
            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/{{user.username}}/search/{{userSearched.id}}/image/{{userSearched.profilePictureId}}\" alt=\"{{userSearched.firstName}} {{userSearched.lastName}}\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/{{user.username}}/search/{{userSearched.id}}/image/{{userSearched.profilePictureId}}'> 
                        {{userSearched.firstName}} {{userSearched.lastName}}
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-danger \" onclick=\"addFriend({{userSearched.id}})\" type=\"button\" id=\"dropdownMenuButton{{userSearched.id}}\"  aria-haspopup=\"true\" aria-expanded=\"false\">
                        Add Friend
                      </button>
                    </div>
              </div>
            </div>    
    {% endfor %}
    </div>
</div>
{% endblock %}", "user_search.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_search.html.twig");
    }
}

<?php

/* user_about.html.twig */
class __TwigTemplate_4becd970a5a3cfedf1f43e1155ff442c858f12ab7d330e973a8e5348d3121c59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_about.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 5
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script type=\"text/javascript\">
        \$(function () {
            \$('#submit').hide();
            \$(\"#edit\").click(function () {
                \$(\"#form\").prop('disabled', false);
                \$('#edit').hide();
                \$('#submit').show();
            });
        });
    </script>
";
    }

    // line 18
    public function block_addsection($context, array $blocks = array())
    {
        echo "     
    ";
        // line 28
        echo "    ";
        if (($context["errorList"] ?? null)) {
            echo "    
        <ul class=\"error\">
            ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 31
                echo "                <li>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "        </ul>
    ";
        }
        // line 35
        echo "    <form method=\"post\">
        <fieldset id=\"form\" disabled>
            <div class=\"form-row\">
                <div class=\"form-group col-md-6\">
                    <label for=\"fName\">First Name</label>
                    <input type=\"text\" class=\"form-control\" id=\"fName\" name=\"fName\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo "\">
                </div>
                <div class=\"form-group col-md-6\">
                    <label for=\"lname\">Last Name</label>
                    <input type=\"text\" class=\"form-control\" id=\"lName\" name=\"lName\" value=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo "\">
                </div>
            </div>

            <div class=\"form-row\">
                <div class=\"form-group col-md-6\">
                    <label for=\"email\">Email</label>
                    <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "email", array()), "html", null, true);
        echo "\">
                </div>
                <div class=\"form-group col-md-6\">
                    <label for=\"Phone\">Phone</label>
                    <input type=\"text\" class=\"form-control\" id=\"phone\" name=\"phone\" value=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "phone", array()), "html", null, true);
        echo "\">
                </div>
            </div>

            <div class=\"form-row\">
                <div class=\"form-group col-md-6\">
                    <label for=\"language\">Language</label>
                    <select id=\"language\" class=\"form-control\" name=\"language\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "language", array()), "html", null, true);
        echo "\">
                        <option selected>English</option>
                        <option>Spanish</option>
                    </select>
                </div>
                <div class=\"form-group col-md-6\">
                    <label for=\"bday\">Birth Date</label>
                    <input type=\"date\" class=\"form-control\" id=\"bday\" name=\"bday\" value=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "bday", array()), "html", null, true);
        echo "\">
                </div>
            </div>

            <div class=\"form-row\">
                <div class=\"form-group col-md-6\">
                    <label for=\"gender\">Gender</label>
                    <select id=\"gender\" class=\"form-control\" name=\"gender\" value=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "gender", array()), "html", null, true);
        echo "\">
                        <option selected>Male</option>
                        <option>Female</option>
                    </select>
                </div>
                <div class=\"form-group col-md-6\">
                    <label for=\"location\">Location</label>
                    <select id=\"location\" class=\"form-control\" name=\"location\" value=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "location", array()), "html", null, true);
        echo "\">
                        <option selected>Canada</option>
                        <option>USA</option>
                    </select>
                </div>
            </div>
        </fieldset>
        <button type=\"submit\" id=\"submit\" class=\"btn btn-primary\">Update</button>
    </form>
    <button id=\"edit\" class=\"btn btn-primary\">Edit</button>
";
    }

    public function getTemplateName()
    {
        return "user_about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 83,  144 => 76,  134 => 69,  124 => 62,  114 => 55,  107 => 51,  97 => 44,  90 => 40,  83 => 35,  79 => 33,  70 => 31,  66 => 30,  60 => 28,  55 => 18,  38 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}

{% block addhead %}   
    <script type=\"text/javascript\">
        \$(function () {
            \$('#submit').hide();
            \$(\"#edit\").click(function () {
                \$(\"#form\").prop('disabled', false);
                \$('#edit').hide();
                \$('#submit').show();
            });
        });
    </script>
{% endblock %}

{% block addsection %}     
    {#    <p>User Name: {{user.username}} <br></p>
        <p>Email: {{user.email}} <br></p>
        <p>Phone: {{user.phone}} <br></p>
        <p>First Name: {{user.firstName}} <br></p>
        <p>Last Name: {{user.lastName}} <br></p>
        <p>Birth Date: {{user.birthday}} <br></p>
        <p>Language: {{user.language}} <br></p>
        <p>Location: {{user.location}} <br></p>
        <p>Gender: {{user.gender}} <br></p>#}
    {% if errorList %}    
        <ul class=\"error\">
            {% for error in errorList %}
                <li>{{error}}</li>
                {% endfor %}
        </ul>
    {% endif %}
    <form method=\"post\">
        <fieldset id=\"form\" disabled>
            <div class=\"form-row\">
                <div class=\"form-group col-md-6\">
                    <label for=\"fName\">First Name</label>
                    <input type=\"text\" class=\"form-control\" id=\"fName\" name=\"fName\" value=\"{{user.firstName}}\">
                </div>
                <div class=\"form-group col-md-6\">
                    <label for=\"lname\">Last Name</label>
                    <input type=\"text\" class=\"form-control\" id=\"lName\" name=\"lName\" value=\"{{user.lastName}}\">
                </div>
            </div>

            <div class=\"form-row\">
                <div class=\"form-group col-md-6\">
                    <label for=\"email\">Email</label>
                    <input type=\"email\" class=\"form-control\" id=\"email\" name=\"email\" value=\"{{user.email}}\">
                </div>
                <div class=\"form-group col-md-6\">
                    <label for=\"Phone\">Phone</label>
                    <input type=\"text\" class=\"form-control\" id=\"phone\" name=\"phone\" value=\"{{user.phone}}\">
                </div>
            </div>

            <div class=\"form-row\">
                <div class=\"form-group col-md-6\">
                    <label for=\"language\">Language</label>
                    <select id=\"language\" class=\"form-control\" name=\"language\" value=\"{{user.language}}\">
                        <option selected>English</option>
                        <option>Spanish</option>
                    </select>
                </div>
                <div class=\"form-group col-md-6\">
                    <label for=\"bday\">Birth Date</label>
                    <input type=\"date\" class=\"form-control\" id=\"bday\" name=\"bday\" value=\"{{user.bday}}\">
                </div>
            </div>

            <div class=\"form-row\">
                <div class=\"form-group col-md-6\">
                    <label for=\"gender\">Gender</label>
                    <select id=\"gender\" class=\"form-control\" name=\"gender\" value=\"{{user.gender}}\">
                        <option selected>Male</option>
                        <option>Female</option>
                    </select>
                </div>
                <div class=\"form-group col-md-6\">
                    <label for=\"location\">Location</label>
                    <select id=\"location\" class=\"form-control\" name=\"location\" value=\"{{user.location}}\">
                        <option selected>Canada</option>
                        <option>USA</option>
                    </select>
                </div>
            </div>
        </fieldset>
        <button type=\"submit\" id=\"submit\" class=\"btn btn-primary\">Update</button>
    </form>
    <button id=\"edit\" class=\"btn btn-primary\">Edit</button>
{% endblock %}", "user_about.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_about.html.twig");
    }
}

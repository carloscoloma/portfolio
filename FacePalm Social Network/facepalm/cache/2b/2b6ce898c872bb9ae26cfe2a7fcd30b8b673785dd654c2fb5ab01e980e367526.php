<?php

/* user_pictures.html.twig */
class __TwigTemplate_8ea769780f8c1d886dd2c07991574c6ac2f19357c4f20e92d64a831e7b63817d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_pictures.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script>
        \$(document).ready(function(){
           \$('[rel=\"tooltip\"]').tooltip(
                    {container:'body', trigger: 'hover'}
                );
";
        // line 11
        echo "           \$(\"#buttonCreateAlbum\").click(function() {
                    \$(\"#inputLoadImage\").click();
                });
           \$(\"#inputLoadImage\").change (function(){ 
                    previewFile();
                });
        }); 
        function loadFile() {
";
        // line 20
        echo "            
        }        
    </script>
";
    }

    // line 24
    public function block_addsection($context, array $blocks = array())
    {
        // line 25
        echo "    <h1>Album list for ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
        echo " here</h1>

<!-- Button trigger modal -->

<button id=\"buttonCreateAlbum\" type=\"button\" class=\"btn btn-primary\" rel=\"tooltip\"
        data-toggle=\"modal\" data-placement=\"right\" title=\"Choose a file to upload\" data-target=\"#uploadPhotosInAlbumModal\">
      + Create new album
</button>
<br>

<!-- Modal -->
<div class=\"modal fade\" id=\"uploadPhotosInAlbumModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"uploadPhotosInAlbumModal\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered modal-xl\" role=\"document\">
        <div class=\"modal-content\">
        <form method=\"post\" enctype=\"multipart/form-data\">   
            <div class=\"modal-header\">
";
        // line 42
        echo "                <div class=\"input-group input-group-lg\">
                    <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\">Create&nbsp;Album</span>
                    </div>
                  <input name=\"albumName\" placeholder=\"Album name\" type=\"text\" class=\"form-control\" aria-label=\"Sizing example input\" aria-describedby=\"inputGroup-sizing-lg\">
                </div>                
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
                     

                
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-3\" style=\"background-color:lavenderblush\">
                            <textarea name=\"description\" rows=\"6\" placeholder=\"Description...\" style=\"width: 100%\"></textarea>
                <br>                    
";
        // line 66
        echo "                    
                    <button id=\"buttonAddPicture\" type=\"button\" class=\"btn btn-primary\">
                        Select pictures</button>
";
        // line 71
        echo "                    </div>
                    <div class=\"col-9\">
                    <div id=\"previewImage\">
";
        // line 80
        echo " 
                    </div>
                    <br>
                    </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <input type=\"submit\" value=\"Create Album\" class=\"btn btn-primary\" name=\"loadInfo\"><br>
                    ";
        // line 89
        echo " 
                </div>
            </form> 
            <form method=\"post\" enctype=\"multipart/form-data\">  
                <input type=\"file\" id=\"inputLoadImage\" name=\"images[]\" 
                            accept=\"image/gif, image/jpeg, image/png\" multiple/>
                <input type=\"submit\" value=\"Load Images\" class=\"btn btn-primary\" name=\"loadImages\">
            </form>     
        </div>
    </div>
</div>
<div class=\"container\"> 
    <br>
    ";
        // line 102
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["albumList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["album"]) {
            // line 103
            echo "        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 104
                echo "            <div class=\"row\">
        ";
            }
            // line 106
            echo "        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                    <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                ";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo " 
                <a href='pictures/album/";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "id", array()), "html", null, true);
            echo "'><b>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "albumName", array()), "html", null, true);
            echo "</b></a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        ";
            // line 118
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 119
                echo "            </div>
        ";
            }
            // line 120
            echo "    
        ";
            // line 121
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 122
                echo "            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 123
                    echo "                </div>
            ";
                }
                // line 124
                echo "  
        ";
            }
            // line 126
            echo "            <br>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 127
        echo "    

</div>
";
    }

    public function getTemplateName()
    {
        return "user_pictures.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 127,  214 => 126,  210 => 124,  206 => 123,  203 => 122,  201 => 121,  198 => 120,  194 => 119,  192 => 118,  181 => 112,  177 => 111,  170 => 106,  166 => 104,  163 => 103,  146 => 102,  131 => 89,  120 => 80,  115 => 71,  110 => 66,  90 => 42,  68 => 25,  65 => 24,  58 => 20,  48 => 11,  38 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}
{% block addhead %}   
    <script>
        \$(document).ready(function(){
           \$('[rel=\"tooltip\"]').tooltip(
                    {container:'body', trigger: 'hover'}
                );
{#           \$('[rel=\"tooltip\"]').tooltip();     #}
           \$(\"#buttonCreateAlbum\").click(function() {
                    \$(\"#inputLoadImage\").click();
                });
           \$(\"#inputLoadImage\").change (function(){ 
                    previewFile();
                });
        }); 
        function loadFile() {
{#            \$('#previewImage').empty();#}
            
        }        
    </script>
{% endblock %}
{% block addsection %}
    <h1>Album list for {{user.firstName}} {{user.lastName}} here</h1>

<!-- Button trigger modal -->

<button id=\"buttonCreateAlbum\" type=\"button\" class=\"btn btn-primary\" rel=\"tooltip\"
        data-toggle=\"modal\" data-placement=\"right\" title=\"Choose a file to upload\" data-target=\"#uploadPhotosInAlbumModal\">
      + Create new album
</button>
<br>

<!-- Modal -->
<div class=\"modal fade\" id=\"uploadPhotosInAlbumModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"uploadPhotosInAlbumModal\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered modal-xl\" role=\"document\">
        <div class=\"modal-content\">
        <form method=\"post\" enctype=\"multipart/form-data\">   
            <div class=\"modal-header\">
{#                <h1 class=\"modal-title\" id=\"uploadPhotosInAlbumModalTitle\">Create&nbsp;Album</h1>#}
                <div class=\"input-group input-group-lg\">
                    <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\">Create&nbsp;Album</span>
                    </div>
                  <input name=\"albumName\" placeholder=\"Album name\" type=\"text\" class=\"form-control\" aria-label=\"Sizing example input\" aria-describedby=\"inputGroup-sizing-lg\">
                </div>                
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
                     

                
                <div class=\"modal-body\">
                    <div class=\"row\">
                        <div class=\"col-3\" style=\"background-color:lavenderblush\">
                            <textarea name=\"description\" rows=\"6\" placeholder=\"Description...\" style=\"width: 100%\"></textarea>
                <br>                    
{#                    {% if errorList %}
                        <ul>
                            {% for error in errorList %}
                                <li>{{error}}</li>
                            {% endfor %}   
                        </ul>
                    {% endif %}#}                    
                    <button id=\"buttonAddPicture\" type=\"button\" class=\"btn btn-primary\">
                        Select pictures</button>
{#                    <input type=\"file\" id=\"inputLoadImage\" name=\"images[]\" 
                           style=\"display:none\" accept=\"image/gif, image/jpeg, image/png\" multiple/>#}
                    </div>
                    <div class=\"col-9\">
                    <div id=\"previewImage\">
{#                    {% if errorList %}
                        <ul>
                            {% for error in errorList %}
                                <li>{{error}}</li>
                            {% endfor %}   
                        </ul>
                    {% endif %}#} 
                    </div>
                    <br>
                    </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <input type=\"submit\" value=\"Create Album\" class=\"btn btn-primary\" name=\"loadInfo\"><br>
                    {#        <button type=\"button\" class=\"btn btn-primary\">Save changes</button>#} 
                </div>
            </form> 
            <form method=\"post\" enctype=\"multipart/form-data\">  
                <input type=\"file\" id=\"inputLoadImage\" name=\"images[]\" 
                            accept=\"image/gif, image/jpeg, image/png\" multiple/>
                <input type=\"submit\" value=\"Load Images\" class=\"btn btn-primary\" name=\"loadImages\">
            </form>     
        </div>
    </div>
</div>
<div class=\"container\"> 
    <br>
    {% for album in albumList %}
        {% if loop.index is odd %}
            <div class=\"row\">
        {% endif %}
        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>
                    <img src='/images/mark-zuckerberg-4.jpg' height=\"100\"/>
                {{ loop.index }} 
                <a href='pictures/album/{{album.id}}'><b>{{album.albumName}}</b></a>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        {% if loop.index is even %}
            </div>
        {% endif %}    
        {% if loop.last %}
            {% if loop.index is odd %}
                </div>
            {% endif %}  
        {% endif %}
            <br>
    {% endfor %}    

</div>
{% endblock %}", "user_pictures.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_pictures.html.twig");
    }
}

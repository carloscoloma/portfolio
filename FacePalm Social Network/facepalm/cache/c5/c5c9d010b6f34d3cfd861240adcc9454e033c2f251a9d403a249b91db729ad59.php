<?php

/* user_pictures.html.twig */
class __TwigTemplate_bf2224f6e230efc7a6ec2e25ff0d4b4b656edbaef7297f3f128eb4ded5ac5501 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_pictures.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script>
        
            \$(document).ready(function() {
                ";
        // line 8
        if (($context["errorList"] ?? null)) {
            // line 9
            echo "                    \$(\"#buttonCreateAlbum\").click();
                ";
        }
        // line 10
        echo " 
                \$(\"#buttonCreateAlbum\").click(function() {
                    loadFile();
                });
                \$(\"#buttonAddPicture\").click(function() {
                    loadFile();
                });
                \$(\"#inputLoadImage\").change (function(){ 
                    previewFile();
                });
                
            });
        function loadFile() {
            \$('#previewImage').empty();
            \$(\"#inputLoadImage\").click();
        }
        function previewFile() {
                var allFiles = \$(\"#inputLoadImage\")[0].files;
                for (var i = 0; i < allFiles.length; i++)
                {
                    if (!/\\.(jpe?g|png|gif)\$/i.test(allFiles[i].name)) {
                          return alert(allFiles[i].name + \" is not an image\");
                        }                    
                    var image = new Image();
                    image.height = 200;
                    image.title = allFiles[i].name;
                    image.src = URL.createObjectURL(allFiles[i]);
                    image.className = \"rounded m-3 img-responsive border-primary\";
                   
                    var descriptionText = document.createElement(\"textarea\");
                    descriptionText.name=\"description_\" + i;
                    descriptionText.cols=40;
                    descriptionText.rows=5;
                    descriptionText.defaultValue =image.title;
                    
                    var largeDiv = document.createElement(\"div\");
                    largeDiv.className = \"row border border-primary m-3 p-2\";
                    
                    var imageDiv = document.createElement(\"div\");
                    imageDiv.className = \"col-5\";
                    
                    var textDiv = document.createElement(\"div\");
                    textDiv.className = \"col\";
                    
                    var br = document.createElement('br');
                    var h6 = document.createElement(\"H6\");
                    
                    imageDiv.append(image);
                    
                    h6.append(\"Description:\");
                    textDiv.append(h6);
";
        // line 62
        echo "                    textDiv.append(descriptionText);
                    
                    largeDiv.append(imageDiv);
                    largeDiv.append(textDiv);
                    \$('#previewImage').append(largeDiv);

                }
            }
            
        function deleteAlbum(albumid) {
                console.log(\"delete album id: \" + albumid);
                console.log(\"/";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/\" + albumid + \"/delete\");
                if (confirm(\"Are you sure to delete this album ?\")){
                    var idToHide = \"#album_\" + (albumid + \"\");
                    \$(idToHide).hide();
                        \$.ajax({
                            url: \"/";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/\" + albumid + \"/delete\",
                            // data: {},
                            type: \"DELETE\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Deleted successfully\");
                        });                    
                }
            }
           
    </script>
";
    }

    // line 90
    public function block_addsection($context, array $blocks = array())
    {
        // line 92
        echo "   
<!-- Button trigger modal -->
<button id=\"buttonCreateAlbum\" type=\"button\" class=\"btn btn-primary\" rel=\"tooltip\"
        data-toggle=\"modal\" data-placement=\"right\" title=\"Choose a file to upload\" data-target=\"#uploadPhotosInAlbumModal\">
  + Create new album
</button>
<br><br>
<!-- Modal -->
<div class=\"modal fade\" id=\"uploadPhotosInAlbumModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"uploadPhotosInAlbumModal\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered modal-xl\" role=\"document\" >
        <div class=\"modal-content\">
            <form method=\"post\" enctype=\"multipart/form-data\">
            <div class=\"modal-header bg-light\">
";
        // line 106
        echo "                <div class=\"input-group input-group-xl\">
                    <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\">Create&nbsp;Album</span>
                    </div>
                  <input name=\"albumName\" placeholder=\"Album name\" type=\"text\" 
                         class=\"form-control\" aria-label=\"Sizing example input\" aria-describedby=\"inputGroup-sizing-lg\" value=\"";
        // line 111
        echo twig_escape_filter($this->env, ($context["albumName"] ?? null), "html", null, true);
        echo "\">
                </div>                
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            
                <div class=\"modal-body\">
                    <div class=\"row\">
                    <div class=\"col-3\" style=\"\">
                        <textarea name=\"albumDescription\" rows=\"6\" placeholder=\"Description...\" style=\"width: 100%\">";
        // line 121
        echo twig_escape_filter($this->env, ($context["albumDescription"] ?? null), "html", null, true);
        echo "</textarea>
                        <br>                    
                  
                    <br><br>

                    <button id=\"buttonAddPicture\" type=\"button\" class=\"btn btn-primary\">
                        Select pictures</button>
                    <input type=\"file\" id=\"inputLoadImage\" name=\"images[]\" 
                           style=\"display:none\" accept=\"image/gif, image/jpeg, image/png\" multiple/>
                    </div>
                    <div class=\"col-9\">
                    <div id=\"previewImage\">
                    ";
        // line 133
        if (($context["errorList"] ?? null)) {
            // line 134
            echo "                        <ul>
                            ";
            // line 135
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 136
                echo "                                <li class='text-danger'><i class=\"material-icons\">error_outline</i> ";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 137
            echo "   
                        </ul>
                    ";
        }
        // line 139
        echo "  
                    </div>
                    <br>
                    </div>
                    </div>
                </div>
                <div class=\"modal-footer bg-light\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <input type=\"submit\" value=\"Create Album\" class=\"btn btn-primary\"><br>
                    ";
        // line 148
        echo " 
                </div>
            </form> 
        </div>
    </div>
</div>
<div class=\"container\">
    ";
        // line 155
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["albumList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["album"]) {
            // line 156
            echo "        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 157
                echo "            <div class=\"row\">
        ";
            }
            // line 159
            echo "        <a href='pictures/album/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "id", array()), "html", null, true);
            echo "'class=\"text-white\" >
          <div class=\"col-5 border border-light rounded\" id=\"album_";
            // line 160
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "id", array()), "html", null, true);
            echo "\" 
               style=\"background: url(/";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/pictures/album/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "albumCoverPictureId", array()), "html", null, true);
            echo ") black no-repeat center; background-size:cover; height: 200px; width: 400px;\">
";
            // line 163
            echo "                <a href='pictures/album/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "id", array()), "html", null, true);
            echo "'class=\"text-white blockquote\" ><b>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "albumName", array()), "html", null, true);
            echo "</b></a>
              <div class=\"row justify-content-around\">
                    <div class=\"col-4\">
                    </div>                  
                <div class=\"col-1\">  
                    ";
            // line 168
            if ($this->getAttribute($context["album"], "editable", array())) {
                // line 169
                echo "                        <a href=\"#\" class=\"btn btn-danger \" onclick=\"deleteAlbum(";
                echo twig_escape_filter($this->env, $this->getAttribute($context["album"], "id", array()), "html", null, true);
                echo ")\"><i class=\"material-icons\">delete_forever</i></a>
                    ";
            }
            // line 170
            echo "    
              </div>

            </div>
          </div>
            <div class=\"col-1 \"></div>
            
        ";
            // line 177
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 178
                echo "            </div>
        ";
            }
            // line 179
            echo "    
        ";
            // line 180
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 181
                echo "            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 182
                    echo "                </div>
            ";
                }
                // line 183
                echo "  
        ";
            }
            // line 185
            echo "            <br>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['album'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 186
        echo "    

</div>
";
    }

    public function getTemplateName()
    {
        return "user_pictures.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  344 => 186,  329 => 185,  325 => 183,  321 => 182,  318 => 181,  316 => 180,  313 => 179,  309 => 178,  307 => 177,  298 => 170,  292 => 169,  290 => 168,  279 => 163,  271 => 161,  267 => 160,  262 => 159,  258 => 157,  255 => 156,  238 => 155,  229 => 148,  218 => 139,  213 => 137,  204 => 136,  200 => 135,  197 => 134,  195 => 133,  180 => 121,  167 => 111,  160 => 106,  145 => 92,  142 => 90,  126 => 78,  118 => 73,  105 => 62,  52 => 10,  48 => 9,  46 => 8,  38 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}
{% block addhead %}   
    <script>
        
            \$(document).ready(function() {
                {% if errorList %}
                    \$(\"#buttonCreateAlbum\").click();
                {% endif %} 
                \$(\"#buttonCreateAlbum\").click(function() {
                    loadFile();
                });
                \$(\"#buttonAddPicture\").click(function() {
                    loadFile();
                });
                \$(\"#inputLoadImage\").change (function(){ 
                    previewFile();
                });
                
            });
        function loadFile() {
            \$('#previewImage').empty();
            \$(\"#inputLoadImage\").click();
        }
        function previewFile() {
                var allFiles = \$(\"#inputLoadImage\")[0].files;
                for (var i = 0; i < allFiles.length; i++)
                {
                    if (!/\\.(jpe?g|png|gif)\$/i.test(allFiles[i].name)) {
                          return alert(allFiles[i].name + \" is not an image\");
                        }                    
                    var image = new Image();
                    image.height = 200;
                    image.title = allFiles[i].name;
                    image.src = URL.createObjectURL(allFiles[i]);
                    image.className = \"rounded m-3 img-responsive border-primary\";
                   
                    var descriptionText = document.createElement(\"textarea\");
                    descriptionText.name=\"description_\" + i;
                    descriptionText.cols=40;
                    descriptionText.rows=5;
                    descriptionText.defaultValue =image.title;
                    
                    var largeDiv = document.createElement(\"div\");
                    largeDiv.className = \"row border border-primary m-3 p-2\";
                    
                    var imageDiv = document.createElement(\"div\");
                    imageDiv.className = \"col-5\";
                    
                    var textDiv = document.createElement(\"div\");
                    textDiv.className = \"col\";
                    
                    var br = document.createElement('br');
                    var h6 = document.createElement(\"H6\");
                    
                    imageDiv.append(image);
                    
                    h6.append(\"Description:\");
                    textDiv.append(h6);
{#                    textDiv.append(br);#}
                    textDiv.append(descriptionText);
                    
                    largeDiv.append(imageDiv);
                    largeDiv.append(textDiv);
                    \$('#previewImage').append(largeDiv);

                }
            }
            
        function deleteAlbum(albumid) {
                console.log(\"delete album id: \" + albumid);
                console.log(\"/{{user.username}}/pictures/album/\" + albumid + \"/delete\");
                if (confirm(\"Are you sure to delete this album ?\")){
                    var idToHide = \"#album_\" + (albumid + \"\");
                    \$(idToHide).hide();
                        \$.ajax({
                            url: \"/{{user.username}}/pictures/album/\" + albumid + \"/delete\",
                            // data: {},
                            type: \"DELETE\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Deleted successfully\");
                        });                    
                }
            }
           
    </script>
{% endblock %}
{% block addsection %}
{#    <h1>Album list for {{user.firstName}} {{user.lastName}} here!!</h1>#}
   
<!-- Button trigger modal -->
<button id=\"buttonCreateAlbum\" type=\"button\" class=\"btn btn-primary\" rel=\"tooltip\"
        data-toggle=\"modal\" data-placement=\"right\" title=\"Choose a file to upload\" data-target=\"#uploadPhotosInAlbumModal\">
  + Create new album
</button>
<br><br>
<!-- Modal -->
<div class=\"modal fade\" id=\"uploadPhotosInAlbumModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"uploadPhotosInAlbumModal\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered modal-xl\" role=\"document\" >
        <div class=\"modal-content\">
            <form method=\"post\" enctype=\"multipart/form-data\">
            <div class=\"modal-header bg-light\">
{#                <h1 class=\"modal-title\" id=\"uploadPhotosInAlbumModalTitle\">Create&nbsp;Album</h1>#}
                <div class=\"input-group input-group-xl\">
                    <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\">Create&nbsp;Album</span>
                    </div>
                  <input name=\"albumName\" placeholder=\"Album name\" type=\"text\" 
                         class=\"form-control\" aria-label=\"Sizing example input\" aria-describedby=\"inputGroup-sizing-lg\" value=\"{{albumName}}\">
                </div>                
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            
                <div class=\"modal-body\">
                    <div class=\"row\">
                    <div class=\"col-3\" style=\"\">
                        <textarea name=\"albumDescription\" rows=\"6\" placeholder=\"Description...\" style=\"width: 100%\">{{albumDescription}}</textarea>
                        <br>                    
                  
                    <br><br>

                    <button id=\"buttonAddPicture\" type=\"button\" class=\"btn btn-primary\">
                        Select pictures</button>
                    <input type=\"file\" id=\"inputLoadImage\" name=\"images[]\" 
                           style=\"display:none\" accept=\"image/gif, image/jpeg, image/png\" multiple/>
                    </div>
                    <div class=\"col-9\">
                    <div id=\"previewImage\">
                    {% if errorList %}
                        <ul>
                            {% for error in errorList %}
                                <li class='text-danger'><i class=\"material-icons\">error_outline</i> {{error}}</li>
                            {% endfor %}   
                        </ul>
                    {% endif %}  
                    </div>
                    <br>
                    </div>
                    </div>
                </div>
                <div class=\"modal-footer bg-light\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <input type=\"submit\" value=\"Create Album\" class=\"btn btn-primary\"><br>
                    {#        <button type=\"button\" class=\"btn btn-primary\">Save changes</button>#} 
                </div>
            </form> 
        </div>
    </div>
</div>
<div class=\"container\">
    {% for album in albumList %}
        {% if loop.index is odd %}
            <div class=\"row\">
        {% endif %}
        <a href='pictures/album/{{album.id}}'class=\"text-white\" >
          <div class=\"col-5 border border-light rounded\" id=\"album_{{album.id}}\" 
               style=\"background: url(/{{user.username}}/pictures/album/{{album.id}}/image/{{album.albumCoverPictureId}}) black no-repeat center; background-size:cover; height: 200px; width: 400px;\">
{#              {{ loop.index }} #}
                <a href='pictures/album/{{album.id}}'class=\"text-white blockquote\" ><b>{{album.albumName}}</b></a>
              <div class=\"row justify-content-around\">
                    <div class=\"col-4\">
                    </div>                  
                <div class=\"col-1\">  
                    {% if album.editable %}
                        <a href=\"#\" class=\"btn btn-danger \" onclick=\"deleteAlbum({{album.id}})\"><i class=\"material-icons\">delete_forever</i></a>
                    {% endif %}    
              </div>

            </div>
          </div>
            <div class=\"col-1 \"></div>
            
        {% if loop.index is even %}
            </div>
        {% endif %}    
        {% if loop.last %}
            {% if loop.index is odd %}
                </div>
            {% endif %}  
        {% endif %}
            <br>
    {% endfor %}    

</div>
{% endblock %}", "user_pictures.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_pictures.html.twig");
    }
}

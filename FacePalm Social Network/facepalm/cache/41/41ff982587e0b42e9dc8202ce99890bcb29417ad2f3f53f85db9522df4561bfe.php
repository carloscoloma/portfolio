<?php

/* user_pictures_album.html.twig */
class __TwigTemplate_4d69c1d416c5c5fb6a4677b88621b442744f393437d81052dd5edf78d3b2129e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_pictures_album.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script>
        
            \$(document).ready(function() {
                ";
        // line 8
        if (($context["errorList"] ?? null)) {
            // line 9
            echo "                    \$(\"#buttonCreateAlbum\").click();
                ";
        }
        // line 10
        echo " 
                \$(\"#buttonCreateAlbum\").click(function() {
                    loadFile();
                });
                \$(\"#buttonAddPicture\").click(function() {
                    loadFile();
                });
                \$(\"#inputLoadImage\").change (function(){ 
                    previewFile();
                });
                
            });
        function loadFile() {
            \$('#previewImage').empty();
            \$(\"#inputLoadImage\").click();
        }
        function previewFile() {
                var allFiles = \$(\"#inputLoadImage\")[0].files;
                for (var i = 0; i < allFiles.length; i++)
                {
                    if (!/\\.(jpe?g|png|gif)\$/i.test(allFiles[i].name)) {
                          return alert(allFiles[i].name + \" is not an image\");
                        }                    
                    var image = new Image();
                    image.height = 200;
                    image.title = allFiles[i].name;
                    image.src = URL.createObjectURL(allFiles[i]);
                    image.className = \"rounded m-3 img-responsive border-primary\";
                   
                    var descriptionText = document.createElement(\"textarea\");
                    descriptionText.name=\"description_\" + i;
                    descriptionText.cols=40;
                    descriptionText.rows=5;
                    descriptionText.defaultValue =image.title;
                    
                    var largeDiv = document.createElement(\"div\");
                    largeDiv.className = \"row border border-primary m-3 p-2\";
                    
                    var imageDiv = document.createElement(\"div\");
                    imageDiv.className = \"col-5\";
                    
                    var textDiv = document.createElement(\"div\");
                    textDiv.className = \"col\";
                    
                    var br = document.createElement('br');
                    var h6 = document.createElement(\"H6\");
                    
                    imageDiv.append(image);
                    
                    h6.append(\"Description:\");
                    textDiv.append(h6);
";
        // line 62
        echo "                    textDiv.append(descriptionText);
                    
                    largeDiv.append(imageDiv);
                    largeDiv.append(textDiv);
                    \$('#previewImage').append(largeDiv);

                }
            }
            
        function deleteAlbum(albumid) {
                console.log(\"delete album id: \" + albumid);
                console.log(\"/";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/\" + albumid + \"/delete\");
                if (confirm(\"Are you sure to delete this album ?\")){
                    var idToHide = \"#album_\" + (albumid + \"\");
                    \$(idToHide).hide();
                        \$.ajax({
                            url: \"/";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/pictures/album/\" + albumid + \"/delete\",
                            // data: {},
                            type: \"DELETE\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Deleted successfully\");
                        });                    
                }
            }
           
    </script>
";
    }

    // line 90
    public function block_addsection($context, array $blocks = array())
    {
        // line 91
        echo "    <h1>";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["albumInfo"] ?? null), "albumName", array()), "html", null, true);
        echo "</h1>
    <h5>";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute(($context["albumInfo"] ?? null), "description", array()), "html", null, true);
        echo "</h5> 
    <!-- Button trigger modal -->
<button id=\"buttonCreateAlbum\" type=\"button\" class=\"btn btn-primary\" rel=\"tooltip\"
        data-toggle=\"modal\" data-placement=\"right\" title=\"Choose a file to upload\" data-target=\"#uploadPhotosInAlbumModal\">
  + Add more pictures
</button>
<br><br>
<!-- Modal -->
<div class=\"modal fade\" id=\"uploadPhotosInAlbumModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"uploadPhotosInAlbumModal\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered modal-xl\" role=\"document\" >
        <div class=\"modal-content\">
            <form method=\"post\" enctype=\"multipart/form-data\">
            <div class=\"modal-header bg-light\">
";
        // line 106
        echo "                <div class=\"input-group input-group-xl\">
                    <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\">Add&nbsp;Pictures</span>
                    </div>
                  ";
        // line 112
        echo "                </div>                
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            
                <div class=\"modal-body\">
                    <div class=\"row\">
                    <div class=\"col-3\" style=\"\">
                  ";
        // line 122
        echo "                        <br>                    
                  
                    <br><br>

                    <button id=\"buttonAddPicture\" type=\"button\" class=\"btn btn-primary\">
                        Select pictures</button>
                    <input type=\"file\" id=\"inputLoadImage\" name=\"images[]\" 
                           style=\"display:none\" accept=\"image/gif, image/jpeg, image/png\" multiple/>
                    </div>
                    <div class=\"col-9\">
                    <div id=\"previewImage\">
                    ";
        // line 133
        if (($context["errorList"] ?? null)) {
            // line 134
            echo "                        <ul>
                            ";
            // line 135
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 136
                echo "                                <li class='text-danger'><i class=\"material-icons\">error_outline</i> ";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 137
            echo "   
                        </ul>
                    ";
        }
        // line 139
        echo "  
                    </div>
                    <br>
                    </div>
                    </div>
                </div>
                <div class=\"modal-footer bg-light\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <input type=\"submit\" value=\"Add Pictures\" class=\"btn btn-primary\"><br>
                    ";
        // line 148
        echo " 
                </div>
            </form> 
        </div>
    </div>
</div>
<div class=\"container\">
    ";
        // line 155
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["pictureList"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["picture"]) {
            // line 156
            echo "        ";
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                // line 157
                echo "            <div class=\"row\">
        ";
            }
            // line 159
            echo "        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>";
            // line 162
            echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
            echo "
                <a href=\"/";
            // line 163
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/pictures/album/";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["albumInfo"] ?? null), "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["picture"], "id", array()), "html", null, true);
            echo "\">
                    <img src=\"/";
            // line 164
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/pictures/album/";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["albumInfo"] ?? null), "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["picture"], "id", array()), "html", null, true);
            echo "\" height=\"100\"/>
                </a>
            <p>";
            // line 166
            echo twig_escape_filter($this->env, $this->getAttribute($context["picture"], "description", array()), "html", null, true);
            echo "</p>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        ";
            // line 172
            if (($this->getAttribute($context["loop"], "index", array()) % 2 == 0)) {
                // line 173
                echo "            </div>
        ";
            }
            // line 174
            echo "    
        ";
            // line 175
            if ($this->getAttribute($context["loop"], "last", array())) {
                // line 176
                echo "            ";
                if (($this->getAttribute($context["loop"], "index", array()) % 2 == 1)) {
                    // line 177
                    echo "                </div>
            ";
                }
                // line 178
                echo "  
        ";
            }
            // line 180
            echo "            <br>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['picture'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 181
        echo "    

</div>
";
    }

    public function getTemplateName()
    {
        return "user_pictures_album.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  335 => 181,  320 => 180,  316 => 178,  312 => 177,  309 => 176,  307 => 175,  304 => 174,  300 => 173,  298 => 172,  289 => 166,  280 => 164,  272 => 163,  268 => 162,  263 => 159,  259 => 157,  256 => 156,  239 => 155,  230 => 148,  219 => 139,  214 => 137,  205 => 136,  201 => 135,  198 => 134,  196 => 133,  183 => 122,  172 => 112,  166 => 106,  150 => 92,  145 => 91,  142 => 90,  126 => 78,  118 => 73,  105 => 62,  52 => 10,  48 => 9,  46 => 8,  38 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}
{% block addhead %}   
    <script>
        
            \$(document).ready(function() {
                {% if errorList %}
                    \$(\"#buttonCreateAlbum\").click();
                {% endif %} 
                \$(\"#buttonCreateAlbum\").click(function() {
                    loadFile();
                });
                \$(\"#buttonAddPicture\").click(function() {
                    loadFile();
                });
                \$(\"#inputLoadImage\").change (function(){ 
                    previewFile();
                });
                
            });
        function loadFile() {
            \$('#previewImage').empty();
            \$(\"#inputLoadImage\").click();
        }
        function previewFile() {
                var allFiles = \$(\"#inputLoadImage\")[0].files;
                for (var i = 0; i < allFiles.length; i++)
                {
                    if (!/\\.(jpe?g|png|gif)\$/i.test(allFiles[i].name)) {
                          return alert(allFiles[i].name + \" is not an image\");
                        }                    
                    var image = new Image();
                    image.height = 200;
                    image.title = allFiles[i].name;
                    image.src = URL.createObjectURL(allFiles[i]);
                    image.className = \"rounded m-3 img-responsive border-primary\";
                   
                    var descriptionText = document.createElement(\"textarea\");
                    descriptionText.name=\"description_\" + i;
                    descriptionText.cols=40;
                    descriptionText.rows=5;
                    descriptionText.defaultValue =image.title;
                    
                    var largeDiv = document.createElement(\"div\");
                    largeDiv.className = \"row border border-primary m-3 p-2\";
                    
                    var imageDiv = document.createElement(\"div\");
                    imageDiv.className = \"col-5\";
                    
                    var textDiv = document.createElement(\"div\");
                    textDiv.className = \"col\";
                    
                    var br = document.createElement('br');
                    var h6 = document.createElement(\"H6\");
                    
                    imageDiv.append(image);
                    
                    h6.append(\"Description:\");
                    textDiv.append(h6);
{#                    textDiv.append(br);#}
                    textDiv.append(descriptionText);
                    
                    largeDiv.append(imageDiv);
                    largeDiv.append(textDiv);
                    \$('#previewImage').append(largeDiv);

                }
            }
            
        function deleteAlbum(albumid) {
                console.log(\"delete album id: \" + albumid);
                console.log(\"/{{user.username}}/pictures/album/\" + albumid + \"/delete\");
                if (confirm(\"Are you sure to delete this album ?\")){
                    var idToHide = \"#album_\" + (albumid + \"\");
                    \$(idToHide).hide();
                        \$.ajax({
                            url: \"/{{user.username}}/pictures/album/\" + albumid + \"/delete\",
                            // data: {},
                            type: \"DELETE\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Deleted successfully\");
                        });                    
                }
            }
           
    </script>
{% endblock %}
{% block addsection %}
    <h1>{{albumInfo.albumName}}</h1>
    <h5>{{albumInfo.description}}</h5> 
    <!-- Button trigger modal -->
<button id=\"buttonCreateAlbum\" type=\"button\" class=\"btn btn-primary\" rel=\"tooltip\"
        data-toggle=\"modal\" data-placement=\"right\" title=\"Choose a file to upload\" data-target=\"#uploadPhotosInAlbumModal\">
  + Add more pictures
</button>
<br><br>
<!-- Modal -->
<div class=\"modal fade\" id=\"uploadPhotosInAlbumModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"uploadPhotosInAlbumModal\" aria-hidden=\"true\">
    <div class=\"modal-dialog modal-dialog-centered modal-xl\" role=\"document\" >
        <div class=\"modal-content\">
            <form method=\"post\" enctype=\"multipart/form-data\">
            <div class=\"modal-header bg-light\">
{#                <h1 class=\"modal-title\" id=\"uploadPhotosInAlbumModalTitle\">Create&nbsp;Album</h1>#}
                <div class=\"input-group input-group-xl\">
                    <div class=\"input-group-prepend\">
                        <span class=\"input-group-text\" id=\"inputGroup-sizing-lg\">Add&nbsp;Pictures</span>
                    </div>
                  {#<input name=\"albumName\" placeholder=\"Album name\" type=\"text\" 
                         class=\"form-control\" aria-label=\"Sizing example input\" aria-describedby=\"inputGroup-sizing-lg\" value=\"{{albumName}}\">#}
                </div>                
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>
            
                <div class=\"modal-body\">
                    <div class=\"row\">
                    <div class=\"col-3\" style=\"\">
                  {#      <textarea name=\"albumDescription\" rows=\"6\" placeholder=\"Description...\" style=\"width: 100%\">{{albumDescription}}</textarea>#}
                        <br>                    
                  
                    <br><br>

                    <button id=\"buttonAddPicture\" type=\"button\" class=\"btn btn-primary\">
                        Select pictures</button>
                    <input type=\"file\" id=\"inputLoadImage\" name=\"images[]\" 
                           style=\"display:none\" accept=\"image/gif, image/jpeg, image/png\" multiple/>
                    </div>
                    <div class=\"col-9\">
                    <div id=\"previewImage\">
                    {% if errorList %}
                        <ul>
                            {% for error in errorList %}
                                <li class='text-danger'><i class=\"material-icons\">error_outline</i> {{error}}</li>
                            {% endfor %}   
                        </ul>
                    {% endif %}  
                    </div>
                    <br>
                    </div>
                    </div>
                </div>
                <div class=\"modal-footer bg-light\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <input type=\"submit\" value=\"Add Pictures\" class=\"btn btn-primary\"><br>
                    {#        <button type=\"button\" class=\"btn btn-primary\">Save changes</button>#} 
                </div>
            </form> 
        </div>
    </div>
</div>
<div class=\"container\">
    {% for picture in pictureList %}
        {% if loop.index is odd %}
            <div class=\"row\">
        {% endif %}
        
          <div class=\"col-5 border border-dark\">
            <p>
                <br>{{ loop.index }}
                <a href=\"/{{user.username}}/pictures/album/{{albumInfo.id}}/image/{{picture.id}}\">
                    <img src=\"/{{user.username}}/pictures/album/{{albumInfo.id}}/image/{{picture.id}}\" height=\"100\"/>
                </a>
            <p>{{picture.description}}</p>
                <br>
            </p>
          </div>
            <div class=\"col-1 \"></div>
            
        {% if loop.index is even %}
            </div>
        {% endif %}    
        {% if loop.last %}
            {% if loop.index is odd %}
                </div>
            {% endif %}  
        {% endif %}
            <br>
    {% endfor %}    

</div>
{% endblock %}", "user_pictures_album.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_pictures_album.html.twig");
    }
}

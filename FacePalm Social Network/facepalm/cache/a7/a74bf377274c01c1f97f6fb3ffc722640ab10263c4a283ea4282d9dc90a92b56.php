<?php

/* logout.html.twig */
class __TwigTemplate_a4fc12243e147730ad1653b17daa623c717c3af1945e7564b48b5dbdf668a5e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "logout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Logged out";
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        echo "  
            <meta http-equiv=\"refresh\" content=\"3; url=/signup\" />
        ";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
        // line 9
        echo "    <h1>You've been logged out. You'll be redirected in 3 seconds</h1>
";
    }

    public function getTemplateName()
    {
        return "logout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  44 => 8,  36 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Logged out{% endblock %}
        {% block addhead %}  
            <meta http-equiv=\"refresh\" content=\"3; url=/signup\" />
        {% endblock %}

{% block content %}
    <h1>You've been logged out. You'll be redirected in 3 seconds</h1>
{% endblock %}", "logout.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\logout.html.twig");
    }
}

<?php

/* user_friends.html.twig */
class __TwigTemplate_1907ee1d4285570e775d8175c9de15986b2f4a48a37051f01d551a808e537745 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("user.html.twig", "user_friends.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'addsection' => array($this, 'block_addsection'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "user.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        echo "   
    <script>
        function unfriend(userid) {
                if (confirm(\"Are you sure ?\")){
                        \$.ajax({
                            url: \"/";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/unfriend/\" + userid,
                            // data: {},
                            type: \"DELETE\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Deleted successfully\");
                        });                    
                }
                location.reload();
            }
        
        function acceptRequest(userid) {
                        var friendship = {userRequestId: userid + \"\", userAnswerId: ";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "id", array()), "html", null, true);
        echo " + \"\", friendshipAccepted: 1 + \"\"};  
                        \$.ajax({
                            url: \"/";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/acceptrequest/\" + userid,
                            data: JSON.stringify(friendship),
                            type: \"PUT\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Request accepted successfully, you are friends!\");
                        });                    
                location.reload();
            }
            
        function addFriend(userid) {
                        var friendship = {userRequestId: ";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "id", array()), "html", null, true);
        echo " + \"\", userAnswerId: userid + \"\", friendshipAccepted: 0 + \"\"};
                        \$.ajax({
                            url: \"/";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
        echo "/addfriend/\" + userid,
                            data: JSON.stringify(friendship),
                            type: \"POST\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Request Sent successfully\");
                        });                    
                location.reload();
            }            
    </script>
";
    }

    // line 47
    public function block_addsection($context, array $blocks = array())
    {
        // line 49
        echo "<div class=\"container\">
    ";
        // line 50
        if (($context["friendList"] ?? null)) {
            // line 51
            echo "    ";
        } else {
            echo "    
        <h1>Hello ";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "lastName", array()), "html", null, true);
            echo ",<br> look for new friends on the search bar!</h1>
    ";
        }
        // line 53
        echo "    
    
    <div class=\"card-columns\">
    ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["friendList"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["friend"]) {
            // line 58
            echo "            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "profilePictureId", array()), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "lastName", array()), "html", null, true);
            echo "\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? null), "username", array()), "html", null, true);
            echo "/search/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "id", array()), "html", null, true);
            echo "/image/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "profilePictureId", array()), "html", null, true);
            echo "'> 
                        ";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "firstName", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "lastName", array()), "html", null, true);
            echo "
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-success dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "id", array()), "html", null, true);
            echo "\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Friends
                      </button>
                      <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton";
            // line 71
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "id", array()), "html", null, true);
            echo "\">
                        <span class=\"dropdown-item\" onclick=\"unfriend(";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute($context["friend"], "id", array()), "html", null, true);
            echo ")\" >Unfriend</span>
                      </div>
                    </div>
              </div>
            </div>    
";
            // line 78
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['friend'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo "    
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "user_friends.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 78,  175 => 72,  171 => 71,  165 => 68,  156 => 64,  148 => 63,  134 => 60,  130 => 58,  126 => 56,  121 => 53,  114 => 52,  109 => 51,  107 => 50,  104 => 49,  101 => 47,  86 => 36,  81 => 34,  67 => 23,  62 => 21,  47 => 9,  38 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"user.html.twig\" %}

{% block title %}{{user.firstName}} {{user.lastName}}{% endblock %}
{% block addhead %}   
    <script>
        function unfriend(userid) {
                if (confirm(\"Are you sure ?\")){
                        \$.ajax({
                            url: \"/{{user.username}}/unfriend/\" + userid,
                            // data: {},
                            type: \"DELETE\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Deleted successfully\");
                        });                    
                }
                location.reload();
            }
        
        function acceptRequest(userid) {
                        var friendship = {userRequestId: userid + \"\", userAnswerId: {{user.id}} + \"\", friendshipAccepted: 1 + \"\"};  
                        \$.ajax({
                            url: \"/{{user.username}}/acceptrequest/\" + userid,
                            data: JSON.stringify(friendship),
                            type: \"PUT\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Request accepted successfully, you are friends!\");
                        });                    
                location.reload();
            }
            
        function addFriend(userid) {
                        var friendship = {userRequestId: {{user.id}} + \"\", userAnswerId: userid + \"\", friendshipAccepted: 0 + \"\"};
                        \$.ajax({
                            url: \"/{{user.username}}/addfriend/\" + userid,
                            data: JSON.stringify(friendship),
                            type: \"POST\",
                            dataType: \"json\"
                        }).done(function () {
                            alert(\"Request Sent successfully\");
                        });                    
                location.reload();
            }            
    </script>
{% endblock %}
{% block addsection %}
{#    <h1>Friend list for {{user.firstName}} {{user.lastName}} here</h1>#}
<div class=\"container\">
    {% if friendList %}
    {% else %}    
        <h1>Hello {{user.firstName}} {{user.lastName}},<br> look for new friends on the search bar!</h1>
    {% endif %}    
    
    <div class=\"card-columns\">
    {% for friend in friendList %}
{#    {% for userSearched in searchedAndFriends %}#}
            <div class=\"card text-center\" style=\"width: 13rem;\">
              <img class=\"card-img-top rounded-circle border border-white profilepicture\" style=\"width: 150px; height: 150px;\"
                   src=\"/{{user.username}}/search/{{friend.id}}/image/{{friend.profilePictureId}}\" alt=\"{{friend.firstName}} {{friend.lastName}}\">
              <div class=\"card-body\">
                <h5 class=\"card-title\">
                    <a href='/{{user.username}}/search/{{friend.id}}/image/{{friend.profilePictureId}}'> 
                        {{friend.firstName}} {{friend.lastName}}
                    </a>
                </h5>
                    <div class=\"dropdown\">
                      <button class=\"btn btn-success dropdown-toggle\" type=\"button\" id=\"dropdownMenuButton{{friend.id}}\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                        Friends
                      </button>
                      <div class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuButton{{friend.id}}\">
                        <span class=\"dropdown-item\" onclick=\"unfriend({{friend.id}})\" >Unfriend</span>
                      </div>
                    </div>
              </div>
            </div>    
{#    {% endfor %}#}
    {% endfor %}    
    </div>
</div>
{% endblock %}", "user_friends.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\user_friends.html.twig");
    }
}

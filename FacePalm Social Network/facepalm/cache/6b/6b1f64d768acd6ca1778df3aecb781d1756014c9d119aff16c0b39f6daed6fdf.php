<?php

/* signup.html.twig */
class __TwigTemplate_4fef042c6a8a0372a35697a50f7ffc2b76bdec0d758135dafd74e10562b14ced extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css\" integrity=\"sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS\" crossorigin=\"anonymous\">
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js\" integrity=\"sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" integrity=\"sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k\" crossorigin=\"anonymous\"></script>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
        <script src='https://www.google.com/recaptcha/api.js?render=6Le_nYwUAAAAAE4TGu1slq-SMKCq8vwraxEaXF69'></script>
        <script>
            grecaptcha.ready(function () {
                grecaptcha.execute('6Le_nYwUAAAAAE4TGu1slq-SMKCq8vwraxEaXF69', {action: 'register'})
                        .then(function (token) {
                            var recaptchaResponse = document.getElementById('recaptchaResponse');
                            recaptchaResponse.value = token;
                        });
            });
        </script>
        <link rel=\"stylesheet\" href=\"/styles.css\" />
        <title>FacePalm</title>
    </head>
    <body>
        <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark flex-column flex-md-row bd-navbar\">
            <div class=\"col-md-1\"></div> 
            <a class=\"navbar-brand \" href=\"#\">FACEPALM</a>
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
            </button>

            <div class=\"collapse navbar-collapse \" id=\"navbarSupportedContent\">
                <form class=\"form-inline my-2 my-lg-0 ml-auto\" method=\"post\">
                    <input class=\"form-control mr-sm-2\" type=\"email\" name=\"emailLogin\" placeholder=\"Email\" aria-label=\"Search\">
                    <input class=\"form-control mr-sm-2\" type=\"password\" name=\"passLogin\" placeholder=\"Password\" aria-label=\"Search\">
                    <button class=\"btn btn-outline-success my-2 my-sm-0\" name=\"doLogin\" type=\"submit\">Login</button>
                </form>
            </div>
            <div class=\"col-md-1\"></div> 
        </nav>
        <br><br><br>
        <div class=\"row mt-5 container-fluid justify-content-md-center\">
            <div class=\"col-lg-4\">
                <p class=\"justify-content-center lead\">Connect with friends and the world around you on FacePalm.</p>
                <br><br>
                <img class=\"rounded justify-content-center \" src='/images/Mega facepalm.gif' maxwidth=\"343\"/>
                <br><br>
            </div>
            <div class=\"col-lg-4\">

                ";
        // line 50
        if (($context["errorList"] ?? null)) {
            echo "    
                    <ul class=\"error\">
                        ";
            // line 52
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errorList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 53
                echo "                            <li class='text-danger'><i class=\"material-icons\">error_outline</i>";
                echo twig_escape_filter($this->env, $context["error"], "html", null, true);
                echo "</li>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "                    </ul>
                ";
        }
        // line 57
        echo "                <form method=\"post\">
                    <div class=\"form-group\">
                        <br>
                        <input class=\"form-control\" placeholder=\"Enter First Name\" type=\"text\" name=\"fName\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "fName", array()), "html", null, true);
        echo "\" ><br>
                        <input class=\"form-control\" placeholder=\"Enter Last Name\" type=\"text\" name=\"lName\" value=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "lName", array()), "html", null, true);
        echo "\" ><br>
                        <input class=\"form-control\" placeholder=\"Enter Email\" type=\"email\" name=\"email\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute(($context["v"] ?? null), "email", array()), "html", null, true);
        echo "\" ><br>                 
                        <input class=\"form-control\" placeholder=\"Enter Password\" type=\"password\" name=\"pass1\"><br>
                        <input class=\"form-control\" placeholder=\"Retype Password\" type=\"password\" name=\"pass2\"><br>
                    </div>
                    <input type=\"submit\" class=\"btn btn-danger\" value=\"Register\" name=\"doRegister\">
                    <input type=\"hidden\" name=\"recaptcha_response\" id=\"recaptchaResponse\">
                </form>
            </div>
        </div>
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "signup.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 62,  101 => 61,  97 => 60,  92 => 57,  88 => 55,  79 => 53,  75 => 52,  70 => 50,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css\" integrity=\"sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS\" crossorigin=\"anonymous\">
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js\" integrity=\"sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js\" integrity=\"sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k\" crossorigin=\"anonymous\"></script>
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
        <script src='https://www.google.com/recaptcha/api.js?render=6Le_nYwUAAAAAE4TGu1slq-SMKCq8vwraxEaXF69'></script>
        <script>
            grecaptcha.ready(function () {
                grecaptcha.execute('6Le_nYwUAAAAAE4TGu1slq-SMKCq8vwraxEaXF69', {action: 'register'})
                        .then(function (token) {
                            var recaptchaResponse = document.getElementById('recaptchaResponse');
                            recaptchaResponse.value = token;
                        });
            });
        </script>
        <link rel=\"stylesheet\" href=\"/styles.css\" />
        <title>FacePalm</title>
    </head>
    <body>
        <nav class=\"navbar navbar-expand-lg navbar-dark bg-dark flex-column flex-md-row bd-navbar\">
            <div class=\"col-md-1\"></div> 
            <a class=\"navbar-brand \" href=\"#\">FACEPALM</a>
            <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                <span class=\"navbar-toggler-icon\"></span>
            </button>

            <div class=\"collapse navbar-collapse \" id=\"navbarSupportedContent\">
                <form class=\"form-inline my-2 my-lg-0 ml-auto\" method=\"post\">
                    <input class=\"form-control mr-sm-2\" type=\"email\" name=\"emailLogin\" placeholder=\"Email\" aria-label=\"Search\">
                    <input class=\"form-control mr-sm-2\" type=\"password\" name=\"passLogin\" placeholder=\"Password\" aria-label=\"Search\">
                    <button class=\"btn btn-outline-success my-2 my-sm-0\" name=\"doLogin\" type=\"submit\">Login</button>
                </form>
            </div>
            <div class=\"col-md-1\"></div> 
        </nav>
        <br><br><br>
        <div class=\"row mt-5 container-fluid justify-content-md-center\">
            <div class=\"col-lg-4\">
                <p class=\"justify-content-center lead\">Connect with friends and the world around you on FacePalm.</p>
                <br><br>
                <img class=\"rounded justify-content-center \" src='/images/Mega facepalm.gif' maxwidth=\"343\"/>
                <br><br>
            </div>
            <div class=\"col-lg-4\">

                {% if errorList %}    
                    <ul class=\"error\">
                        {% for error in errorList %}
                            <li class='text-danger'><i class=\"material-icons\">error_outline</i>{{error}}</li>
                            {% endfor %}
                    </ul>
                {% endif %}
                <form method=\"post\">
                    <div class=\"form-group\">
                        <br>
                        <input class=\"form-control\" placeholder=\"Enter First Name\" type=\"text\" name=\"fName\" value=\"{{v.fName}}\" ><br>
                        <input class=\"form-control\" placeholder=\"Enter Last Name\" type=\"text\" name=\"lName\" value=\"{{v.lName}}\" ><br>
                        <input class=\"form-control\" placeholder=\"Enter Email\" type=\"email\" name=\"email\" value=\"{{v.email}}\" ><br>                 
                        <input class=\"form-control\" placeholder=\"Enter Password\" type=\"password\" name=\"pass1\"><br>
                        <input class=\"form-control\" placeholder=\"Retype Password\" type=\"password\" name=\"pass2\"><br>
                    </div>
                    <input type=\"submit\" class=\"btn btn-danger\" value=\"Register\" name=\"doRegister\">
                    <input type=\"hidden\" name=\"recaptcha_response\" id=\"recaptchaResponse\">
                </form>
            </div>
        </div>
    </body>
</html>", "signup.html.twig", "C:\\xampp\\htdocs\\project\\facepalm\\templates\\signup.html.twig");
    }
}
